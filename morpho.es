<?xml version="1.0" encoding="UTF-8"?>
<description lang="si">
  <letterclass name="eií" letters="e i í"/>
  <letterclass name="enr" letters="e n r"/>
  <letterclass name="i" letters="i"/>
  <letterclass name="í" letters="í"/>
  <letterclass name="bdfhjklmnprstvwxy" letters="b d f h j k l m n p r s t v w x y"/>

  <!-- fusions -->
  <!--fusion source="g_[:eií:]" target="gu_[:eií:]"/-->
  <!--fusion source="z_[:eií:]" target="c_[:eií:]"/-->
  <!--fusion source="c_[:eií:]" target="qu_[:eií:]"/-->
  
  <!--fusion source="a_i" target="a_í"/-->
  <fusion source="e_ina" target="e_ína"/>
  <fusion source="[:enr:]_íto" target="[:enr:]_cito"/>
  <fusion source="[:enr:]_íta" target="[:enr:]_cita"/>
  <fusion source="[:enr:]_íco" target="[:enr:]_cico"/>
  <fusion source="[:enr:]_íca" target="[:enr:]_cica"/>
  <fusion source="[:enr:]_íllo" target="[:enr:]_cillo"/>
  <fusion source="[:enr:]_ílla" target="[:enr:]_cilla"/>
  <fusion source="[:enr:]_ete" target="[:enr:]_cete"/>
  <fusion source="[:enr:]_eta" target="[:enr:]_ceta"/>
  <fusion source="én_es" target="en_es"/>
  <fusion source="én_zote" target="en_zote"/>
  <fusion source="én_zón" target="en_zón"/>
  <fusion source="én_zazo" target="en_zazo"/>
  <fusion source="én_zaco" target="en_zaco"/>
  <fusion source="én_zanco" target="en_zanco"/>
  <fusion source="én_cito" target="en_cito"/>
  <fusion source="én_cico" target="en_cico"/>
  <fusion source="én_cillo" target="en_cillo"/>
  <fusion source="én_cete" target="en_cete"/>
  <fusion source="én_cín" target="en_cín"/>

  <fusion source="g_íto" target="gu_ito"/>
  <fusion source="g_íta" target="gu_ita"/>
  <fusion source="g_íco" target="gu_ico"/>
  <fusion source="g_íca" target="gu_ica"/>
  <fusion source="g_íllo" target="gu_illo"/>
  <fusion source="g_ílla" target="gu_illa"/>
  <fusion source="g_ete" target="gu_ete"/>
  <fusion source="g_eta" target="gu_eta"/>
  <!--fusion source="g_ín" target="gu_ín"/-->
  <!--fusion source="g_ina" target="gu_ina"/-->

  <fusion source="z_íto" target="c_ito"/>
  <fusion source="z_íta" target="c_ita"/>
  <fusion source="z_íco" target="c_ico"/>
  <fusion source="z_íca" target="c_ica"/>
  <fusion source="z_íllo" target="c_illo"/>
  <fusion source="z_ílla" target="c_illa"/>
  <fusion source="z_ete" target="c_ete"/>
  <fusion source="z_eta" target="c_eta"/>
  <!--fusion source="z_ín" target="c_ín"/-->
  <!--fusion source="z_ina" target="c_ina"/-->

  <fusion source="c_íto" target="qu_ito"/>
  <fusion source="c_íta" target="qu_ita"/>
  <fusion source="c_íco" target="qu_ico"/>
  <fusion source="c_íca" target="qu_ica"/>
  <fusion source="c_íllo" target="qu_illo"/>
  <fusion source="c_ílla" target="qu_illa"/>
  <fusion source="c_ete" target="qu_ete"/>
  <fusion source="c_eta" target="qu_eta"/>
  <!--fusion source="c_ín" target="qu_ín"/-->
  <!--fusion source="c_ina" target="qu_ina"/-->
  
  <fusion source="[:bdfhjklmnprstvwxy:]_íto" target="[:bdfhjklmnprstvwxy:]_ito"/>
  <fusion source="[:bdfhjklmnprstvwxy:]_íta" target="[:bdfhjklmnprstvwxy:]_ita"/> 
  <fusion source="[:bdfhjklmnprstvwxy:]_íco" target="[:bdfhjklmnprstvwxy:]_ico"/> 
  <fusion source="[:bdfhjklmnprstvwxy:]_íca" target="[:bdfhjklmnprstvwxy:]_ica"/>
  <fusion source="[:bdfhjklmnprstvwxy:]_íllo" target="[:bdfhjklmnprstvwxy:]_illo"/>
  <fusion source="[:bdfhjklmnprstvwxy:]_ílla" target="[:bdfhjklmnprstvwxy:]_illa"/>

  <!--fusion source="[:i:]_i" target="[:i:]_í"/-->

  <!-- INVARIABLE -->
  <table name="inv" rads="..*" fast="-">
    <form suffix="" tag=""/>
  </table>
  <table name="pref" rads="..*" fast="-">
    <form suffix="_" tag=""/>
  </table>
  <!-- Nombres propios -->
  <table name="np-ms" rads="..*">
    <form suffix="" tag="ms"/>
  </table>
  <table name="np-mp" rads="..*">
    <form suffix="" tag="mp"/>
  </table>
  <table name="np-fs" rads="..*">
    <form suffix="" tag="fs"/>
  </table>
  <table name="np-fp" rads="..*">
    <form suffix="" tag="fp"/>
  </table>
  <table name="np-f" rads="..*">
    <form suffix="" tag="fs"/>
    <form suffix="s" tag="fp"/>
  </table>
  <table name="np-m" rads="..*">
    <form suffix="" tag="ms"/>
    <form suffix="s" tag="mp"/>
  </table>
  <table name="np-s" rads="..*">
    <form suffix="" tag="s"/>
  </table>
  <table name="np-m0" rads="..*">
    <form suffix="" tag="m0"/>
  </table>
  <table name="np-f0" rads="..*">
    <form suffix="" tag="f0"/>
  </table>
  <table name="np-00" rads="..*">
    <form suffix="" tag="00"/>
  </table>
  <table name="A1" rads=".*"> 
    <!-- 11470 members -->
    <form suffix="o" tag="Q0MS0"/>
    <form suffix="a" tag="Q0FS0"/>
    <form suffix="as" tag="Q0FP0"/>
    <form suffix="os" tag="Q0MP0"/>
    <!-- Added by Nieves -->
    <derivation name="adverbio" suffix="amente" table="adverb"/>
    <derivation name="aumentativo" suffix="ote" table="aument"/>
    <derivation name="aumentativo" suffix="ota" table="aument"/>
    <derivation name="aumentativo" suffix="ón" table="aument"/>
    <derivation name="aumentativo" suffix="ona" table="aument"/>
    <derivation name="aumentativo" suffix="azo" table="aument"/>
    <derivation name="aumentativo" suffix="aza" table="aument"/>
    <derivation name="aumentativo" suffix="aco" table="aument"/>
    <derivation name="aumentativo" suffix="aca" table="aument"/>
    <derivation name="aumentativo" suffix="anco" table="aument"/>
    <derivation name="aumentativo" suffix="anca" table="aument"/>
    <derivation name="diminutivo" suffix="íto" table="diminut"/>
    <derivation name="diminutivo" suffix="íta" table="diminut"/>
    <derivation name="diminutivo" suffix="íco" table="diminut"/>
    <derivation name="diminutivo" suffix="íca" table="diminut"/>
    <derivation name="diminutivo" suffix="íllo" table="diminut"/>
    <derivation name="diminutivo" suffix="ílla" table="diminut"/>
    <derivation name="diminutivo" suffix="ete" table="diminut"/>
    <derivation name="diminutivo" suffix="eta" table="diminut"/>
    <!--derivation name="diminutivo" suffix="ín" table="diminut"/-->
    <!--derivation name="diminutivo" suffix="ina" table="diminut"/-->
  </table>
  <table name="A2" rads=".*"> 
    <!-- 3499 members -->
    <form suffix="" tag="Q0CS0"/>
    <form suffix="s" tag="Q0CP0"/>
    <!-- Added by Nieves -->
    <!-- Impossible aumentatives and diminutives for: idioms, parties, places, territories or institutions in that exists an autority, professions or activities with the suffix -ista, points cardinals or from orientation --> 
    <derivation name="adverbio" suffix="mente" table="adverb"/>
  </table>
  <table name="A3" rads=".*">
    <!-- 2178 members -->
    <form suffix="" tag="Q0CS0"/>
    <form suffix="es" tag="Q0CP0"/>
    <!-- Added by Nieves -->
    <derivation name="adverbio" suffix="mente" table="adverb"/>
  </table>
  <table name="A4" rads=".*[ouea][rln]"> <!-- Nieves adjetivos que terminan en -or -->
    <!-- 1561 members -->
    <form suffix="" tag="Q0MS0"/>
    <form suffix="a" tag="Q0FS0"/>
    <form suffix="as" tag="Q0FP0"/>
    <form suffix="es" tag="Q0MP0"/>
    <!-- Added by Nieves -->
    <derivation name="adverbio" suffix="amente" table="adverb"/>
    <derivation name="aumentativo" suffix="ote" table="aument"/>
    <derivation name="aumentativo" suffix="ota" table="aument"/>
    <derivation name="aumentativo" suffix="ón" table="aument"/>
    <derivation name="aumentativo" suffix="ona" table="aument"/>
    <derivation name="aumentativo" suffix="azo" table="aument"/>
    <derivation name="aumentativo" suffix="aza" table="aument"/>
    <derivation name="aumentativo" suffix="aco" table="aument"/>
    <derivation name="aumentativo" suffix="aca" table="aument"/>
    <derivation name="aumentativo" suffix="anco" table="aument"/>
    <derivation name="aumentativo" suffix="anca" table="aument"/>
    <derivation name="diminutivo" suffix="íto" table="diminut"/>
    <derivation name="diminutivo" suffix="íta" table="diminut"/>
    <derivation name="diminutivo" suffix="íco" table="diminut"/>
    <derivation name="diminutivo" suffix="íca" table="diminut"/>
    <derivation name="diminutivo" suffix="íllo" table="diminut"/>
    <derivation name="diminutivo" suffix="ílla" table="diminut"/>
    <derivation name="diminutivo" suffix="ete" table="diminut"/>
    <derivation name="diminutivo" suffix="eta" table="diminut"/>
    <derivation name="diminutivo" suffix="ín" table="diminut"/>
    <derivation name="diminutivo" suffix="ina" table="diminut"/>
    
  </table>
  <table name="A5" rads=".*">
    <!-- 240 members -->
    <form suffix="ón" tag="Q0MS0"/>
    <form suffix="ona" tag="Q0FS0"/>
    <form suffix="onas" tag="Q0FP0"/>
    <form suffix="ones" tag="Q0MP0"/>
    <!-- Added by Nieves -->
    <derivation name="adverbio" suffix="onamente" table="adverb"/>
    <derivation name="aumentativo" suffix="onzote" table="aument"/>
    <derivation name="aumentativo" suffix="onzota" table="aument"/>
    <derivation name="aumentativo" suffix="onzazo" table="aument"/>
    <derivation name="aumentativo" suffix="onzaza" table="aument"/>
    <derivation name="aumentativo" suffix="onzaco" table="aument"/>
    <derivation name="aumentativo" suffix="onzaca" table="aument"/>
    <derivation name="aumentativo" suffix="onzanco" table="aument"/>
    <derivation name="aumentativo" suffix="onzanca" table="aument"/>
    <derivation name="diminutivo" suffix="oncito" table="diminut"/>
    <derivation name="diminutivo" suffix="oncita" table="diminut"/>
    <derivation name="diminutivo" suffix="oncico" table="diminut"/>
    <derivation name="diminutivo" suffix="oncica" table="diminut"/>
    <derivation name="diminutivo" suffix="oncillo" table="diminut"/>
    <derivation name="diminutivo" suffix="oncilla" table="diminut"/>
    <derivation name="diminutivo" suffix="oncete" table="diminut"/>
    <derivation name="diminutivo" suffix="onceta" table="diminut"/>
    <derivation name="diminutivo" suffix="oncín" table="diminut"/>
    <derivation name="diminutivo" suffix="oncina" table="diminut"/>
    
  </table>
  <table name="A6" rads=".*"> <!-- Nieves revisar -->
    <!-- 236 members -->
    <form suffix="o" tag="O0MS0"/>
    <form suffix="a" tag="O0FS0"/>
    <form suffix="as" tag="O0FP0"/>
    <form suffix="os" tag="O0MP0"/>
  </table>
  <table name="A7" rads=".*"> <!-- Nieves revisar -->
    <!-- 113 members -->
    <form suffix="" tag="Q0CN0"/>
  </table>
  <table name="A8" rads=".*"> <!-- Nieves revisar -->
    <!-- 93 members -->
    <form suffix="és" tag="Q0MS0"/>
    <form suffix="esa" tag="Q0FS0"/>
    <form suffix="esas" tag="Q0FP0"/>
    <form suffix="eses" tag="Q0MP0"/>
  </table>
  <table name="A9" rads=".*[td]">
    <!-- 50 members -->
    <form suffix="o" tag="Q0MSP"/>
    <form suffix="a" tag="Q0FSP"/>
    <form suffix="as" tag="Q0FPP"/>
    <form suffix="os" tag="Q0MPP"/>
    <!-- Added by Nieves -->
    <derivation name="adverbio" suffix="amente" table="adverb"/>
    <derivation name="aumentativo" suffix="ote" table="aument"/>
    <derivation name="aumentativo" suffix="ota" table="aument"/>
    <derivation name="aumentativo" suffix="ón" table="aument"/>
    <derivation name="aumentativo" suffix="ona" table="aument"/>
    <derivation name="aumentativo" suffix="azo" table="aument"/>
    <derivation name="aumentativo" suffix="aza" table="aument"/>
    <derivation name="aumentativo" suffix="aco" table="aument"/>
    <derivation name="aumentativo" suffix="aca" table="aument"/>
    <derivation name="aumentativo" suffix="anco" table="aument"/>
    <derivation name="aumentativo" suffix="anca" table="aument"/>
    <derivation name="diminutivo" suffix="íto" table="diminut"/>
    <derivation name="diminutivo" suffix="íta" table="diminut"/>
    <derivation name="diminutivo" suffix="íco" table="diminut"/>
    <derivation name="diminutivo" suffix="íca" table="diminut"/>
    <derivation name="diminutivo" suffix="íllo" table="diminut"/>
    <derivation name="diminutivo" suffix="ílla" table="diminut"/>
    <derivation name="diminutivo" suffix="ete" table="diminut"/>
    <derivation name="diminutivo" suffix="eta" table="diminut"/>
    <derivation name="diminutivo" suffix="ín" table="diminut"/>
    <derivation name="diminutivo" suffix="ina" table="diminut"/>
  </table>
  <table name="A10" rads=".*[íkrú]"> <!-- Nieves revisar -->
    <!-- 41 members -->
    <form suffix="" tag="Q0CS0"/>
    <form suffix="es" tag="Q0CP0"/>
    <form suffix="s" tag="Q0CP0"/>
  </table>
  <table name="A11" rads=".*" fast="-"> <!-- Nieves revisar -->
    <!-- 27 members -->
    <form suffix="" tag="Q0CP0"/>
  </table>
  <table name="A12" rads=".*[oae]" fast="-">
    <!-- 26 members -->
    <form suffix="z" tag="Q0CS0"/>
    <form suffix="ces" tag="Q0CP0"/>
    <derivation name="adverbio" suffix="zmente" table="adverb"/>
  </table>
  <table name="A13" rads=".*" fast="-"> <!-- Nieves revisar -->
    <!-- 22 members -->
    <form suffix="o" tag="Q0MS0"/>
    <form suffix="os" tag="Q0MP0"/>
  </table>
  <table name="A14" rads=".*" fast="-"> <!-- Nieves revisar -->
    <!-- 21 members -->
    <form suffix="ín" tag="Q0MS0"/>
    <form suffix="ina" tag="Q0FS0"/>
    <form suffix="inas" tag="Q0FP0"/>
    <form suffix="ines" tag="Q0MP0"/>
  </table>
  <table name="A15" rads=".*a" fast="-"> <!-- Nieves revisar -->
    <!-- 20 members -->
    <form suffix="" tag="Q0FS0"/>
    <form suffix="s" tag="Q0FP0"/>
  </table>
  <table name="A16" rads=".*" fast="-">
    <!-- 17 members -->
    <form suffix="íaco" tag="Q0MS0"/>
    <form suffix="iaca" tag="Q0FS0"/>
    <form suffix="iacas" tag="Q0FP0"/>
    <form suffix="iacos" tag="Q0MP0"/>
    <form suffix="íaca" tag="Q0FS0"/>
    <form suffix="íacas" tag="Q0FP0"/>
    <form suffix="íacos" tag="Q0MP0"/>
    <derivation name="adverbio" suffix="íacamente" table="adverb"/>
    <derivation name="adverbio" suffix="iacamente" table="adverb"/>
  </table>
  <table name="A17" rads=".*" fast="-">
    <!-- 15 members -->
    <form suffix="án" tag="Q0MS0"/>
    <form suffix="ana" tag="Q0FS0"/>
    <form suffix="anas" tag="Q0FP0"/>
    <form suffix="anes" tag="Q0MP0"/>
    <!-- Added by Nieves -->
    <derivation name="adverbio" suffix="amente" table="adverb"/>
    <derivation name="aumentativo" suffix="anzote" table="aument"/>
    <derivation name="aumentativo" suffix="anzota" table="aument"/>
    <derivation name="aumentativo" suffix="anzón" table="aument"/>
    <derivation name="aumentativo" suffix="anzona" table="aument"/>
    <derivation name="aumentativo" suffix="anzazo" table="aument"/>
    <derivation name="aumentativo" suffix="anzaza" table="aument"/>
    <derivation name="aumentativo" suffix="anzaco" table="aument"/>
    <derivation name="aumentativo" suffix="anzaca" table="aument"/>
    <derivation name="aumentativo" suffix="anzanco" table="aument"/>
    <derivation name="aumentativo" suffix="anzanca" table="aument"/>
    <derivation name="diminutivo" suffix="ancito" table="diminut"/>
    <derivation name="diminutivo" suffix="ancita" table="diminut"/>
    <derivation name="diminutivo" suffix="ancico" table="diminut"/>
    <derivation name="diminutivo" suffix="ancica" table="diminut"/>
    <derivation name="diminutivo" suffix="ancillo" table="diminut"/>
    <derivation name="diminutivo" suffix="ancilla" table="diminut"/>
    <derivation name="diminutivo" suffix="ancete" table="diminut"/>
    <derivation name="diminutivo" suffix="anceta" table="diminut"/>
    <!--derivation name="diminutivo" suffix="ancín" table="diminut"/-->
    <!--derivation name="diminutivo" suffix="ancina" table="diminut"/-->
  </table>
  <table name="A18" rads=".*" fast="-">
    <!-- 14 members -->
    <form suffix="és" tag="Q0CS0"/>
    <form suffix="esa" tag="Q0FS0"/>
    <form suffix="esas" tag="Q0FP0"/>
    <form suffix="eses" tag="Q0CP0"/>
    <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="esote" table="aument"/>
    <derivation name="aumentativo" suffix="esota" table="aument"/>
    <derivation name="aumentativo" suffix="esón" table="aument"/>
    <derivation name="aumentativo" suffix="esona" table="aument"/>
    <derivation name="aumentativo" suffix="esazo" table="aument"/>
    <derivation name="aumentativo" suffix="esaza" table="aument"/>
    <derivation name="aumentativo" suffix="esaco" table="aument"/>
    <derivation name="aumentativo" suffix="esaca" table="aument"/>
    <derivation name="aumentativo" suffix="esanco" table="aument"/>
    <derivation name="aumentativo" suffix="esanca" table="aument"/>
    <derivation name="diminutivo" suffix="esito" table="diminut"/>
    <derivation name="diminutivo" suffix="esita" table="diminut"/>
    <derivation name="diminutivo" suffix="esico" table="diminut"/>
    <derivation name="diminutivo" suffix="esica" table="diminut"/>
    <derivation name="diminutivo" suffix="esillo" table="diminut"/>
    <derivation name="diminutivo" suffix="esilla" table="diminut"/>
    <derivation name="diminutivo" suffix="esete" table="diminut"/>
    <derivation name="diminutivo" suffix="eseta" table="diminut"/>
    <!--derivation name="diminutivo" suffix="esín" table="diminut"/-->
    <!--derivation name="diminutivo" suffix="esina" table="diminut"/-->
  </table>
  <table name="A19" rads=".*[oen]t" fast="-"> <!-- Nieves revisar -->
    <!-- 14 members -->
    <form suffix="e" tag="Q0MS0"/>
    <form suffix="a" tag="Q0FS0"/>
    <form suffix="as" tag="Q0FP0"/>
    <form suffix="es" tag="Q0MP0"/>
  </table>
  <table name="A20" rads=".*[iae]" fast="-">
    <!-- 13 members -->
    <form suffix="z" tag="Q0CS0"/>
    <form suffix="ces" tag="Q0CP0"/>
    <derivation name="adverbio" suffix="zmente" table="adverb"/>
  </table>
  <table name="A21" rads=".*[nmerd][aoc]tri" fast="-">
    <!-- 13 members -->
    <form suffix="z" tag="Q0FS0"/>
    <form suffix="ces" tag="Q0FP0"/>
    <derivation name="adverbio" suffix="zmente" table="adverb"/>
  </table>
  <table name="A22" rads=".*[sncgr]" fast="-">
    <!-- 12 members -->
    <form suffix="iaco" tag="Q0MS0"/>
    <form suffix="iaca" tag="Q0FS0"/>
    <form suffix="iacas" tag="Q0FP0"/>
    <form suffix="iacos" tag="Q0MP0"/>
    <form suffix="íaca" tag="Q0FS0"/>
    <form suffix="íacas" tag="Q0FP0"/>
    <form suffix="íacos" tag="Q0MP0"/>
    <!-- Added by Nieves -->
    <derivation name="adverbio" suffix="iacamente" table="adverb"/>
    <derivation name="adverbio" suffix="íacamente" table="adverb"/>
  </table>
  <table name="A23" rads=".*" fast="-"> <!-- Nieves revisar -->
    <!-- 9 members -->
    <form suffix="és" tag="Q0CS0"/>
    <form suffix="eses" tag="Q0CP0"/>
  </table>
  <table name="A24" rads=".*[orse]" fast="-"> <!-- Nieves revisar -->
    <!-- 8 members -->
    <form suffix="" tag="Q0MS0"/>
  </table>
  <table name="A25" rads=".*[íú]" fast="-"> <!-- Nieves revisar -->
    <!-- 7 members -->
    <form suffix="" tag="Q0CS0"/>
    <form suffix="es" tag="Q0CP0"/>
    <form suffix="s" tag="Q0CP0"/>
    <form suffix="ses" tag="Q0CP0"/>
  </table>
  <table name="A26" rads=".*[ondae]" fast="-"> <!-- Nieves revisar -->
    <!-- 7 members -->
    <form suffix="" tag="Q0CS0"/>
  </table>
  <table name="A27" rads=".*[lmsrz]" fast="-"> <!-- Nieves reviar -->
    <!-- 7 members -->
    <form suffix="ón" tag="Q0CS0"/>
    <form suffix="ones" tag="Q0CP0"/>
  </table>
  <table name="A28" rads=".*(rococó|beige|antidoping|magenta)" fast="-">
    <!-- 4 members -->
    <form suffix="" tag="Q0CN0"/>
    <form suffix="s" tag="Q0CP0"/>
  </table>
  <table name="A29" rads=".*(trece|nueve|doce|siete)" fast="-"> <!-- Nieves revisar -->
    <!-- 4 members -->
    <form suffix="" tag="Q0CP0"/>
    <form suffix="s" tag="Q0CP0"/>
  </table>
  <table name="A30" rads=".*(unicolor|multicanal|monocolor)" fast="-"> 
    <!-- 3 members -->
    <form suffix="" tag="Q0CN0"/>
    <form suffix="es" tag="Q0CP0"/>
  </table>
  <table name="A31" rads=".*(asistent|parient|principiant)" fast="-">
    <!-- 3 members -->
    <form suffix="e" tag="Q0CS0"/>
    <form suffix="a" tag="Q0FS0"/>
    <form suffix="as" tag="Q0FP0"/>
    <form suffix="es" tag="Q0CP0"/>
  </table>
  <table name="A32" rads=".*(cent|b|caud)" fast="-">
    <!-- 3 members -->
    <form suffix="ímano" tag="Q0MS0"/>
    <form suffix="imana" tag="Q0FS0"/>
    <form suffix="imanas" tag="Q0FP0"/>
    <form suffix="imanos" tag="Q0MP0"/>
    <form suffix="ímana" tag="Q0FS0"/>
    <form suffix="ímanas" tag="Q0FP0"/>
    <form suffix="ímanos" tag="Q0MP0"/>
  </table>
  <table name="A33" rads=".*(cent|b|caud)" fast="-">
    <!-- 3 members -->
    <form suffix="imano" tag="Q0MS0"/>
    <form suffix="imana" tag="Q0FS0"/>
    <form suffix="imanas" tag="Q0FP0"/>
    <form suffix="imanos" tag="Q0MP0"/>
    <form suffix="ímana" tag="Q0FS0"/>
    <form suffix="ímanas" tag="Q0FP0"/>
    <form suffix="ímanos" tag="Q0MP0"/>
  </table>
  <table name="A34" rads=".*(yemen|paquistan|bengal)" fast="-">
    <!-- 3 members -->
    <form suffix="í" tag="Q0CS0"/>
    <form suffix="ises" tag="Q0CP0"/>
    <form suffix="íes" tag="Q0CP0"/>
    <form suffix="ís" tag="Q0CP0"/>
  </table>
  <table name="A35" rads=".*(superior|motor|mongol)" fast="-"> <!-- Nieves revisar -->
    <!-- 3 members -->
    <form suffix="" tag="Q0CS0"/>
    <form suffix="a" tag="Q0FS0"/>
    <form suffix="as" tag="Q0FP0"/>
    <form suffix="es" tag="Q0CP0"/>
  </table>
  <table name="A36" rads=".*(jurídico-político|economicoadministrativ|recién)" fast="-"> <!-- Nieves revisar -->
    <!-- 3 members -->
    <form suffix="" tag="Q0MS0"/>
    <form suffix="a" tag="Q0FS0"/>
    <form suffix="as" tag="Q0FP0"/>
    <form suffix="o" tag="Q0MS0"/>
    <form suffix="os" tag="Q0MP0"/>
  </table>
  <table name="C1" rads=".*" fast="-">
    <!-- 18 members Coordinate-->
    <form suffix="" tag=""/>
  </table>
  <table name="C2" rads=".*" fast="-">
    <!-- 13 members Subordinate-->
    <form suffix="" tag=""/>
  </table>
  <table name="D1" rads=".*" fast="-">
    <!-- 11 members -->
    <form suffix="o" tag="I0MS0"/>
    <form suffix="a" tag="I0FS0"/>
    <form suffix="as" tag="I0FP0"/>
    <form suffix="os" tag="I0MP0"/>
  </table>
  <table name="D2" rads=".*(un|distint|divers)" fast="-">
    <!-- 3 members -->
    <form suffix="o" tag="I0MS0"/>
    <form suffix="as" tag="I0FP0"/>
    <form suffix="os" tag="I0MP0"/>
  </table>
  <table name="D3" rads=".*(send|amb|vari)" fast="-">
    <!-- 3 members -->
    <form suffix="os" tag="I0MP0"/>
    <form suffix="as" tag="I0FP0"/>
  </table>
  <table name="D4" rads=".*" fast="-">
    <!-- Addeb by Miguel: To include un, una -->
    <form suffix="" tag="I0MS0"/>
    <form suffix="a" tag="I0FS0"/>
  </table>
  <table name="I1" rads=".*">
    <!-- 206 members -->
    <form suffix="" tag=""/>
  </table>
  <table name="N1" rads=".*"> <!-- Nieves revisar -->
    <!-- 16490 members -->
    <form suffix="" tag="CMS000"/>
    <form suffix="s" tag="CMP000"/>
  </table>
  <table name="N2" rads=".*"> <!-- Nieves revisar -->
    <!-- 14339 members -->
    <form suffix="" tag="CFS000"/>
    <form suffix="s" tag="CFP000"/>
  </table>
  <table name="N3" rads=".*[izst]">
    <!-- 3286 members -->
    <form suffix="ón" tag="CFS000"/>
    <form suffix="ones" tag="CFP000"/>
    <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="onota" table="aument"/>
    <derivation name="aumentativo" suffix="onaza" table="aument"/>
    <derivation name="aumentativo" suffix="onaca" table="aument"/>
    <derivation name="aumentativo" suffix="onanca" table="aument"/>
    <derivation name="diminutivo" suffix="onita" table="diminut"/>
    <derivation name="diminutivo" suffix="onica" table="diminut"/>
    <derivation name="diminutivo" suffix="onilla" table="diminut"/>
    <derivation name="diminutivo" suffix="oneta" table="diminut"/>
    <!--derivation name="diminutivo" suffix="onina" table="diminut"/-->
  </table>
  <table name="N4" rads=".*">
    <!-- 2892 members -->
    <form suffix="o" tag="CMS000"/>
    <form suffix="a" tag="CFS000"/>
    <form suffix="as" tag="CFP000"/>
    <form suffix="os" tag="CMP000"/>
    <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="ote" table="aument"/>
    <derivation name="aumentativo" suffix="ota" table="aument"/>
    <derivation name="aumentativo" suffix="ón" table="aument"/>
    <derivation name="aumentativo" suffix="ona" table="aument"/>
    <derivation name="aumentativo" suffix="azo" table="aument"/>
    <derivation name="aumentativo" suffix="aza" table="aument"/>
    <derivation name="aumentativo" suffix="aco" table="aument"/>
    <derivation name="aumentativo" suffix="aca" table="aument"/>
    <derivation name="aumentativo" suffix="anco" table="aument"/>
    <derivation name="aumentativo" suffix="anca" table="aument"/>
    <derivation name="diminutivo" suffix="íto" table="diminut"/>
    <derivation name="diminutivo" suffix="íta" table="diminut"/>
    <derivation name="diminutivo" suffix="íco" table="diminut"/>
    <derivation name="diminutivo" suffix="íca" table="diminut"/>
    <derivation name="diminutivo" suffix="íllo" table="diminut"/>
    <derivation name="diminutivo" suffix="ílla" table="diminut"/>
    <derivation name="diminutivo" suffix="ete" table="diminut"/>
    <derivation name="diminutivo" suffix="eta" table="diminut"/>
    <!--derivation name="diminutivo" suffix="ín" table="diminut"/-->
    <!--derivation name="diminutivo" suffix="ina" table="diminut"/-->
  </table>
  <table name="N5" rads=".*">
    <!-- 2616 members -->
    <form suffix="" tag="CMS000"/>
    <form suffix="es" tag="CMP000"/>
    <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="ote" table="aument"/>
    <derivation name="aumentativo" suffix="ón" table="aument"/>
    <derivation name="aumentativo" suffix="azo" table="aument"/>
    <derivation name="aumentativo" suffix="aco" table="aument"/>
    <derivation name="aumentativo" suffix="anco" table="aument"/>
    <derivation name="diminutivo" suffix="íto" table="diminut"/>
    <derivation name="diminutivo" suffix="íco" table="diminut"/>
    <derivation name="diminutivo" suffix="íllo" table="diminut"/>
    <derivation name="diminutivo" suffix="ete" table="diminut"/>
    <derivation name="diminutivo" suffix="ín" table="diminut"/>
  </table>
  <table name="N6" rads=".*"> <!-- Nieves revisar -->
    <!-- 1695 members -->
    <form suffix="" tag="CCS000"/>
    <form suffix="s" tag="CCP000"/>
  </table>
  <table name="N7" rads=".*"> <!-- Nieves revisar -->
    <!-- 1512 members -->
    <form suffix="" tag="CFS000"/>
    <form suffix="es" tag="CFP000"/>
  </table>
  <table name="N8" rads=".*[rlsn]"> 
    <!-- 1344 members -->
    <form suffix="" tag="CMS000"/>
    <form suffix="a" tag="CFS000"/>
    <form suffix="as" tag="CFP000"/>
    <form suffix="es" tag="CMP000"/>
    <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="ote" table="aument"/>
    <derivation name="aumentativo" suffix="ota" table="aument"/>
    <derivation name="aumentativo" suffix="ón" table="aument"/>
    <derivation name="aumentativo" suffix="ona" table="aument"/>
    <derivation name="aumentativo" suffix="azo" table="aument"/>
    <derivation name="aumentativo" suffix="aza" table="aument"/>
    <derivation name="aumentativo" suffix="aco" table="aument"/>
    <derivation name="aumentativo" suffix="aca" table="aument"/>
    <derivation name="aumentativo" suffix="anco" table="aument"/>
    <derivation name="aumentativo" suffix="anca" table="aument"/>
    <derivation name="diminutivo" suffix="íto" table="diminut"/>
    <derivation name="diminutivo" suffix="íta" table="diminut"/>
    <derivation name="diminutivo" suffix="íco" table="diminut"/>
    <derivation name="diminutivo" suffix="íca" table="diminut"/>
    <derivation name="diminutivo" suffix="íllo" table="diminut"/>
    <derivation name="diminutivo" suffix="ílla" table="diminut"/>
    <derivation name="diminutivo" suffix="ete" table="diminut"/>
    <derivation name="diminutivo" suffix="eta" table="diminut"/>
    <!--derivation name="diminutivo" suffix="ín" table="diminut"/-->
    <!--derivation name="diminutivo" suffix="ina" table="diminut"/-->
  </table>
  <table name="N9" rads=".*">
    <!-- 916 members -->
    <form suffix="" tag="CFN000"/>
  </table>
  <table name="N10" rads=".*">
    <!-- 906 members -->
    <form suffix="ón" tag="CMS000"/>
    <form suffix="ones" tag="CMP000"/>
  </table>
  <table name="N11" rads=".*">
    <!-- 729 members -->
    <form suffix="" tag="CMN000"/>
  </table>
  <table name="N12" rads=".*">
    <!-- 267 members -->
    <form suffix="" tag="CMS000"/>
    <form suffix="es" tag="CMP000"/>
    <form suffix="s" tag="CMP000"/>
  </table>
  <table name="N13" rads=".*">
    <!-- 204 members -->
    <form suffix="ín" tag="CMS000"/>
    <form suffix="ines" tag="CMP000"/>
  </table>
  <table name="N14" rads=".*">
    <!-- 194 members -->
    <form suffix="ón" tag="CMS000"/>
    <form suffix="ona" tag="CFS000"/>
    <form suffix="onas" tag="CFP000"/>
    <form suffix="ones" tag="CMP000"/>
    <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="onzote" table="aument"/>
    <derivation name="aumentativo" suffix="onzota" table="aument"/>
    <derivation name="aumentativo" suffix="onzazo" table="aument"/>
    <derivation name="aumentativo" suffix="onzaza" table="aument"/>
    <derivation name="aumentativo" suffix="onzaco" table="aument"/>
    <derivation name="aumentativo" suffix="onzaca" table="aument"/>
    <derivation name="aumentativo" suffix="onzanco" table="aument"/>
    <derivation name="aumentativo" suffix="onzanca" table="aument"/>
    <derivation name="diminutivo" suffix="oncito" table="diminut"/>
    <derivation name="diminutivo" suffix="oncita" table="diminut"/>
    <derivation name="diminutivo" suffix="oncico" table="diminut"/>
    <derivation name="diminutivo" suffix="oncica" table="diminut"/>
    <derivation name="diminutivo" suffix="oncillo" table="diminut"/>
    <derivation name="diminutivo" suffix="oncilla" table="diminut"/>
    <derivation name="diminutivo" suffix="oncete" table="diminut"/>
    <derivation name="diminutivo" suffix="onceta" table="diminut"/>
    <!--derivation name="diminutivo" suffix="oncín" table="diminut"/-->
    <!--derivation name="diminutivo" suffix="oncina" table="diminut"/-->
  </table>
  <table name="N15" rads=".*">
    <!-- 183 members -->
    <form suffix="z" tag="CFS000"/>
    <form suffix="ces" tag="CFP000"/>
    <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="ota" table="aument"/>
    <derivation name="aumentativo" suffix="ona" table="aument"/>
    <derivation name="aumentativo" suffix="aza" table="aument"/>
    <derivation name="aumentativo" suffix="aca" table="aument"/>
    <derivation name="aumentativo" suffix="anca" table="aument"/>
    <derivation name="diminutivo" suffix="íta" table="diminut"/>
    <derivation name="diminutivo" suffix="íca" table="diminut"/>
    <derivation name="diminutivo" suffix="ílla" table="diminut"/>
    <derivation name="diminutivo" suffix="eta" table="diminut"/>
    <!--derivation name="diminutivo" suffix="ina" table="diminut"/-->
  </table>
  <table name="N16" rads=".*">
    <!-- 143 members -->
    <form suffix="án" tag="CMS000"/>
    <form suffix="anes" tag="CMP000"/>
    <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="anote" table="aument"/>
    <derivation name="aumentativo" suffix="anón" table="aument"/>
    <derivation name="aumentativo" suffix="anazo" table="aument"/>
    <derivation name="aumentativo" suffix="anaco" table="aument"/>
    <derivation name="aumentativo" suffix="ananco" table="aument"/>
    <derivation name="diminutivo" suffix="anito" table="diminut"/>
    <derivation name="diminutivo" suffix="anico" table="diminut"/>
    <derivation name="diminutivo" suffix="anillo" table="diminut"/>
    <derivation name="diminutivo" suffix="anete" table="diminut"/>
    <derivation name="diminutivo" suffix="anín" table="diminut"/>
  </table>
  <table name="N17" rads=".*"> <!-- Nieves revisar -->
    <!-- 135 members -->
    <form suffix="" tag="CMS000"/>
  </table>
  <table name="N18" rads=".*">
    <!-- 106 members -->
    <form suffix="" tag="CCN000"/>
  </table>
  <table name="N19" rads=".*">
    <!-- 104 members -->
    <form suffix="" tag="CCS000"/>
    <form suffix="es" tag="CCP000"/>
  </table>
  <table name="N20" rads=".*">
    <!-- 92 members -->
    <form suffix="és" tag="CMS000"/>
    <form suffix="esa" tag="CFS000"/>
    <form suffix="esas" tag="CFP000"/>
    <form suffix="eses" tag="CMP000"/>
  </table>
  <table name="N21" rads=".*">
    <!-- 92 members -->
    <form suffix="z" tag="CMS000"/>
    <form suffix="ces" tag="CMP000"/>
  </table>
  <table name="N22" rads=".*">
    <!-- 65 members -->
    <form suffix="" tag="CMN000"/>
    <form suffix="s" tag="CMP000"/>
  </table>
  <table name="N23" rads=".*[ctsn]i">
    <!-- 59 members -->
    <form suffix="ón" tag="CFS000"/>
    <form suffix="ones" tag="CFP000"/>
    <form suffix="ónes" tag="CFP000"/>
    <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="oncita" table="aument"/>
    <derivation name="aumentativo" suffix="onzaza" table="aument"/>
    <derivation name="aumentativo" suffix="onzaca" table="aument"/>
    <derivation name="aumentativo" suffix="onzanca" table="aument"/>
    <derivation name="diminutivo" suffix="onzota" table="diminut"/>
    <derivation name="diminutivo" suffix="oncica" table="diminut"/>
    <derivation name="diminutivo" suffix="oncilla" table="diminut"/>
    <derivation name="diminutivo" suffix="onceta" table="diminut"/>
    <!--derivation name="diminutivo" suffix="oncina" table="diminut"/-->
  </table>
  <table name="N24" rads=".*">
    <!-- 49 members -->
    <form suffix="" tag="CFS000"/>
  </table>
  <table name="N25" rads=".*én">
    <!-- 44 members -->
    <form suffix="" tag="CMS000"/>
    <form suffix="es" tag="CMP000"/>
    <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="zote" table="aument"/>
    <derivation name="aumentativo" suffix="zón" table="aument"/>
    <derivation name="aumentativo" suffix="zazo" table="aument"/>
    <derivation name="aumentativo" suffix="zaco" table="aument"/>
    <derivation name="aumentativo" suffix="zanco" table="aument"/>
    <derivation name="diminutivo" suffix="cito" table="diminut"/>
    <derivation name="diminutivo" suffix="cico" table="diminut"/>
    <derivation name="diminutivo" suffix="cillo" table="diminut"/>
    <derivation name="diminutivo" suffix="cete" table="diminut"/>
    <derivation name="diminutivo" suffix="cín" table="diminut"/>
  </table>
  <table name="N26" rads=".*">
    <!-- 42 members -->
    <form suffix="" tag="CCS000"/>
    <form suffix="es" tag="CCP000"/>
    <form suffix="s" tag="CCP000"/>
  </table>
  <table name="N27" rads=".*" fast="-">
    <!-- 27 members -->
    <form suffix="és" tag="CMS000"/>
    <form suffix="eses" tag="CMP000"/>
  </table>
  <table name="N28" rads=".*" fast="-">
    <!-- 22 members -->
    <form suffix="ín" tag="CMS000"/>
    <form suffix="ina" tag="CFS000"/>
    <form suffix="inas" tag="CFP000"/>
    <form suffix="ines" tag="CMP000"/>
  </table>
  <table name="N29" rads=".*[entou][ftnrj]" fast="-">
    <!-- 22 members -->
    <form suffix="e" tag="CMS000"/>
    <form suffix="a" tag="CFS000"/>
    <form suffix="as" tag="CFP000"/>
    <form suffix="es" tag="CMP000"/>
  </table>
  <table name="N30" rads=".*" fast="-">
    <!-- 21 members -->
    <form suffix="" tag="CMN000"/>
    <form suffix="es" tag="CMP000"/>
    <form suffix="s" tag="CMP000"/>
  </table>
  <table name="N31" rads=".*" fast="-">
    <!-- 21 members -->
    <form suffix="án" tag="CMS000"/>
    <form suffix="ana" tag="CFS000"/>
    <form suffix="anas" tag="CFP000"/>
    <form suffix="anes" tag="CMP000"/>
  </table>
  <table name="N32" rads=".*[eaé]s" fast="-">
    <!-- 20 members -->
    <form suffix="" tag="CFP000"/>
  </table>
  <table name="N33" rads=".*" fast="-"> <!-- Nieves revisar -->
    <!-- 19 members -->
    <form suffix="amen" tag="CMS000"/>
    <form suffix="ámenes" tag="CMP000"/>
  </table>
  <table name="N34" rads=".*" fast="-">
    <!-- 17 members -->
    <form suffix="ón" tag="CMS000"/>
    <form suffix="ones" tag="CMP000"/>
    <form suffix="ónes" tag="CMP000"/>
    <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="onote" table="aument"/>
    <derivation name="aumentativo" suffix="onón" table="aument"/>
    <derivation name="aumentativo" suffix="onazo" table="aument"/>
    <derivation name="aumentativo" suffix="onaco" table="aument"/>
    <derivation name="aumentativo" suffix="onanco" table="aument"/>
    <derivation name="diminutivo" suffix="oncito" table="diminut"/>
    <derivation name="diminutivo" suffix="oncico" table="diminut"/>
    <derivation name="diminutivo" suffix="oncillo" table="diminut"/>
    <derivation name="diminutivo" suffix="oncete" table="diminut"/>
    <derivation name="diminutivo" suffix="onín" table="diminut"/>
  </table>
  <table name="N35" rads=".*[ímúr]" fast="-">
    <!-- 15 members -->
    <form suffix="" tag="CMS000"/>
    <form suffix="es" tag="CMP000"/>
    <form suffix="s" tag="CMP000"/>
    <form suffix="ses" tag="CMP000"/>
  </table>
  <table name="N36" rads=".*[oeni][sa]" fast="-">
    <!-- 15 members -->
    <form suffix="" tag="CMP000"/>
  </table>
  <table name="N37" rads=".*[onrai][ndvcñ]" fast="-">
    <!-- 14 members -->
    <form suffix="és" tag="CMS000"/>
    <form suffix="esa" tag="CFS000"/>
    <form suffix="esas" tag="CFP000"/>
    <form suffix="eses" tag="CMP000"/>
    <form suffix="ésa" tag="CFS000"/>
    <form suffix="ésas" tag="CFP000"/>
    <form suffix="éses" tag="CMP000"/>
  </table>
  <table name="N38" rads=".*[íú]" fast="-">
    <!-- 14 members -->
    <form suffix="" tag="CCS000"/>
    <form suffix="es" tag="CCP000"/>
    <form suffix="s" tag="CCP000"/>
    <form suffix="ses" tag="CCP000"/>
  </table>
  <table name="N39" rads=".*" fast="-">
    <!-- 14 members -->
    <form suffix="ás" tag="CMS000"/>
    <form suffix="ases" tag="CMP000"/>
  </table>
  <table name="N40" rads=".*" fast="-">
    <!-- 12 members -->
    <form suffix="umen" tag="CMS000"/>
    <form suffix="úmenes" tag="CMP000"/>
    <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="umenote" table="aument"/>
    <derivation name="aumentativo" suffix="umenón" table="aument"/>
    <derivation name="aumentativo" suffix="umenazo" table="aument"/>
    <derivation name="aumentativo" suffix="umenaco" table="aument"/>
    <derivation name="aumentativo" suffix="umenanco" table="aument"/>
    <derivation name="diminutivo" suffix="umencito" table="diminut"/>
    <derivation name="diminutivo" suffix="umencico" table="diminut"/>
    <derivation name="diminutivo" suffix="umencillo" table="diminut"/>
    <derivation name="diminutivo" suffix="umencete" table="diminut"/>
    <derivation name="diminutivo" suffix="umenín" table="diminut"/>
  </table>
  <table name="N41" rads=".*[iaíol][spmyr]" fast="-">
    <!-- 11 members -->
    <form suffix="" tag="CMN000"/>
    <form suffix="es" tag="CMP000"/>
  </table>
  <table name="N42" rads=".*[aioe]" fast="-"> <!-- Nieves revisar -->
    <!-- 10 members -->
    <form suffix="z" tag="CCS000"/>
    <form suffix="ces" tag="CCP000"/>
  </table>
  <table name="N43" rads=".*[ioea][bsl]" fast="-">
    <!-- 10 members -->
    <form suffix="ús" tag="CMS000"/>
    <form suffix="uses" tag="CMP000"/>
  </table>
  <table name="N44" rads=".*" fast="-">
    <!-- 10 members -->
    <form suffix="ís" tag="CMS000"/>
    <form suffix="ises" tag="CMP000"/>
  </table>
  <table name="N45" rads=".*[drtbp]" fast="-">
    <!-- 10 members -->
    <form suffix="y" tag="CMS000"/>
    <form suffix="ies" tag="CMP000"/>
  </table>
  <table name="N46" rads=".*" fast="-">
    <!-- 8 members -->
    <form suffix="" tag="CFS000"/>
    <form suffix="es" tag="CFP000"/>
    <form suffix="s" tag="CFP000"/>
  </table>
  <table name="N47" rads=".*[idj]" fast="-">
    <!-- 7 members -->
    <form suffix="ós" tag="CMS000"/>
    <form suffix="oses" tag="CMP000"/>
  </table>
  <table name="N48" rads=".*[trdmv]" fast="-">
    <!-- 7 members -->
    <form suffix="án" tag="CMS000"/>
    <form suffix="anes" tag="CMP000"/>
    <form suffix="ánes" tag="CMP000"/>
  </table>
  <table name="N49" rads=".*(simon|sir|egipc|dipsoman|monoman|il)" fast="-">
    <!-- 6 members -->
    <form suffix="iaco" tag="CMS000"/>
    <form suffix="iaca" tag="CFS000"/>
    <form suffix="iacas" tag="CFP000"/>
    <form suffix="iacos" tag="CMP000"/>
    <form suffix="íaca" tag="CFS000"/>
    <form suffix="íacas" tag="CFP000"/>
    <form suffix="íacos" tag="CMP000"/>
  </table>
  <table name="N50" rads=".*(géiser|marabú|estándar|pixel|escáner|póster)" fast="-">
    <!-- 6 members -->
    <form suffix="" tag="CMS000"/>
    <form suffix="es" tag="CMP000"/>
    <form suffix="ses" tag="CMP000"/>
  </table>
  <table name="N51" rads=".*(haikú|interviú|bunker|charter|geiser|scanner)" fast="-">
    <!-- 6 members -->
    <form suffix="" tag="CMS000"/>
    <form suffix="ses" tag="CMP000"/>
  </table>
  <table name="N52" rads=".*(val|teut|lap|gasc|saj|let)" fast="-">
    <!-- 6 members -->
    <form suffix="ón" tag="CMS000"/>
    <form suffix="ona" tag="CFS000"/>
    <form suffix="onas" tag="CFP000"/>
    <form suffix="ones" tag="CMP000"/>
    <form suffix="óna" tag="CFS000"/>
    <form suffix="ónas" tag="CFP000"/>
    <form suffix="ónes" tag="CMP000"/>
  </table>
  <table name="N53" rads=".*(tamandu|tamoat|tarar|sur|pach|jacarand)" fast="-">
    <!-- 6 members -->
    <form suffix="á" tag="CMS000"/>
    <form suffix="aes" tag="CMP000"/>
    <form suffix="ás" tag="CMP000"/>
  </table>
  <table name="N54" rads=".*(zlot|rall|hobb|whisk|derb)" fast="-">
    <!-- 5 members -->
    <form suffix="y" tag="CMS000"/>
    <form suffix="ies" tag="CMP000"/>
    <form suffix="ys" tag="CMP000"/>
  </table>
  <table name="N55" rads=".*(runr|sim|procom|at|bet)" fast="-">
    <!-- 5 members -->
    <form suffix="ún" tag="CMS000"/>
    <form suffix="unes" tag="CMP000"/>
  </table>
  <table name="N56" rads=".*(maratha|sabelotodo|welter|yaro|sexsymbol)" fast="-">
    <!-- 5 members -->
    <form suffix="" tag="CCN000"/>
    <form suffix="s" tag="CCP000"/>
  </table>
  <table name="N57" rads=".*(guaimí|omaní|magrebí|kuwaití|madagascarí)" fast="-">
    <!-- 5 members -->
    <form suffix="" tag="CCS000"/>
    <form suffix="ses" tag="CCP000"/>
  </table>
  <table name="N58" rads=".*(politbur|quingomb|timb|quimbomb|yap)" fast="-">
    <!-- 5 members -->
    <form suffix="ó" tag="CMS000"/>
    <form suffix="oes" tag="CMP000"/>
    <form suffix="ós" tag="CMP000"/>
  </table>
  <table name="N59" rads=".*(sk|tlatoan|tas|teucal|teocal)" fast="-">
    <!-- 5 members -->
    <form suffix="i" tag="CMS000"/>
    <form suffix="is" tag="CMP000"/>
    <form suffix="íes" tag="CMP000"/>
  </table>
  <table name="N60" rads=".*(il|simon|egipc|dipsoman|monoman)" fast="-">
    <!-- 5 members -->
    <form suffix="íaco" tag="CMS000"/>
    <form suffix="iaca" tag="CFS000"/>
    <form suffix="iacas" tag="CFP000"/>
    <form suffix="iacos" tag="CMP000"/>
    <form suffix="íaca" tag="CFS000"/>
    <form suffix="íacas" tag="CFP000"/>
    <form suffix="íacos" tag="CMP000"/>
  </table>
  <table name="N61" rads=".*(script|scriptgirl|stagflation|vamp)" fast="-">
    <!-- 4 members -->
    <form suffix="" tag="CFN000"/>
    <form suffix="s" tag="CFP000"/>
  </table>
  <table name="N62" rads=".*(salsif|organd|telesqu|coat)" fast="-">
    <!-- 4 members -->
    <form suffix="í" tag="CMS000"/>
    <form suffix="ises" tag="CMP000"/>
    <form suffix="íes" tag="CMP000"/>
    <form suffix="ís" tag="CMP000"/>
  </table>
  <table name="N63" rads=".*(heliesqu|fedr|man|monoesqu)" fast="-">
    <!-- 4 members -->
    <form suffix="í" tag="CMS000"/>
    <form suffix="ises" tag="CMP000"/>
  </table>
  <table name="N64" rads=".*(prean|agn|velic|abd)" fast="-">
    <!-- 4 members -->
    <form suffix="omen" tag="CMS000"/>
    <form suffix="ómenes" tag="CMP000"/>
  </table>
  <table name="N65" rads=".*(inga|rajá|broker|linier)" fast="-">
    <!-- 4 members -->
    <form suffix="" tag="CCS000"/>
    <form suffix="es" tag="CMP000"/>
    <form suffix="s" tag="CCP000"/>
  </table>
  <table name="N66" rads=".*(pasqu|fort|jazm|escarp)" fast="-">
    <!-- 4 members -->
    <form suffix="ín" tag="CMS000"/>
    <form suffix="ines" tag="CMP000"/>
    <form suffix="ínes" tag="CMP000"/>
  </table>
  <table name="N67" rads=".*(claqué|mobiliario|asma|psique)" fast="-">
    <!-- 4 members -->
    <form suffix="" tag="CMS000"/>
    <form suffix="s" tag="CFP000"/>
  </table>
  <table name="N68" rads=".*(sub|super|des)" fast="-">
    <!-- 3 members -->
    <form suffix="orden" tag="CMS000"/>
    <form suffix="órdenes" tag="CMP000"/>
  </table>
  <table name="N69" rads=".*(andalu|aprendi|jue)" fast="-">
    <!-- 3 members -->
    <form suffix="z" tag="CMS000"/>
    <!--form suffix="ca" tag="CFS000"- Nieves: impossible in Spanish -->
    <!--form suffix="cas" tag="CFP000"- Nieves: impossible in Spanish -->
    <form suffix="ces" tag="CMP000"/>
    <form suffix="za" tag="CFS000"/>
    <form suffix="zas" tag="CFP000"/>
    <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="zote" table="aument"/>
    <derivation name="aumentativo" suffix="zota" table="aument"/>
    <derivation name="aumentativo" suffix="zón" table="aument"/>
    <derivation name="aumentativo" suffix="zona" table="aument"/>
    <derivation name="aumentativo" suffix="zazo" table="aument"/>
    <derivation name="aumentativo" suffix="zaza" table="aument"/>
    <derivation name="aumentativo" suffix="zaco" table="aument"/>
    <derivation name="aumentativo" suffix="zaca" table="aument"/>
    <derivation name="aumentativo" suffix="zanco" table="aument"/>
    <derivation name="aumentativo" suffix="zanca" table="aument"/>
    <derivation name="diminutivo" suffix="cito" table="diminut"/>
    <derivation name="diminutivo" suffix="cita" table="diminut"/>
    <derivation name="diminutivo" suffix="cico" table="diminut"/>
    <derivation name="diminutivo" suffix="cica" table="diminut"/>
    <derivation name="diminutivo" suffix="cillo" table="diminut"/>
    <derivation name="diminutivo" suffix="cilla" table="diminut"/>
    <derivation name="diminutivo" suffix="cete" table="diminut"/>
    <derivation name="diminutivo" suffix="ceta" table="diminut"/>
    <!--derivation name="diminutivo" suffix="cín" table="diminut"/-->
    <!--derivation name="diminutivo" suffix="cina" table="diminut"/-->
    
  </table>
  <table name="N70" rads=".*(copilot|cirujan|médic)" fast="-">
    <!-- 3 members -->
    <form suffix="o" tag="CCS000"/>
    <form suffix="a" tag="CFS000"/>
    <form suffix="as" tag="CFP000"/>
    <form suffix="os" tag="CCP000"/>
    <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="ote" table="aument"/>
    <derivation name="aumentativo" suffix="ota" table="aument"/>
    <derivation name="aumentativo" suffix="ón" table="aument"/>
    <derivation name="aumentativo" suffix="ona" table="aument"/>
    <derivation name="aumentativo" suffix="azo" table="aument"/>
    <derivation name="aumentativo" suffix="aza" table="aument"/>
    <derivation name="aumentativo" suffix="aco" table="aument"/>
    <derivation name="aumentativo" suffix="aca" table="aument"/>
    <derivation name="aumentativo" suffix="anco" table="aument"/>
    <derivation name="aumentativo" suffix="anca" table="aument"/>
    <derivation name="diminutivo" suffix="íto" table="diminut"/>
    <derivation name="diminutivo" suffix="íta" table="diminut"/>
    <derivation name="diminutivo" suffix="íco" table="diminut"/>
    <derivation name="diminutivo" suffix="íca" table="diminut"/>
    <derivation name="diminutivo" suffix="íllo" table="diminut"/>
    <derivation name="diminutivo" suffix="ílla" table="diminut"/>
    <derivation name="diminutivo" suffix="ete" table="diminut"/>
    <derivation name="diminutivo" suffix="eta" table="diminut"/>
    <!--derivation name="diminutivo" suffix="ín" table="diminut"/-->
    <!--derivation name="diminutivo" suffix="ina" table="diminut"/-->
  </table>
  <table name="N71" rads=".*(comediant|client|figurant)" fast="-">
    <!-- 3 members -->
    <form suffix="e" tag="CCS000"/>
    <form suffix="a" tag="CFS000"/>
    <form suffix="as" tag="CFP000"/>
    <form suffix="es" tag="CCP000"/>
    <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="ote" table="aument"/>
    <derivation name="aumentativo" suffix="ota" table="aument"/>
    <derivation name="aumentativo" suffix="ón" table="aument"/>
    <derivation name="aumentativo" suffix="ona" table="aument"/>
    <derivation name="aumentativo" suffix="azo" table="aument"/>
    <derivation name="aumentativo" suffix="aza" table="aument"/>
    <derivation name="aumentativo" suffix="aco" table="aument"/>
    <derivation name="aumentativo" suffix="aca" table="aument"/>
    <derivation name="aumentativo" suffix="anco" table="aument"/>
    <derivation name="aumentativo" suffix="anca" table="aument"/>
    <derivation name="diminutivo" suffix="íto" table="diminut"/>
    <derivation name="diminutivo" suffix="íta" table="diminut"/>
    <derivation name="diminutivo" suffix="íco" table="diminut"/>
    <derivation name="diminutivo" suffix="íca" table="diminut"/>
    <derivation name="diminutivo" suffix="íllo" table="diminut"/>
    <derivation name="diminutivo" suffix="ílla" table="diminut"/>
    <derivation name="diminutivo" suffix="ete" table="diminut"/>
    <derivation name="diminutivo" suffix="eta" table="diminut"/>
    <!--derivation name="diminutivo" suffix="ín" table="diminut"/-->
    <!--derivation name="diminutivo" suffix="ina" table="diminut"/-->
  </table>
  <table name="N72" rads=".*(gigoló|colibrí|canesú)" fast="-">
    <!-- 3 members -->
    <form suffix="" tag="CMS000"/>
    <form suffix="s" tag="CMP000"/>
    <form suffix="ses" tag="CMP000"/>
  </table>
  <table name="N73" rads=".*(herpe|nahuatle|chiita)" fast="-">
    <!-- 3 members -->
    <form suffix="" tag="CCS000"/>
  </table>
  <table name="N74" rads=".*(sialitizac|fotorreactivac|biorretroalimentac)" fast="-">
    <!-- 3 members -->
    <form suffix="ión" tag="CFS000"/>
    <form suffix="oónes" tag="CFP000"/>
    <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="onzota" table="aument"/>
    <derivation name="aumentativo" suffix="oncita" table="aument"/>
    <derivation name="aumentativo" suffix="onaza" table="aument"/>
    <derivation name="aumentativo" suffix="onaca" table="aument"/>
    <derivation name="aumentativo" suffix="onanca" table="aument"/>
    <derivation name="diminutivo" suffix="onota" table="diminut"/>
    <derivation name="diminutivo" suffix="oncica" table="diminut"/>
    <derivation name="diminutivo" suffix="oncilla" table="diminut"/>
    <derivation name="diminutivo" suffix="onceta" table="diminut"/>
    <!--derivation name="diminutivo" suffix="oncina" table="diminut"/-->
  </table>
  <table name="N75" rads=".*(yare|quibe|yata)" fast="-">
    <!-- 3 members -->
    <form suffix="y" tag="CMS000"/>
    <form suffix="is" tag="CMP000"/>
    <form suffix="yes" tag="CMP000"/>
    <form suffix="ys" tag="CMP000"/>
  </table>
  <table name="N76" rads=".*(vich|m|henr)" fast="-">
    <!-- 3 members -->
    <form suffix="y" tag="CMN000"/>
    <form suffix="ies" tag="CMP000"/>
  </table>
  <table name="N77" rads=".*(plexigl|anan|sut)" fast="-">
    <!-- 3 members -->
    <form suffix="ás" tag="CMN000"/>
    <form suffix="ases" tag="CMP000"/>
  </table>
  <table name="N78" rads=".*(l|h|cr)" fast="-">
    <!-- 3 members -->
    <form suffix="imen" tag="CMS000"/>
    <form suffix="ímenes" tag="CMP000"/>
   <!-- Added by Nieves -->
    <derivation name="aumentativo" suffix="imenzote" table="aument"/>
    <derivation name="aumentativo" suffix="imencito" table="aument"/>
    <derivation name="aumentativo" suffix="imenzazo" table="aument"/>
    <derivation name="aumentativo" suffix="imenzaco" table="aument"/>
    <derivation name="aumentativo" suffix="imenzanco" table="aument"/>
    <derivation name="diminutivo" suffix="imenzote" table="diminut"/>
    <derivation name="diminutivo" suffix="imencico" table="diminut"/>
    <derivation name="diminutivo" suffix="imencillo" table="diminut"/>
    <derivation name="diminutivo" suffix="imencete" table="diminut"/>
    <derivation name="diminutivo" suffix="imencín" table="diminut"/>
  </table>
  <table name="P1" rads=".*" fast="-">
    <!-- 9 members -->
    <form suffix="o" tag="I0MS000"/>
    <form suffix="a" tag="I0FS000"/>
    <form suffix="as" tag="I0FP000"/>
    <form suffix="os" tag="I0MP000"/>
  </table>
  <table name="P2" rads=".*(dónde|cómo|adónde|cuándo)" fast="-">
    <!-- 4 members -->
    <form suffix="" tag="T000000"/>
  </table>
  <table name="P3" rads=".*(adonde|donde|cuando|como)" fast="-">
    <!-- 4 members -->
    <form suffix="" tag="R000000"/>
  </table>
  <table name="R1" rads=".*">
    <!-- 104 members -->
    <form suffix="" tag="G"/>
  </table>
  <table name="S1" rads=".*">
    <!-- 30 members -->
    <form suffix="" tag="PS00"/>
  </table>
  <!-- Nieves: tabla para el verbo decir-->
  <table name="V1" rads=".*"> 
    <form suffix="decir" tag="MN0000" synt="Infinitive"/>
    <form suffix="decid" tag="MM02P0" synt="Imperative"/>
    <form suffix="decimos" tag="MIP1P0"/>
    <form suffix="decía" tag="MII1S0"/>
    <form suffix="decía" tag="MII3S0" synt="ThirSing"/>
    <form suffix="decíais" tag="MII2P0"/>
    <form suffix="decíamos" tag="MII1P0"/>
    <form suffix="decían" tag="MII3P0"/>
    <form suffix="decías" tag="MII2S0"/>
    <form suffix="decís" tag="MIP2P0"/>	
    <form suffix="di" tag="MM02S0" synt="Imperative"/>
    <form suffix="dice" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="dicen" tag="MIP3P0"/>
    <form suffix="dices" tag="MIP2S0"/>
    <form suffix="dicha" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="dichas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="dicho" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="dichos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="diciendo" tag="MG0000"/>
    <form suffix="diga" tag="MM03S0" synt="Imperative"/>
    <form suffix="diga" tag="MSP1S0"/>
    <form suffix="diga " tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="digamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="digamos" tag="MSP1P0"/>
    <form suffix="digan" tag="MM03P0" synt="Imperative"/>
    <form suffix="digan" tag="MSP3P0"/>
    <form suffix="digas" tag="MSP2S0"/>
    <form suffix="digo" tag="MIP1S0"/>
    <form suffix="digáis" tag="MSP2P0"/>
    <form suffix="dije" tag="MIS1S0"/>
    <form suffix="dijera" tag="MSI1S0"/>
    <form suffix="dijera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="dijerais" tag="MSI2P0"/>
    <form suffix="dijeran" tag="MSI3P0"/>
    <form suffix="dijeras" tag="MSI2S0"/>
    <form suffix="dijere" tag="MSF1S0"/>
    <form suffix="dijere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="dijereis" tag="MSF2P0"/>
    <form suffix="dijeren" tag="MSF3P0"/>
    <form suffix="dijeres" tag="MSF2S0"/>
    <form suffix="dijeron" tag="MIS3P0"/>
    <form suffix="dijese" tag="MSI1S0"/>
    <form suffix="dijese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="dijeseis" tag="MSI2P0"/>
    <form suffix="dijesen" tag="MSI3P0"/>
    <form suffix="dijeses" tag="MSI2S0"/>
    <form suffix="dijimos" tag="MIS1P0"/>
    <form suffix="dijiste" tag="MIS2S0"/>
    <form suffix="dijisteis" tag="MIS2P0"/>
    <form suffix="dijo" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="dijéramos" tag="MSI1P0"/>
    <form suffix="dijéremos" tag="MSF1P0"/>
    <form suffix="dijésemos" tag="MSI1P0"/>
    <form suffix="diremos" tag="MIF1P0"/>
    <form suffix="dirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="dirán" tag="MIF3P0"/>
    <form suffix="dirás" tag="MIF2S0"/>
    <form suffix="diré" tag="MIF1S0"/>
    <form suffix="diréis" tag="MIF2P0"/>
    <form suffix="diría" tag="MIC1S0"/>
    <form suffix="diría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="diríais" tag="MIC2P0"/>
    <form suffix="diríamos" tag="MIC1P0"/>
    <form suffix="dirían" tag="MIC3P0"/>
    <form suffix="dirías" tag="MIC2S0"/>
  </table> 
  <table name="V2" rads=".*"> <!-- Nieves: verbos primera congujación en infinitivo: ar -->
    <!-- 2329 members -->
    <form suffix="ar" tag="MN0000" synt="Infinitive"/> <!-- Nieves: Infinitivo -->
    <form suffix="a" tag="MIP3S0" synt="ThirdSing"/> <!-- Nieves: presente 3s -->
    <form suffix="a" tag="MM02S0" synt="Imperative"/> <!-- Nieves: Imperativo 2ª pers. sing. -->
    <form suffix="aba" tag="MII1S0"/> <!-- Nieves: pret. imp. 1s -->
    <form suffix="aba" tag="MII3S0" synt="ThirdSing"/> <!-- Nieves: pret. imp. 3s -->
    <form suffix="abais" tag="MII2P0"/> <!-- Nieves: pret. imp. 2p -->
    <form suffix="aban" tag="MII3P0"/> <!-- Nieves: pret. imp. 3p -->
    <form suffix="abas" tag="MII2S0"/> <!-- Nieves: pret. imp 2s -->
    <form suffix="ad" tag="MM02P0" synt="Imperative"/> <!-- Nieves: Imperativo 2ª pers. plural  -->
    <form suffix="ada" tag="MP00SF" synt="PastParticiple"/> 
    <form suffix="adas" tag="MP00PF" synt="PastParticiple"/> 
    <form suffix="ado" tag="MP00SM" synt="PastParticiple"/> 
    <form suffix="ados" tag="MP00PM" synt="PastParticiple"/> 
    <form suffix="amos" tag="MIP1P0"/> <!-- Nieves: presente 1p -->
    <form suffix="amos" tag="MIS1P0"/> <!-- Nieves: pret. perf. simple 1p -->
    <form suffix="an" tag="MIP3P0"/> <!-- Nieves: presente 3p-->
    <form suffix="ando" tag="MG0000"/> <!-- Nieves: gerundio -->
    <form suffix="ara" tag="MSI1S0"/> <!-- Nieves: imperfecto subjuntivo 1s -->
    <form suffix="ara" tag="MSI3S0" synt="ThirdSing"/> <!-- Nieves: imperfecto subjuntivo 3s -->
    <form suffix="arais" tag="MSI2P0"/> <!-- Nieves: imperfecto subjuntivo 2p -->
    <form suffix="aran" tag="MSI3P0"/> <!-- Nieves: imperfecto subjuntivo 3p -->
    <form suffix="aras" tag="MSI2S0"/> <!-- Nieves: imperfecto subjuntivo 2s -->
    <form suffix="are" tag="MSF1S0"/> <!-- Nieves: futuro sub. 1s -->
    <form suffix="are" tag="MSF3S0" synt="ThirdSing"/> <!-- Nieves: futuro sub. 3s  -->
    <form suffix="areis" tag="MSF2P0"/> <!-- Nieves: futuro sub. 2p -->
    <form suffix="aremos" tag="MIF1P0"/> <!-- Nieves: futuro 1p -->
    <form suffix="aren" tag="MSF3P0"/> <!-- Nieves: futuro sub. 3p -->
    <form suffix="ares" tag="MSF2S0"/> <!-- Nieves: futuro sub. 2s -->
    <form suffix="aron" tag="MIS3P0"/> <!-- Nieves: pret. perf. simple 3p -->
    <form suffix="ará" tag="MIF3S0" synt="ThirdSing"/> <!-- Nieves: futuro 3s -->
    <form suffix="arán" tag="MIF3P0"/> <!-- Nieves: futuro 3p -->
    <form suffix="arás" tag="MIF2S0"/> <!-- Nieves: futuro 2s -->
    <form suffix="aré" tag="MIF1S0"/> <!-- Nieves: futuro 1s -->
    <form suffix="aréis" tag="MIF2P0"/> <!-- Nieves: futuro 2p -->
    <form suffix="aría" tag="MIC1S0"/> <!-- Nieves: condicional 1s  -->
    <form suffix="aría" tag="MIC3S0" synt="ThirdSing"/> <!-- Nieves: condicional 3s -->
    <form suffix="aríais" tag="MIC2P0"/> <!-- Nieves: condicional 2p -->
    <form suffix="aríamos" tag="MIC1P0"/> <!-- Nieves: condicional 1p -->
    <form suffix="arían" tag="MIC3P0"/> <!-- Nieves: condicional 3p -->
    <form suffix="arías" tag="MIC2S0"/> <!-- Nieves: condicional 2s -->
    <form suffix="as" tag="MIP2S0"/> <!-- Nieves: presente 2s -->
    <form suffix="ase" tag="MSI1S0"/> <!-- Nieves: imperfecto subjuntivo2 1s -->
    <form suffix="ase" tag="MSI3S0" synt="ThirdSing"/> <!-- Nieves: imperfecto subjuntivo2 3s -->
    <form suffix="aseis" tag="MSI2P0"/> <!-- Nieves:  imperfecto subjuntivo2 2p-->
    <form suffix="asen" tag="MSI3P0"/> <!-- Nieves: imperfecto subjuntivo2 3p -->
    <form suffix="ases" tag="MSI2S0"/> <!-- Nieves: imperfecto subjuntivo 2s -->
    <form suffix="aste" tag="MIS2S0"/> <!-- Nieves: pret. perf. simple 2s -->
    <form suffix="asteis" tag="MIS2P0"/> <!-- Nieves: pret. perf. simple 2p -->
    <form suffix="e" tag="MM03S0" synt="Imperative"/> <!-- Nieves: Imperativo 3ª pers. sing. -->
    <form suffix="e" tag="MSP1S0"/> 
    <form suffix="e" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="emos" tag="MM01P0" synt="Imperative"/> <!-- Nieves: Imperativo 1ª pers. plural -->
    <form suffix="emos" tag="MSP1P0"/> 
    <form suffix="en" tag="MM03P0" synt="Imperative"/> <!-- Nieves: Imperativo 3ª pers. plural -->
    <form suffix="en" tag="MSP3P0"/> 
    <form suffix="es" tag="MSP2S0"/>
    <form suffix="o" tag="MIP1S0"/> <!-- Nieves:  presente 1s -->
    <form suffix="ábamos" tag="MII1P0"/> <!-- Nieves: pret. imp. 1p -->
    <form suffix="áis" tag="MIP2P0"/> <!-- Nieves: presente 2p -->
    <form suffix="áramos" tag="MSI1P0"/> <!-- Nieves: imperfecto subjuntivo 1p -->
    <form suffix="áremos" tag="MSF1P0"/> <!-- Nieves: futuro sub. 1p3 -->
    <form suffix="ásemos" tag="MSI1P0"/> <!-- Nieves: imperfecto subjuntivo2 1p -->
    <form suffix="é" tag="MIS1S0"/> <!-- Nieves: pret. perf. simple 1s -->
    <form suffix="éis" tag="MSP2P0"/> 
    <form suffix="ó" tag="MIS3S0" synt="ThirdSing"/> <!-- Nieves: pret. perf. simple 3s -->
  </table> 
  <table name="V3" rads=".*"> <!-- Nieves: verbos primera congujación en infinitivo: z+ar -->
    <!-- 351 members -->
    <form suffix="zar" tag="MN0000" synt="Infinitive"/>
    <form suffix="ce" tag="MM03S0" synt="Imperative"/>
    <form suffix="ce" tag="MSP1S0"/>
    <form suffix="ce" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="cemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="cemos" tag="MSP1P0"/>
    <form suffix="cen" tag="MM03P0" synt="Imperative"/>
    <form suffix="cen" tag="MSP3P0"/>
    <form suffix="ces" tag="MSP2S0"/>
    <form suffix="cé" tag="MIS1S0"/>
    <form suffix="céis" tag="MSP2P0"/>
    <form suffix="za" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="za" tag="MM02S0" synt="Imperative"/>
    <form suffix="zaba" tag="MII1S0"/>
    <form suffix="zaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="zabais" tag="MII2P0"/>
    <form suffix="zaban" tag="MII3P0"/>
    <form suffix="zabas" tag="MII2S0"/>
    <form suffix="zad" tag="MM02P0" synt="Imperative"/>
    <form suffix="zada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="zadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="zado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="zados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="zamos" tag="MIP1P0"/>
    <form suffix="zamos" tag="MIS1P0"/>
    <form suffix="zan" tag="MIP3P0"/>
    <form suffix="zando" tag="MG0000"/>
    <form suffix="zara" tag="MSI1S0"/>
    <form suffix="zara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="zarais" tag="MSI2P0"/>
    <form suffix="zaran" tag="MSI3P0"/>
    <form suffix="zaras" tag="MSI2S0"/>
    <form suffix="zare" tag="MSF1S0"/>
    <form suffix="zare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="zareis" tag="MSF2P0"/>
    <form suffix="zaremos" tag="MIF1P0"/>
    <form suffix="zaren" tag="MSF3P0"/>
    <form suffix="zares" tag="MSF2S0"/>
    <form suffix="zaron" tag="MIS3P0"/>
    <form suffix="zará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="zarán" tag="MIF3P0"/>
    <form suffix="zarás" tag="MIF2S0"/>
    <form suffix="zaré" tag="MIF1S0"/>
    <form suffix="zaréis" tag="MIF2P0"/>
    <form suffix="zaría" tag="MIC1S0"/>
    <form suffix="zaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="zaríais" tag="MIC2P0"/>
    <form suffix="zaríamos" tag="MIC1P0"/>
    <form suffix="zarían" tag="MIC3P0"/>
    <form suffix="zarías" tag="MIC2S0"/>
    <form suffix="zas" tag="MIP2S0"/>
    <form suffix="zase" tag="MSI1S0"/>
    <form suffix="zase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="zaseis" tag="MSI2P0"/>
    <form suffix="zasen" tag="MSI3P0"/>
    <form suffix="zases" tag="MSI2S0"/>
    <form suffix="zaste" tag="MIS2S0"/>
    <form suffix="zasteis" tag="MIS2P0"/>
    <form suffix="zo" tag="MIP1S0"/>
    <form suffix="zábamos" tag="MII1P0"/>
    <form suffix="záis" tag="MIP2P0"/>
    <form suffix="záramos" tag="MSI1P0"/>
    <form suffix="záremos" tag="MSF1P0"/>
    <form suffix="zásemos" tag="MSI1P0"/>
    <form suffix="zó" tag="MIS3S0" synt="ThirdSing"/>
  </table>
  <table name="V4" rads=".*"> <!-- Nieves: verbos primera congujación en infinitivo: c+ar -->
    <form suffix="car" tag="MN0000" synt="Infinitive"/>
    <form suffix="ca" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ca" tag="MM02S0" synt="Imperative"/>
    <form suffix="caba" tag="MII1S0"/>
    <form suffix="caba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="cabais" tag="MII2P0"/>
    <form suffix="caban" tag="MII3P0"/>
    <form suffix="cabas" tag="MII2S0"/>
    <form suffix="cad" tag="MM02P0" synt="Imperative"/>
    <form suffix="cada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="cadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="cado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="cados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="camos" tag="MIP1P0"/>
    <form suffix="camos" tag="MIS1P0"/>
    <form suffix="can" tag="MIP3P0"/>
    <form suffix="cando" tag="MG0000"/>
    <form suffix="cara" tag="MSI1S0"/>
    <form suffix="cara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="carais" tag="MSI2P0"/>
    <form suffix="caran" tag="MSI3P0"/>
    <form suffix="caras" tag="MSI2S0"/>
    <form suffix="care" tag="MSF1S0"/>
    <form suffix="care" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="careis" tag="MSF2P0"/>
    <form suffix="caremos" tag="MIF1P0"/>
    <form suffix="caren" tag="MSF3P0"/>
    <form suffix="cares" tag="MSF2S0"/>
    <form suffix="caron" tag="MIS3P0"/>
    <form suffix="cará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="carán" tag="MIF3P0"/>
    <form suffix="carás" tag="MIF2S0"/>
    <form suffix="caré" tag="MIF1S0"/>
    <form suffix="caréis" tag="MIF2P0"/>
    <form suffix="caría" tag="MIC1S0"/>
    <form suffix="caría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="caríais" tag="MIC2P0"/>
    <form suffix="caríamos" tag="MIC1P0"/>
    <form suffix="carían" tag="MIC3P0"/>
    <form suffix="carías" tag="MIC2S0"/>
    <form suffix="cas" tag="MIP2S0"/>
    <form suffix="case" tag="MSI1S0"/>
    <form suffix="case" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="caseis" tag="MSI2P0"/>
    <form suffix="casen" tag="MSI3P0"/>
    <form suffix="cases" tag="MSI2S0"/>
    <form suffix="caste" tag="MIS2S0"/>
    <form suffix="casteis" tag="MIS2P0"/>
    <form suffix="co" tag="MIP1S0"/>
    <form suffix="cábamos" tag="MII1P0"/>
    <form suffix="cáis" tag="MIP2P0"/>
    <form suffix="cáramos" tag="MSI1P0"/>
    <form suffix="cáremos" tag="MSF1P0"/>
    <form suffix="cásemos" tag="MSI1P0"/>
    <form suffix="có" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="que" tag="MM03S0" synt="Imperative"/>
    <form suffix="que" tag="MSP1S0"/>
    <form suffix="que" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="quemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="quemos" tag="MSP1P0"/>
    <form suffix="quen" tag="MM03P0" synt="Imperative"/>
    <form suffix="quen" tag="MSP3P0"/>
    <form suffix="ques" tag="MSP2S0"/>
    <form suffix="qué" tag="MIS1S0"/>
    <form suffix="quéis" tag="MSP2P0"/>
  </table>
    <!-- Nieves: tabla para los verbos terminados en decir-->
  <table name="V5" rads=".*[ben|mal]"> 
    <form suffix="decir" tag="MN0000" synt="Infinitive"/>
    <form suffix="decid" tag="MM02P0" synt="Imperative"/>
    <form suffix="decimos" tag="MIP1P0"/>
    <form suffix="decía" tag="MII1S0"/>
    <form suffix="decía" tag="MII3S0" synt="ThirSing"/>
    <form suffix="decíais" tag="MII2P0"/>
    <form suffix="decíamos" tag="MII1P0"/>
    <form suffix="decían" tag="MII3P0"/>
    <form suffix="decías" tag="MII2S0"/>
    <form suffix="decís" tag="MIP2P0"/>	
    <form suffix="dice" tag="MM02S0" synt="Imperative"/>
    <form suffix="dice" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="dicen" tag="MIP3P0"/>
    <form suffix="dices" tag="MIP2S0"/>
    <form suffix="dita" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ditas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="dito" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ditos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="decida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="decidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="decido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="decidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="diciendo" tag="MG0000"/>
    <form suffix="diga" tag="MM03S0" synt="Imperative"/>
    <form suffix="diga" tag="MSP1S0"/>
    <form suffix="diga " tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="digamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="digamos" tag="MSP1P0"/>
    <form suffix="digan" tag="MM03P0" synt="Imperative"/>
    <form suffix="digan" tag="MSP3P0"/>
    <form suffix="digas" tag="MSP2S0"/>
    <form suffix="digo" tag="MIP1S0"/>
    <form suffix="digáis" tag="MSP2P0"/>
    <form suffix="dije" tag="MIS1S0"/>
    <form suffix="dijera" tag="MSI1S0"/>
    <form suffix="dijera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="dijerais" tag="MSI2P0"/>
    <form suffix="dijeran" tag="MSI3P0"/>
    <form suffix="dijeras" tag="MSI2S0"/>
    <form suffix="dijere" tag="MSF1S0"/>
    <form suffix="dijere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="dijereis" tag="MSF2P0"/>
    <form suffix="dijeren" tag="MSF3P0"/>
    <form suffix="dijeres" tag="MSF2S0"/>
    <form suffix="dijeron" tag="MIS3P0"/>
    <form suffix="dijese" tag="MSI1S0"/>
    <form suffix="dijese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="dijeseis" tag="MSI2P0"/>
    <form suffix="dijesen" tag="MSI3P0"/>
    <form suffix="dijeses" tag="MSI2S0"/>
    <form suffix="dijimos" tag="MIS1P0"/>
    <form suffix="dijiste" tag="MIS2S0"/>
    <form suffix="dijisteis" tag="MIS2P0"/>
    <form suffix="dijo" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="dijéramos" tag="MSI1P0"/>
    <form suffix="dijéremos" tag="MSF1P0"/>
    <form suffix="dijésemos" tag="MSI1P0"/>
    <form suffix="deciremos" tag="MIF1P0"/>
    <form suffix="decirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="decirán" tag="MIF3P0"/>
    <form suffix="decirás" tag="MIF2S0"/>
    <form suffix="deciré" tag="MIF1S0"/>
    <form suffix="deciréis" tag="MIF2P0"/>
    <form suffix="deciría" tag="MIC1S0"/>
    <form suffix="deciría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="deciríais" tag="MIC2P0"/>
    <form suffix="deciríamos" tag="MIC1P0"/>
    <form suffix="decirían" tag="MIC3P0"/>
    <form suffix="decirías" tag="MIC2S0"/>
  </table> 
  <table name="V6" rads=".*[des|con|contra|re|ante|pre|entre]"> 
    <form suffix="decir" tag="MN0000" synt="Infinitive"/>
    <form suffix="decid" tag="MM02P0" synt="Imperative"/>
    <form suffix="decimos" tag="MIP1P0"/>
    <form suffix="decía" tag="MII1S0"/>
    <form suffix="decía" tag="MII3S0" synt="ThirSing"/>
    <form suffix="decíais" tag="MII2P0"/>
    <form suffix="decíamos" tag="MII1P0"/>
    <form suffix="decían" tag="MII3P0"/>
    <form suffix="decías" tag="MII2S0"/>
    <form suffix="decís" tag="MIP2P0"/>	
    <form suffix="dice" tag="MM02S0" synt="Imperative"/>
    <form suffix="dice" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="dicen" tag="MIP3P0"/>
    <form suffix="dices" tag="MIP2S0"/>
    <form suffix="dicha" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="dichas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="dicho" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="dichos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="diciendo" tag="MG0000"/>
    <form suffix="diga" tag="MM03S0" synt="Imperative"/>
    <form suffix="diga" tag="MSP1S0"/>
    <form suffix="diga " tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="digamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="digamos" tag="MSP1P0"/>
    <form suffix="digan" tag="MM03P0" synt="Imperative"/>
    <form suffix="digan" tag="MSP3P0"/>
    <form suffix="digas" tag="MSP2S0"/>
    <form suffix="digo" tag="MIP1S0"/>
    <form suffix="digáis" tag="MSP2P0"/>
    <form suffix="dije" tag="MIS1S0"/>
    <form suffix="dijera" tag="MSI1S0"/>
    <form suffix="dijera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="dijerais" tag="MSI2P0"/>
    <form suffix="dijeran" tag="MSI3P0"/>
    <form suffix="dijeras" tag="MSI2S0"/>
    <form suffix="dijere" tag="MSF1S0"/>
    <form suffix="dijere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="dijereis" tag="MSF2P0"/>
    <form suffix="dijeren" tag="MSF3P0"/>
    <form suffix="dijeres" tag="MSF2S0"/>
    <form suffix="dijeron" tag="MIS3P0"/>
    <form suffix="dijese" tag="MSI1S0"/>
    <form suffix="dijese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="dijeseis" tag="MSI2P0"/>
    <form suffix="dijesen" tag="MSI3P0"/>
    <form suffix="dijeses" tag="MSI2S0"/>
    <form suffix="dijimos" tag="MIS1P0"/>
    <form suffix="dijiste" tag="MIS2S0"/>
    <form suffix="dijisteis" tag="MIS2P0"/>
    <form suffix="dijo" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="dijéramos" tag="MSI1P0"/>
    <form suffix="dijéremos" tag="MSF1P0"/>
    <form suffix="dijésemos" tag="MSI1P0"/>
    <form suffix="deciremos" tag="MIF1P0"/>
    <form suffix="decirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="decirán" tag="MIF3P0"/>
    <form suffix="decirás" tag="MIF2S0"/>
    <form suffix="deciré" tag="MIF1S0"/>
    <form suffix="deciréis" tag="MIF2P0"/>
    <form suffix="deciría" tag="MIC1S0"/>
    <form suffix="deciría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="deciríais" tag="MIC2P0"/>
    <form suffix="deciríamos" tag="MIC1P0"/>
    <form suffix="decirían" tag="MIC3P0"/>
    <form suffix="decirías" tag="MIC2S0"/>
  </table> 
  <table name="V8" rads=".*"> <!-- Nieves: verbos tercera conjugación en infinitivo: ir -->
    <!-- 134 members -->
    <form suffix="ir" tag="MN0000" synt="Infinitive"/>
    <form suffix="a" tag="MM03S0" synt="Imperative"/>
    <form suffix="a" tag="MSP1S0"/>
    <form suffix="a" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="amos" tag="MM01P0" synt="Imperative"/>
    <form suffix="amos" tag="MSP1P0"/>
    <form suffix="an" tag="MM03P0" synt="Imperative"/>
    <form suffix="an" tag="MSP3P0"/>
    <form suffix="as" tag="MSP2S0"/>
    <form suffix="e" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="e" tag="MM02S0" synt="Imperative"/>
    <form suffix="en" tag="MIP3P0"/>
    <form suffix="es" tag="MIP2S0"/>
    <form suffix="id" tag="MM02P0" synt="Imperative"/>
    <form suffix="ida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="idas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="idos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="iendo" tag="MG0000"/>
    <form suffix="iera" tag="MSI1S0"/>
    <form suffix="iera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ierais" tag="MSI2P0"/>
    <form suffix="ieran" tag="MSI3P0"/>
    <form suffix="ieras" tag="MSI2S0"/>
    <form suffix="iere" tag="MSF1S0"/>
    <form suffix="iere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="iereis" tag="MSF2P0"/>
    <form suffix="ieren" tag="MSF3P0"/>
    <form suffix="ieres" tag="MSF2S0"/>
    <form suffix="ieron" tag="MIS3P0"/>
    <form suffix="iese" tag="MSI1S0"/>
    <form suffix="iese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ieseis" tag="MSI2P0"/>
    <form suffix="iesen" tag="MSI3P0"/>
    <form suffix="ieses" tag="MSI2S0"/>
    <form suffix="imos" tag="MIP1P0"/>
    <form suffix="imos" tag="MIS1P0"/>
    <form suffix="iremos" tag="MIF1P0"/>
    <form suffix="irá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="irán" tag="MIF3P0"/>
    <form suffix="irás" tag="MIF2S0"/>
    <form suffix="iré" tag="MIF1S0"/>
    <form suffix="iréis" tag="MIF2P0"/>
    <form suffix="iría" tag="MIC1S0"/>
    <form suffix="iría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="iríais" tag="MIC2P0"/>
    <form suffix="iríamos" tag="MIC1P0"/>
    <form suffix="irían" tag="MIC3P0"/>
    <form suffix="irías" tag="MIC2S0"/>
    <form suffix="iste" tag="MIS2S0"/>
    <form suffix="isteis" tag="MIS2P0"/>
    <form suffix="iéramos" tag="MSI1P0"/>
    <form suffix="iéremos" tag="MSF1P0"/>
    <form suffix="iésemos" tag="MSI1P0"/>
    <form suffix="ió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="o" tag="MIP1S0"/>
    <form suffix="áis" tag="MSP2P0"/>
    <form suffix="í" tag="MIS1S0"/>
    <form suffix="ía" tag="MII1S0"/>
    <form suffix="ía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="íais" tag="MII2P0"/>
    <form suffix="íamos" tag="MII1P0"/>
    <form suffix="ían" tag="MII3P0"/>
    <form suffix="ías" tag="MII2S0"/>
    <form suffix="ís" tag="MIP2P0"/>
  </table>
  <table name="V9" rads=".*g"> <!-- Nieves: verbos primera congujación en infinitivo: g+ar -->
    <!-- 114 members -->
    <form suffix="ar" tag="MN0000" synt="Infinitive"/>
    <form suffix="a" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="a" tag="MM02S0" synt="Imperative"/>
    <form suffix="aba" tag="MII1S0"/>
    <form suffix="aba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="abais" tag="MII2P0"/>
    <form suffix="aban" tag="MII3P0"/>
    <form suffix="abas" tag="MII2S0"/>
    <form suffix="ad" tag="MM02P0" synt="Imperative"/>
    <form suffix="ada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="adas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="amos" tag="MIP1P0"/>
    <form suffix="amos" tag="MIS1P0"/>
    <form suffix="an" tag="MIP3P0"/>
    <form suffix="ando" tag="MG0000"/>
    <form suffix="ara" tag="MSI1S0"/>
    <form suffix="ara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="arais" tag="MSI2P0"/>
    <form suffix="aran" tag="MSI3P0"/>
    <form suffix="aras" tag="MSI2S0"/>
    <form suffix="are" tag="MSF1S0"/>
    <form suffix="are" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="areis" tag="MSF2P0"/>
    <form suffix="aremos" tag="MIF1P0"/>
    <form suffix="aren" tag="MSF3P0"/>
    <form suffix="ares" tag="MSF2S0"/>
    <form suffix="aron" tag="MIS3P0"/>
    <form suffix="ará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="arán" tag="MIF3P0"/>
    <form suffix="arás" tag="MIF2S0"/>
    <form suffix="aré" tag="MIF1S0"/>
    <form suffix="aréis" tag="MIF2P0"/>
    <form suffix="aría" tag="MIC1S0"/>
    <form suffix="aría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="aríais" tag="MIC2P0"/>
    <form suffix="aríamos" tag="MIC1P0"/>
    <form suffix="arían" tag="MIC3P0"/>
    <form suffix="arías" tag="MIC2S0"/>
    <form suffix="as" tag="MIP2S0"/>
    <form suffix="ase" tag="MSI1S0"/>
    <form suffix="ase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="aseis" tag="MSI2P0"/>
    <form suffix="asen" tag="MSI3P0"/>
    <form suffix="ases" tag="MSI2S0"/>
    <form suffix="aste" tag="MIS2S0"/>
    <form suffix="asteis" tag="MIS2P0"/>
    <form suffix="o" tag="MIP1S0"/>
    <form suffix="ue" tag="MM03S0" synt="Imperative"/>
    <form suffix="ue" tag="MSP1S0"/>
    <form suffix="ue" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="uemos" tag="MSP1P0"/>
    <form suffix="uen" tag="MM03P0" synt="Imperative"/>
    <form suffix="uen" tag="MSP3P0"/>
    <form suffix="ues" tag="MSP2S0"/>
    <form suffix="ué" tag="MIS1S0"/>
    <form suffix="uéis" tag="MSP2P0"/>
    <form suffix="ábamos" tag="MII1P0"/>
    <form suffix="áis" tag="MIP2P0"/>
    <form suffix="áramos" tag="MSI1P0"/>
    <form suffix="áremos" tag="MSF1P0"/>
    <form suffix="ásemos" tag="MSI1P0"/>
    <form suffix="ó" tag="MIS3S0" synt="ThirdSing"/>
  </table>
  <table name="V10" rads=".*[eao]"> <!-- Nieves: verbos segunda conjugación en infinitivo: e/a/o+cer -->
    <!-- 97 members -->
    <form suffix="cer" tag="MN0000" synt="Infinitive"/>
    <form suffix="ce" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ce" tag="MM02S0" synt="Imperative"/>
    <form suffix="ced" tag="MM02P0" synt="Imperative"/>
    <form suffix="cemos" tag="MIP1P0"/>
    <form suffix="cen" tag="MIP3P0"/>
    <form suffix="ceremos" tag="MIF1P0"/>
    <form suffix="cerá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="cerán" tag="MIF3P0"/>
    <form suffix="cerás" tag="MIF2S0"/>
    <form suffix="ceré" tag="MIF1S0"/>
    <form suffix="ceréis" tag="MIF2P0"/>
    <form suffix="cería" tag="MIC1S0"/>
    <form suffix="cería" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ceríais" tag="MIC2P0"/>
    <form suffix="ceríamos" tag="MIC1P0"/>
    <form suffix="cerían" tag="MIC3P0"/>
    <form suffix="cerías" tag="MIC2S0"/>
    <form suffix="ces" tag="MIP2S0"/>
    <form suffix="cida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="cidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="cido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="cidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ciendo" tag="MG0000"/>
    <form suffix="ciera" tag="MSI1S0"/>
    <form suffix="ciera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="cierais" tag="MSI2P0"/>
    <form suffix="cieran" tag="MSI3P0"/>
    <form suffix="cieras" tag="MSI2S0"/>
    <form suffix="ciere" tag="MSF1S0"/>
    <form suffix="ciere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ciereis" tag="MSF2P0"/>
    <form suffix="cieren" tag="MSF3P0"/>
    <form suffix="cieres" tag="MSF2S0"/>
    <form suffix="cieron" tag="MIS3P0"/>
    <form suffix="ciese" tag="MSI1S0"/>
    <form suffix="ciese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="cieseis" tag="MSI2P0"/>
    <form suffix="ciesen" tag="MSI3P0"/>
    <form suffix="cieses" tag="MSI2S0"/>
    <form suffix="cimos" tag="MIS1P0"/>
    <form suffix="ciste" tag="MIS2S0"/>
    <form suffix="cisteis" tag="MIS2P0"/>
    <form suffix="ciéramos" tag="MSI1P0"/>
    <form suffix="ciéremos" tag="MSF1P0"/>
    <form suffix="ciésemos" tag="MSI1P0"/>
    <form suffix="ció" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="céis" tag="MIP2P0"/>
    <form suffix="cí" tag="MIS1S0"/>
    <form suffix="cía" tag="MII1S0"/>
    <form suffix="cía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="cíais" tag="MII2P0"/>
    <form suffix="cíamos" tag="MII1P0"/>
    <form suffix="cían" tag="MII3P0"/>
    <form suffix="cías" tag="MII2S0"/>
    <form suffix="zca" tag="MM03S0" synt="Imperative"/>
    <form suffix="zca" tag="MSP1S0"/>
    <form suffix="zca" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="zcamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="zcamos" tag="MSP1P0"/>
    <form suffix="zcan" tag="MM03P0" synt="Imperative"/>
    <form suffix="zcan" tag="MSP3P0"/>
    <form suffix="zcas" tag="MSP2S0"/>
    <form suffix="zco" tag="MIP1S0"/>
    <form suffix="zcáis" tag="MSP2P0"/>
  </table>
  <table name="V12" rads=".*"> <!-- Nieves: verbos segunda conjugación en infinitivo: er -->
    <!-- 67 members -->
    <form suffix="er" tag="MN0000" synt="Infinitive"/>
    <form suffix="a" tag="MM03S0" synt="Imperative"/>
    <form suffix="a" tag="MSP1S0"/>
    <form suffix="a" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="amos" tag="MM01P0" synt="Imperative"/>
    <form suffix="amos" tag="MSP1P0"/>
    <form suffix="an" tag="MM03P0" synt="Imperative"/>
    <form suffix="an" tag="MSP3P0"/>
    <form suffix="as" tag="MSP2S0"/>
    <form suffix="e" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="e" tag="MM02S0" synt="Imperative"/>
    <form suffix="ed" tag="MM02P0" synt="Imperative"/>
    <form suffix="emos" tag="MIP1P0"/>
    <form suffix="en" tag="MIP3P0"/>
    <form suffix="eremos" tag="MIF1P0"/>
    <form suffix="erá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="erán" tag="MIF3P0"/>
    <form suffix="erás" tag="MIF2S0"/>
    <form suffix="eré" tag="MIF1S0"/>
    <form suffix="eréis" tag="MIF2P0"/>
    <form suffix="ería" tag="MIC1S0"/>
    <form suffix="ería" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="eríais" tag="MIC2P0"/>
    <form suffix="eríamos" tag="MIC1P0"/>
    <form suffix="erían" tag="MIC3P0"/>
    <form suffix="erías" tag="MIC2S0"/>
    <form suffix="es" tag="MIP2S0"/>
    <form suffix="ida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="idas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="idos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="iendo" tag="MG0000"/>
    <form suffix="iera" tag="MSI1S0"/>
    <form suffix="iera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ierais" tag="MSI2P0"/>
    <form suffix="ieran" tag="MSI3P0"/>
    <form suffix="ieras" tag="MSI2S0"/>
    <form suffix="iere" tag="MSF1S0"/>
    <form suffix="iere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="iereis" tag="MSF2P0"/>
    <form suffix="ieren" tag="MSF3P0"/>
    <form suffix="ieres" tag="MSF2S0"/>
    <form suffix="ieron" tag="MIS3P0"/>
    <form suffix="iese" tag="MSI1S0"/>
    <form suffix="iese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ieseis" tag="MSI2P0"/>
    <form suffix="iesen" tag="MSI3P0"/>
    <form suffix="ieses" tag="MSI2S0"/>
    <form suffix="imos" tag="MIS1P0"/>
    <form suffix="iste" tag="MIS2S0"/>
    <form suffix="isteis" tag="MIS2P0"/>
    <form suffix="iéramos" tag="MSI1P0"/>
    <form suffix="iéremos" tag="MSF1P0"/>
    <form suffix="iésemos" tag="MSI1P0"/>
    <form suffix="ió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="o" tag="MIP1S0"/>
    <form suffix="áis" tag="MSP2P0"/>
    <form suffix="éis" tag="MIP2P0"/>
    <form suffix="í" tag="MIS1S0"/>
    <form suffix="ía" tag="MII1S0"/>
    <form suffix="ía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="íais" tag="MII2P0"/>
    <form suffix="íamos" tag="MII1P0"/>
    <form suffix="ían" tag="MII3P0"/>
    <form suffix="ías" tag="MII2S0"/>
  </table>
  <table name="V13" rads=".*"> <!-- Nieves: verbos primera conjugación en infinitivo: iar -->
    <!-- 50 members -->
    <form suffix="iar" tag="MN0000" synt="Infinitive"/>
    <form suffix="iaba" tag="MII1S0"/>
    <form suffix="iaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="iabais" tag="MII2P0"/>
    <form suffix="iaban" tag="MII3P0"/>
    <form suffix="iabas" tag="MII2S0"/>
    <form suffix="iad" tag="MM02P0" synt="Imperative"/>
    <form suffix="iada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="iadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="iado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="iados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="iamos" tag="MIP1P0"/>
    <form suffix="iamos" tag="MIS1P0"/>
    <form suffix="iando" tag="MG0000"/>
    <form suffix="iara" tag="MSI1S0"/>
    <form suffix="iara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="iarais" tag="MSI2P0"/>
    <form suffix="iaran" tag="MSI3P0"/>
    <form suffix="iaras" tag="MSI2S0"/>
    <form suffix="iare" tag="MSF1S0"/>
    <form suffix="iare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="iareis" tag="MSF2P0"/>
    <form suffix="iaremos" tag="MIF1P0"/>
    <form suffix="iaren" tag="MSF3P0"/>
    <form suffix="iares" tag="MSF2S0"/>
    <form suffix="iaron" tag="MIS3P0"/>
    <form suffix="iará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="iarán" tag="MIF3P0"/>
    <form suffix="iarás" tag="MIF2S0"/>
    <form suffix="iaré" tag="MIF1S0"/>
    <form suffix="iaréis" tag="MIF2P0"/>
    <form suffix="iaría" tag="MIC1S0"/>
    <form suffix="iaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="iaríais" tag="MIC2P0"/>
    <form suffix="iaríamos" tag="MIC1P0"/>
    <form suffix="iarían" tag="MIC3P0"/>
    <form suffix="iarías" tag="MIC2S0"/>
    <form suffix="iase" tag="MSI1S0"/>
    <form suffix="iase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="iaseis" tag="MSI2P0"/>
    <form suffix="iasen" tag="MSI3P0"/>
    <form suffix="iases" tag="MSI2S0"/>
    <form suffix="iaste" tag="MIS2S0"/>
    <form suffix="iasteis" tag="MIS2P0"/>
    <form suffix="iemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="iemos" tag="MSP1P0"/>
    <form suffix="iábamos" tag="MII1P0"/>
    <form suffix="iáis" tag="MIP2P0"/>
    <form suffix="iáramos" tag="MSI1P0"/>
    <form suffix="iáremos" tag="MSF1P0"/>
    <form suffix="iásemos" tag="MSI1P0"/>
    <form suffix="ié" tag="MIS1S0"/>
    <form suffix="iéis" tag="MSP2P0"/>
    <form suffix="ió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ía" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ía" tag="MM02S0" synt="Imperative"/>
    <form suffix="ían" tag="MIP3P0"/>
    <form suffix="ías" tag="MIP2S0"/>
    <form suffix="íe" tag="MM03S0" synt="Imperative"/>
    <form suffix="íe" tag="MSP1S0"/>
    <form suffix="íe" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="íen" tag="MM03P0" synt="Imperative"/>
    <form suffix="íen" tag="MSP3P0"/>
    <form suffix="íes" tag="MSP2S0"/>
    <form suffix="ío" tag="MIP1S0"/>
  </table>
  <table name="V15" rads=".*u">  <!-- Nieves: verbos tercera conjugación en infinitivo: uir -->
    <!-- 33 members -->
    <form suffix="ir" tag="MN0000" synt="Infinitive"/>
    <form suffix="id" tag="MM02P0" synt="Imperative"/>
    <form suffix="ida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="idas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="idos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="imos" tag="MIP1P0"/>
    <form suffix="imos" tag="MIS1P0"/>
    <form suffix="iremos" tag="MIF1P0"/>
    <form suffix="irá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="irán" tag="MIF3P0"/>
    <form suffix="irás" tag="MIF2S0"/>
    <form suffix="iré" tag="MIF1S0"/>
    <form suffix="iréis" tag="MIF2P0"/>
    <form suffix="iría" tag="MIC1S0"/>
    <form suffix="iría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="iríais" tag="MIC2P0"/>
    <form suffix="iríamos" tag="MIC1P0"/>
    <form suffix="irían" tag="MIC3P0"/>
    <form suffix="irías" tag="MIC2S0"/>
    <form suffix="iste" tag="MIS2S0"/>
    <form suffix="isteis" tag="MIS2P0"/>
    <form suffix="ya" tag="MM03S0" synt="Imperative"/>
    <form suffix="ya" tag="MSP1S0"/>
    <form suffix="ya" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="yamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="yamos" tag="MSP1P0"/>
    <form suffix="yan" tag="MM03P0" synt="Imperative"/>
    <form suffix="yan" tag="MSP3P0"/>
    <form suffix="yas" tag="MSP2S0"/>
    <form suffix="ye" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ye" tag="MM02S0" synt="Imperative"/>
    <form suffix="yen" tag="MIP3P0"/>
    <form suffix="yendo" tag="MG0000"/>
    <form suffix="yera" tag="MSI1S0"/>
    <form suffix="yera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="yerais" tag="MSI2P0"/>
    <form suffix="yeran" tag="MSI3P0"/>
    <form suffix="yeras" tag="MSI2S0"/>
    <form suffix="yere" tag="MSF1S0"/>
    <form suffix="yere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="yereis" tag="MSF2P0"/>
    <form suffix="yeren" tag="MSF3P0"/>
    <form suffix="yeres" tag="MSF2S0"/>
    <form suffix="yeron" tag="MIS3P0"/>
    <form suffix="yes" tag="MIP2S0"/>
    <form suffix="yese" tag="MSI1S0"/>
    <form suffix="yese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="yeseis" tag="MSI2P0"/>
    <form suffix="yesen" tag="MSI3P0"/>
    <form suffix="yeses" tag="MSI2S0"/>
    <form suffix="yo" tag="MIP1S0"/>
    <form suffix="yáis" tag="MSP2P0"/>
    <form suffix="yéramos" tag="MSI1P0"/>
    <form suffix="yéremos" tag="MSF1P0"/>
    <form suffix="yésemos" tag="MSI1P0"/>
    <form suffix="yó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="í" tag="MIS1S0"/>
    <form suffix="ía" tag="MII1S0"/>
    <form suffix="ía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="íais" tag="MII2P0"/>
    <form suffix="íamos" tag="MII1P0"/>
    <form suffix="ían" tag="MII3P0"/>
    <form suffix="ías" tag="MII2S0"/>
    <form suffix="ís" tag="MIP2P0"/>
  </table>
  <table name="V16" rads=".*" fast="-">  <!-- Nieves: verbos primera conjugación en infinitivo: iar -->
    <!-- 27 members -->
    <form suffix="iar" tag="MN0000" synt="Infinitive"/>
    <form suffix="iaba" tag="MII1S0"/>
    <form suffix="iaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="iabais" tag="MII2P0"/>
    <form suffix="iaban" tag="MII3P0"/>
    <form suffix="iabas" tag="MII2S0"/>
    <form suffix="iad" tag="MM02P0" synt="Imperative"/>
    <form suffix="iada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="iadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="iado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="iados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="iamos" tag="MIP1P0"/>
    <form suffix="iamos" tag="MIS1P0"/>
    <form suffix="iando" tag="MG0000"/>
    <form suffix="iara" tag="MSI1S0"/>
    <form suffix="iara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="iarais" tag="MSI2P0"/>
    <form suffix="iaran" tag="MSI3P0"/>
    <form suffix="iaras" tag="MSI2S0"/>
    <form suffix="iare" tag="MSF1S0"/>
    <form suffix="iare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="iareis" tag="MSF2P0"/>
    <form suffix="iaremos" tag="MIF1P0"/>
    <form suffix="iaren" tag="MSF3P0"/>
    <form suffix="iares" tag="MSF2S0"/>
    <form suffix="iaron" tag="MIS3P0"/>
    <form suffix="iará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="iarán" tag="MIF3P0"/>
    <form suffix="iarás" tag="MIF2S0"/>
    <form suffix="iaré" tag="MIF1S0"/>
    <form suffix="iaréis" tag="MIF2P0"/>
    <form suffix="iaría" tag="MIC1S0"/>
    <form suffix="iaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="iaríais" tag="MIC2P0"/>
    <form suffix="iaríamos" tag="MIC1P0"/>
    <form suffix="iarían" tag="MIC3P0"/>
    <form suffix="iarías" tag="MIC2S0"/>
    <form suffix="iase" tag="MSI1S0"/>
    <form suffix="iase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="iaseis" tag="MSI2P0"/>
    <form suffix="iasen" tag="MSI3P0"/>
    <form suffix="iases" tag="MSI2S0"/>
    <form suffix="iaste" tag="MIS2S0"/>
    <form suffix="iasteis" tag="MIS2P0"/>
    <form suffix="iemos" tag="MSP1P0"/>
    <form suffix="iábamos" tag="MII1P0"/>
    <form suffix="iáis" tag="MIP2P0"/>
    <form suffix="iáramos" tag="MSI1P0"/>
    <form suffix="iáremos" tag="MSF1P0"/>
    <form suffix="iásemos" tag="MSI1P0"/>
    <form suffix="ié" tag="MIS1S0"/>
    <form suffix="iéis" tag="MSP2P0"/>
    <form suffix="ió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ía" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ía" tag="MM02S0" synt="Imperative"/>
    <form suffix="ían" tag="MIP3P0"/>
    <form suffix="ías" tag="MIP2S0"/>
    <form suffix="íe" tag="MSP1S0"/>
    <form suffix="íe" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="íen" tag="MSP3P0"/>
    <form suffix="íes" tag="MSP2S0"/>
    <form suffix="ío" tag="MIP1S0"/>
  </table>
  <table name="V18" rads=".*" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: uar -->
    <!-- 23 members -->
    <form suffix="uar" tag="MN0000" synt="Infinitive"/>
    <form suffix="uaba" tag="MII1S0"/>
    <form suffix="uaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="uabais" tag="MII2P0"/>
    <form suffix="uaban" tag="MII3P0"/>
    <form suffix="uabas" tag="MII2S0"/>
    <form suffix="uad" tag="MM02P0" synt="Imperative"/>
    <form suffix="uada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="uadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="uado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="uados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="uamos" tag="MIP1P0"/>
    <form suffix="uamos" tag="MIS1P0"/>
    <form suffix="uando" tag="MG0000"/>
    <form suffix="uara" tag="MSI1S0"/>
    <form suffix="uara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="uarais" tag="MSI2P0"/>
    <form suffix="uaran" tag="MSI3P0"/>
    <form suffix="uaras" tag="MSI2S0"/>
    <form suffix="uare" tag="MSF1S0"/>
    <form suffix="uare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="uareis" tag="MSF2P0"/>
    <form suffix="uaremos" tag="MIF1P0"/>
    <form suffix="uaren" tag="MSF3P0"/>
    <form suffix="uares" tag="MSF2S0"/>
    <form suffix="uaron" tag="MIS3P0"/>
    <form suffix="uará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="uarán" tag="MIF3P0"/>
    <form suffix="uarás" tag="MIF2S0"/>
    <form suffix="uaré" tag="MIF1S0"/>
    <form suffix="uaréis" tag="MIF2P0"/>
    <form suffix="uaría" tag="MIC1S0"/>
    <form suffix="uaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="uaríais" tag="MIC2P0"/>
    <form suffix="uaríamos" tag="MIC1P0"/>
    <form suffix="uarían" tag="MIC3P0"/>
    <form suffix="uarías" tag="MIC2S0"/>
    <form suffix="uase" tag="MSI1S0"/>
    <form suffix="uase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="uaseis" tag="MSI2P0"/>
    <form suffix="uasen" tag="MSI3P0"/>
    <form suffix="uases" tag="MSI2S0"/>
    <form suffix="uaste" tag="MIS2S0"/>
    <form suffix="uasteis" tag="MIS2P0"/>
    <form suffix="uemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="uemos" tag="MSP1P0"/>
    <form suffix="uábamos" tag="MII1P0"/>
    <form suffix="uáis" tag="MIP2P0"/>
    <form suffix="uáramos" tag="MSI1P0"/>
    <form suffix="uáremos" tag="MSF1P0"/>
    <form suffix="uásemos" tag="MSI1P0"/>
    <form suffix="ué" tag="MIS1S0"/>
    <form suffix="uéis" tag="MSP2P0"/>
    <form suffix="uó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="úa" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="úa" tag="MM02S0" synt="Imperative"/>
    <form suffix="úan" tag="MIP3P0"/>
    <form suffix="úas" tag="MIP2S0"/>
    <form suffix="úe" tag="MM03S0" synt="Imperative"/>
    <form suffix="úe" tag="MSP1S0"/>
    <form suffix="úe" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="úen" tag="MM03P0" synt="Imperative"/>
    <form suffix="úen" tag="MSP3P0"/>
    <form suffix="úes" tag="MSP2S0"/>
    <form suffix="úo" tag="MIP1S0"/>
  </table>
  <table name="V19" rads=".*p" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: p + oner -->
    <!-- 22 members -->
    <form suffix="oner" tag="MN0000" synt="Infinitive"/>
    <form suffix="ondremos" tag="MIF1P0"/>
    <form suffix="ondrá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="ondrán" tag="MIF3P0"/>
    <form suffix="ondrás" tag="MIF2S0"/>
    <form suffix="ondré" tag="MIF1S0"/>
    <form suffix="ondréis" tag="MIF2P0"/>
    <form suffix="ondría" tag="MIC1S0"/>
    <form suffix="ondría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ondríais" tag="MIC2P0"/>
    <form suffix="ondríamos" tag="MIC1P0"/>
    <form suffix="ondrían" tag="MIC3P0"/>
    <form suffix="ondrías" tag="MIC2S0"/>
    <form suffix="one" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="oned" tag="MM02P0" synt="Imperative"/>
    <form suffix="onemos" tag="MIP1P0"/>
    <form suffix="onen" tag="MIP3P0"/>
    <form suffix="ones" tag="MIP2S0"/>
    <form suffix="onga" tag="MM03S0" synt="Imperative"/>
    <form suffix="onga" tag="MSP1S0"/>
    <form suffix="onga" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ongamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="ongamos" tag="MSP1P0"/>
    <form suffix="ongan" tag="MM03P0" synt="Imperative"/>
    <form suffix="ongan" tag="MSP3P0"/>
    <form suffix="ongas" tag="MSP2S0"/>
    <form suffix="ongo" tag="MIP1S0"/>
    <form suffix="ongáis" tag="MSP2P0"/>
    <form suffix="oniendo" tag="MG0000"/>
    <form suffix="onéis" tag="MIP2P0"/>
    <form suffix="onía" tag="MII1S0"/>
    <form suffix="onía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="oníais" tag="MII2P0"/>
    <form suffix="oníamos" tag="MII1P0"/>
    <form suffix="onían" tag="MII3P0"/>
    <form suffix="onías" tag="MII2S0"/>
    <form suffix="uesta" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="uestas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="uesto" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="uestos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="use" tag="MIS1S0"/>
    <form suffix="usiera" tag="MSI1S0"/>
    <form suffix="usiera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="usierais" tag="MSI2P0"/>
    <form suffix="usieran" tag="MSI3P0"/>
    <form suffix="usieras" tag="MSI2S0"/>
    <form suffix="usiere" tag="MSF1S0"/>
    <form suffix="usiere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="usiereis" tag="MSF2P0"/>
    <form suffix="usieren" tag="MSF3P0"/>
    <form suffix="usieres" tag="MSF2S0"/>
    <form suffix="usieron" tag="MIS3P0"/>
    <form suffix="usiese" tag="MSI1S0"/>
    <form suffix="usiese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="usieseis" tag="MSI2P0"/>
    <form suffix="usiesen" tag="MSI3P0"/>
    <form suffix="usieses" tag="MSI2S0"/>
    <form suffix="usimos" tag="MIS1P0"/>
    <form suffix="usiste" tag="MIS2S0"/>
    <form suffix="usisteis" tag="MIS2P0"/>
    <form suffix="usiéramos" tag="MSI1P0"/>
    <form suffix="usiéremos" tag="MSF1P0"/>
    <form suffix="usiésemos" tag="MSI1P0"/>
    <form suffix="uso" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ón" tag="MM02S0" synt="Imperative"/>
  </table>
  <table name="V20" rads=".*[nrui]" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: n/r/u/i + gir -->
    <!-- 19 members -->
    <form suffix="gir" tag="MN0000" synt="Infinitive"/>
    <form suffix="ge" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ge" tag="MM02S0" synt="Imperative"/>
    <form suffix="gen" tag="MIP3P0"/>
    <form suffix="ges" tag="MIP2S0"/>
    <form suffix="gid" tag="MM02P0" synt="Imperative"/>
    <form suffix="gida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="gidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="gido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="gidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="giendo" tag="MG0000"/>
    <form suffix="giera" tag="MSI1S0"/>
    <form suffix="giera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="gierais" tag="MSI2P0"/>
    <form suffix="gieran" tag="MSI3P0"/>
    <form suffix="gieras" tag="MSI2S0"/>
    <form suffix="giere" tag="MSF1S0"/>
    <form suffix="giere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="giereis" tag="MSF2P0"/>
    <form suffix="gieren" tag="MSF3P0"/>
    <form suffix="gieres" tag="MSF2S0"/>
    <form suffix="gieron" tag="MIS3P0"/>
    <form suffix="giese" tag="MSI1S0"/>
    <form suffix="giese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="gieseis" tag="MSI2P0"/>
    <form suffix="giesen" tag="MSI3P0"/>
    <form suffix="gieses" tag="MSI2S0"/>
    <form suffix="gimos" tag="MIP1P0"/>
    <form suffix="gimos" tag="MIS1P0"/>
    <form suffix="giremos" tag="MIF1P0"/>
    <form suffix="girá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="girán" tag="MIF3P0"/>
    <form suffix="girás" tag="MIF2S0"/>
    <form suffix="giré" tag="MIF1S0"/>
    <form suffix="giréis" tag="MIF2P0"/>
    <form suffix="giría" tag="MIC1S0"/>
    <form suffix="giría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="giríais" tag="MIC2P0"/>
    <form suffix="giríamos" tag="MIC1P0"/>
    <form suffix="girían" tag="MIC3P0"/>
    <form suffix="girías" tag="MIC2S0"/>
    <form suffix="giste" tag="MIS2S0"/>
    <form suffix="gisteis" tag="MIS2P0"/>
    <form suffix="giéramos" tag="MSI1P0"/>
    <form suffix="giéremos" tag="MSF1P0"/>
    <form suffix="giésemos" tag="MSI1P0"/>
    <form suffix="gió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="gí" tag="MIS1S0"/>
    <form suffix="gía" tag="MII1S0"/>
    <form suffix="gía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="gíais" tag="MII2P0"/>
    <form suffix="gíamos" tag="MII1P0"/>
    <form suffix="gían" tag="MII3P0"/>
    <form suffix="gías" tag="MII2S0"/>
    <form suffix="gís" tag="MIP2P0"/>
    <form suffix="ja" tag="MM03S0" synt="Imperative"/>
    <form suffix="ja" tag="MSP1S0"/>
    <form suffix="ja" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="jamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="jamos" tag="MSP1P0"/>
    <form suffix="jan" tag="MM03P0" synt="Imperative"/>
    <form suffix="jan" tag="MSP3P0"/>
    <form suffix="jas" tag="MSP2S0"/>
    <form suffix="jo" tag="MIP1S0"/>
    <form suffix="jáis" tag="MSP2P0"/>
  </table>
  <table name="V21" rads=".*[hfsjgu]" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: h/f/s/j/g + erir -->
    <!-- 17 members -->
    <form suffix="erir" tag="MN0000" synt="Infinitive"/>
    <form suffix="erid" tag="MM02P0" synt="Imperative"/>
    <form suffix="erida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="eridas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="erido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="eridos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="erimos" tag="MIP1P0"/>
    <form suffix="erimos" tag="MIS1P0"/>
    <form suffix="eriremos" tag="MIF1P0"/>
    <form suffix="erirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="erirán" tag="MIF3P0"/>
    <form suffix="erirás" tag="MIF2S0"/>
    <form suffix="eriré" tag="MIF1S0"/>
    <form suffix="eriréis" tag="MIF2P0"/>
    <form suffix="eriría" tag="MIC1S0"/>
    <form suffix="eriría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="eriríais" tag="MIC2P0"/>
    <form suffix="eriríamos" tag="MIC1P0"/>
    <form suffix="erirían" tag="MIC3P0"/>
    <form suffix="erirías" tag="MIC2S0"/>
    <form suffix="eriste" tag="MIS2S0"/>
    <form suffix="eristeis" tag="MIS2P0"/>
    <form suffix="erí" tag="MIS1S0"/>
    <form suffix="ería" tag="MII1S0"/>
    <form suffix="ería" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="eríais" tag="MII2P0"/>
    <form suffix="eríamos" tag="MII1P0"/>
    <form suffix="erían" tag="MII3P0"/>
    <form suffix="erías" tag="MII2S0"/>
    <form suffix="erís" tag="MIP2P0"/>
    <form suffix="iera" tag="MM03S0" synt="Imperative"/>
    <form suffix="iera" tag="MSP1S0"/>
    <form suffix="iera" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ieran" tag="MM03P0" synt="Imperative"/>
    <form suffix="ieran" tag="MSP3P0"/>
    <form suffix="ieras" tag="MSP2S0"/>
    <form suffix="iere" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="iere" tag="MM02S0" synt="Imperative"/>
    <form suffix="ieren" tag="MIP3P0"/>
    <form suffix="ieres" tag="MIP2S0"/>
    <form suffix="iero" tag="MIP1S0"/>
    <form suffix="iramos" tag="MM01P0" synt="Imperative"/>
    <form suffix="iramos" tag="MSP1P0"/>
    <form suffix="iriendo" tag="MG0000"/>
    <form suffix="iriera" tag="MSI1S0"/>
    <form suffix="iriera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="irierais" tag="MSI2P0"/>
    <form suffix="irieran" tag="MSI3P0"/>
    <form suffix="irieras" tag="MSI2S0"/>
    <form suffix="iriere" tag="MSF1S0"/>
    <form suffix="iriere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="iriereis" tag="MSF2P0"/>
    <form suffix="irieren" tag="MSF3P0"/>
    <form suffix="irieres" tag="MSF2S0"/>
    <form suffix="irieron" tag="MIS3P0"/>
    <form suffix="iriese" tag="MSI1S0"/>
    <form suffix="iriese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="irieseis" tag="MSI2P0"/>
    <form suffix="iriesen" tag="MSI3P0"/>
    <form suffix="irieses" tag="MSI2S0"/>
    <form suffix="iriéramos" tag="MSI1P0"/>
    <form suffix="iriéremos" tag="MSF1P0"/>
    <form suffix="iriésemos" tag="MSI1P0"/>
    <form suffix="irió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="iráis" tag="MSP2P0"/>
  </table>
  <table name="V22" rads=".*" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: entar -->
    <!-- 16 members -->
    <form suffix="entar" tag="MN0000" synt="Infinitive"/>
    <form suffix="entaba" tag="MII1S0"/>
    <form suffix="entaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="entabais" tag="MII2P0"/>
    <form suffix="entaban" tag="MII3P0"/>
    <form suffix="entabas" tag="MII2S0"/>
    <form suffix="entad" tag="MM02P0" synt="Imperative"/>
    <form suffix="entada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="entadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="entado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="entados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="entamos" tag="MIP1P0"/>
    <form suffix="entamos" tag="MIS1P0"/>
    <form suffix="entando" tag="MG0000"/>
    <form suffix="entara" tag="MSI1S0"/>
    <form suffix="entara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="entarais" tag="MSI2P0"/>
    <form suffix="entaran" tag="MSI3P0"/>
    <form suffix="entaras" tag="MSI2S0"/>
    <form suffix="entare" tag="MSF1S0"/>
    <form suffix="entare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="entareis" tag="MSF2P0"/>
    <form suffix="entaremos" tag="MIF1P0"/>
    <form suffix="entaren" tag="MSF3P0"/>
    <form suffix="entares" tag="MSF2S0"/>
    <form suffix="entaron" tag="MIS3P0"/>
    <form suffix="entará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="entarán" tag="MIF3P0"/>
    <form suffix="entarás" tag="MIF2S0"/>
    <form suffix="entaré" tag="MIF1S0"/>
    <form suffix="entaréis" tag="MIF2P0"/>
    <form suffix="entaría" tag="MIC1S0"/>
    <form suffix="entaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="entaríais" tag="MIC2P0"/>
    <form suffix="entaríamos" tag="MIC1P0"/>
    <form suffix="entarían" tag="MIC3P0"/>
    <form suffix="entarías" tag="MIC2S0"/>
    <form suffix="entase" tag="MSI1S0"/>
    <form suffix="entase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="entaseis" tag="MSI2P0"/>
    <form suffix="entasen" tag="MSI3P0"/>
    <form suffix="entases" tag="MSI2S0"/>
    <form suffix="entaste" tag="MIS2S0"/>
    <form suffix="entasteis" tag="MIS2P0"/>
    <form suffix="entemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="entemos" tag="MSP1P0"/>
    <form suffix="entábamos" tag="MII1P0"/>
    <form suffix="entáis" tag="MIP2P0"/>
    <form suffix="entáramos" tag="MSI1P0"/>
    <form suffix="entáremos" tag="MSF1P0"/>
    <form suffix="entásemos" tag="MSI1P0"/>
    <form suffix="enté" tag="MIS1S0"/>
    <form suffix="entéis" tag="MSP2P0"/>
    <form suffix="entó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ienta" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ienta" tag="MM02S0" synt="Imperative"/>
    <form suffix="ientan" tag="MIP3P0"/>
    <form suffix="ientas" tag="MIP2S0"/>
    <form suffix="iente" tag="MM03S0" synt="Imperative"/>
    <form suffix="iente" tag="MSP1S0"/>
    <form suffix="iente" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ienten" tag="MM03P0" synt="Imperative"/>
    <form suffix="ienten" tag="MSP3P0"/>
    <form suffix="ientes" tag="MSP2S0"/>
    <form suffix="iento" tag="MIP1S0"/>
  </table>
  <table name="V23" rads=".*[tchf]" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: t/c/h/f + ender -->
    <!-- 15 members -->
    <form suffix="ender" tag="MN0000" synt="Infinitive"/>
    <form suffix="endamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="endamos" tag="MSP1P0"/>
    <form suffix="ended" tag="MM02P0" synt="Imperative"/>
    <form suffix="endemos" tag="MIP1P0"/>
    <form suffix="enderemos" tag="MIF1P0"/>
    <form suffix="enderá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="enderán" tag="MIF3P0"/>
    <form suffix="enderás" tag="MIF2S0"/>
    <form suffix="enderé" tag="MIF1S0"/>
    <form suffix="enderéis" tag="MIF2P0"/>
    <form suffix="endería" tag="MIC1S0"/>
    <form suffix="endería" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="enderíais" tag="MIC2P0"/>
    <form suffix="enderíamos" tag="MIC1P0"/>
    <form suffix="enderían" tag="MIC3P0"/>
    <form suffix="enderías" tag="MIC2S0"/>
    <form suffix="endida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="endidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="endido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="endidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="endiendo" tag="MG0000"/>
    <form suffix="endiera" tag="MSI1S0"/>
    <form suffix="endiera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="endierais" tag="MSI2P0"/>
    <form suffix="endieran" tag="MSI3P0"/>
    <form suffix="endieras" tag="MSI2S0"/>
    <form suffix="endiere" tag="MSF1S0"/>
    <form suffix="endiere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="endiereis" tag="MSF2P0"/>
    <form suffix="endieren" tag="MSF3P0"/>
    <form suffix="endieres" tag="MSF2S0"/>
    <form suffix="endieron" tag="MIS3P0"/>
    <form suffix="endiese" tag="MSI1S0"/>
    <form suffix="endiese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="endieseis" tag="MSI2P0"/>
    <form suffix="endiesen" tag="MSI3P0"/>
    <form suffix="endieses" tag="MSI2S0"/>
    <form suffix="endimos" tag="MIS1P0"/>
    <form suffix="endiste" tag="MIS2S0"/>
    <form suffix="endisteis" tag="MIS2P0"/>
    <form suffix="endiéramos" tag="MSI1P0"/>
    <form suffix="endiéremos" tag="MSF1P0"/>
    <form suffix="endiésemos" tag="MSI1P0"/>
    <form suffix="endió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="endáis" tag="MSP2P0"/>
    <form suffix="endéis" tag="MIP2P0"/>
    <form suffix="endí" tag="MIS1S0"/>
    <form suffix="endía" tag="MII1S0"/>
    <form suffix="endía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="endíais" tag="MII2P0"/>
    <form suffix="endíamos" tag="MII1P0"/>
    <form suffix="endían" tag="MII3P0"/>
    <form suffix="endías" tag="MII2S0"/>
    <form suffix="ienda" tag="MM03S0" synt="Imperative"/>
    <form suffix="ienda" tag="MSP1S0"/>
    <form suffix="ienda" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="iendan" tag="MM03P0" synt="Imperative"/>
    <form suffix="iendan" tag="MSP3P0"/>
    <form suffix="iendas" tag="MSP2S0"/>
    <form suffix="iende" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="iende" tag="MM02S0" synt="Imperative"/>
    <form suffix="ienden" tag="MIP3P0"/>
    <form suffix="iendes" tag="MIP2S0"/>
    <form suffix="iendo" tag="MIP1S0"/>
  </table>
  <table name="V25" rads=".*[srlnc]" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: s/r/l/n/c + egar -->
    <!-- 13 members -->
    <form suffix="egar" tag="MN0000" synt="Infinitive"/>
    <form suffix="egaba" tag="MII1S0"/>
    <form suffix="egaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="egabais" tag="MII2P0"/>
    <form suffix="egaban" tag="MII3P0"/>
    <form suffix="egabas" tag="MII2S0"/>
    <form suffix="egad" tag="MM02P0" synt="Imperative"/>
    <form suffix="egada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="egadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="egado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="egados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="egamos" tag="MIP1P0"/>
    <form suffix="egamos" tag="MIS1P0"/>
    <form suffix="egando" tag="MG0000"/>
    <form suffix="egara" tag="MSI1S0"/>
    <form suffix="egara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="egarais" tag="MSI2P0"/>
    <form suffix="egaran" tag="MSI3P0"/>
    <form suffix="egaras" tag="MSI2S0"/>
    <form suffix="egare" tag="MSF1S0"/>
    <form suffix="egare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="egareis" tag="MSF2P0"/>
    <form suffix="egaremos" tag="MIF1P0"/>
    <form suffix="egaren" tag="MSF3P0"/>
    <form suffix="egares" tag="MSF2S0"/>
    <form suffix="egaron" tag="MIS3P0"/>
    <form suffix="egará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="egarán" tag="MIF3P0"/>
    <form suffix="egarás" tag="MIF2S0"/>
    <form suffix="egaré" tag="MIF1S0"/>
    <form suffix="egaréis" tag="MIF2P0"/>
    <form suffix="egaría" tag="MIC1S0"/>
    <form suffix="egaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="egaríais" tag="MIC2P0"/>
    <form suffix="egaríamos" tag="MIC1P0"/>
    <form suffix="egarían" tag="MIC3P0"/>
    <form suffix="egarías" tag="MIC2S0"/>
    <form suffix="egase" tag="MSI1S0"/>
    <form suffix="egase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="egaseis" tag="MSI2P0"/>
    <form suffix="egasen" tag="MSI3P0"/>
    <form suffix="egases" tag="MSI2S0"/>
    <form suffix="egaste" tag="MIS2S0"/>
    <form suffix="egasteis" tag="MIS2P0"/>
    <form suffix="eguemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="eguemos" tag="MSP1P0"/>
    <form suffix="egué" tag="MIS1S0"/>
    <form suffix="eguéis" tag="MSP2P0"/>
    <form suffix="egábamos" tag="MII1P0"/>
    <form suffix="egáis" tag="MIP2P0"/>
    <form suffix="egáramos" tag="MSI1P0"/>
    <form suffix="egáremos" tag="MSF1P0"/>
    <form suffix="egásemos" tag="MSI1P0"/>
    <form suffix="egó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="iega" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="iega" tag="MM02S0" synt="Imperative"/>
    <form suffix="iegan" tag="MIP3P0"/>
    <form suffix="iegas" tag="MIP2S0"/>
    <form suffix="iego" tag="MIP1S0"/>
    <form suffix="iegue" tag="MM03S0" synt="Imperative"/>
    <form suffix="iegue" tag="MSP1S0"/>
    <form suffix="iegue" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ieguen" tag="MM03P0" synt="Imperative"/>
    <form suffix="ieguen" tag="MSP3P0"/>
    <form suffix="iegues" tag="MSP2S0"/>
  </table>
  <table name="V26" rads=".*[alixp][tldrn]" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: a/l/i/x/p + t/l/d/r/n + uar -->
    <!-- 13 members -->
    <form suffix="uar" tag="MN0000" synt="Infinitive"/>
    <form suffix="uaba" tag="MII1S0"/>
    <form suffix="uaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="uabais" tag="MII2P0"/>
    <form suffix="uaban" tag="MII3P0"/>
    <form suffix="uabas" tag="MII2S0"/>
    <form suffix="uad" tag="MM02P0" synt="Imperative"/>
    <form suffix="uada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="uadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="uado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="uados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="uamos" tag="MIP1P0"/>
    <form suffix="uamos" tag="MIS1P0"/>
    <form suffix="uando" tag="MG0000"/>
    <form suffix="uara" tag="MSI1S0"/>
    <form suffix="uara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="uarais" tag="MSI2P0"/>
    <form suffix="uaran" tag="MSI3P0"/>
    <form suffix="uaras" tag="MSI2S0"/>
    <form suffix="uare" tag="MSF1S0"/>
    <form suffix="uare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="uareis" tag="MSF2P0"/>
    <form suffix="uaremos" tag="MIF1P0"/>
    <form suffix="uaren" tag="MSF3P0"/>
    <form suffix="uares" tag="MSF2S0"/>
    <form suffix="uaron" tag="MIS3P0"/>
    <form suffix="uará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="uarán" tag="MIF3P0"/>
    <form suffix="uarás" tag="MIF2S0"/>
    <form suffix="uaré" tag="MIF1S0"/>
    <form suffix="uaréis" tag="MIF2P0"/>
    <form suffix="uaría" tag="MIC1S0"/>
    <form suffix="uaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="uaríais" tag="MIC2P0"/>
    <form suffix="uaríamos" tag="MIC1P0"/>
    <form suffix="uarían" tag="MIC3P0"/>
    <form suffix="uarías" tag="MIC2S0"/>
    <form suffix="uase" tag="MSI1S0"/>
    <form suffix="uase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="uaseis" tag="MSI2P0"/>
    <form suffix="uasen" tag="MSI3P0"/>
    <form suffix="uases" tag="MSI2S0"/>
    <form suffix="uaste" tag="MIS2S0"/>
    <form suffix="uasteis" tag="MIS2P0"/>
    <form suffix="uemos" tag="MSP1P0"/>
    <form suffix="uábamos" tag="MII1P0"/>
    <form suffix="uáis" tag="MIP2P0"/>
    <form suffix="uáramos" tag="MSI1P0"/>
    <form suffix="uáremos" tag="MSF1P0"/>
    <form suffix="uásemos" tag="MSI1P0"/>
    <form suffix="ué" tag="MIS1S0"/>
    <form suffix="uéis" tag="MSP2P0"/>
    <form suffix="uó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="úa" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="úa" tag="MM02S0" synt="Imperative"/>
    <form suffix="úan" tag="MIP3P0"/>
    <form suffix="úas" tag="MIP2S0"/>
    <form suffix="úe" tag="MSP1S0"/>
    <form suffix="úe" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="úen" tag="MSP3P0"/>
    <form suffix="úes" tag="MSP2S0"/>
    <form suffix="úo" tag="MIP1S0"/>
  </table>
  <table name="V27" rads=".*scri" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: scri + bir -->
    <!-- 12 members -->
    <form suffix="bir" tag="MN0000" synt="Infinitive"/>
    <form suffix="ba" tag="MM03S0" synt="Imperative"/>
    <form suffix="ba" tag="MSP1S0"/>
    <form suffix="ba" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="bamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="bamos" tag="MSP1P0"/>
    <form suffix="ban" tag="MM03P0" synt="Imperative"/>
    <form suffix="ban" tag="MSP3P0"/>
    <form suffix="bas" tag="MSP2S0"/>
    <form suffix="be" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="be" tag="MM02S0" synt="Imperative"/>
    <form suffix="ben" tag="MIP3P0"/>
    <form suffix="bes" tag="MIP2S0"/>
    <form suffix="bid" tag="MM02P0" synt="Imperative"/>
    <form suffix="biendo" tag="MG0000"/>
    <form suffix="biera" tag="MSI1S0"/>
    <form suffix="biera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="bierais" tag="MSI2P0"/>
    <form suffix="bieran" tag="MSI3P0"/>
    <form suffix="bieras" tag="MSI2S0"/>
    <form suffix="biere" tag="MSF1S0"/>
    <form suffix="biere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="biereis" tag="MSF2P0"/>
    <form suffix="bieren" tag="MSF3P0"/>
    <form suffix="bieres" tag="MSF2S0"/>
    <form suffix="bieron" tag="MIS3P0"/>
    <form suffix="biese" tag="MSI1S0"/>
    <form suffix="biese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="bieseis" tag="MSI2P0"/>
    <form suffix="biesen" tag="MSI3P0"/>
    <form suffix="bieses" tag="MSI2S0"/>
    <form suffix="bimos" tag="MIP1P0"/>
    <form suffix="bimos" tag="MIS1P0"/>
    <form suffix="biremos" tag="MIF1P0"/>
    <form suffix="birá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="birán" tag="MIF3P0"/>
    <form suffix="birás" tag="MIF2S0"/>
    <form suffix="biré" tag="MIF1S0"/>
    <form suffix="biréis" tag="MIF2P0"/>
    <form suffix="biría" tag="MIC1S0"/>
    <form suffix="biría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="biríais" tag="MIC2P0"/>
    <form suffix="biríamos" tag="MIC1P0"/>
    <form suffix="birían" tag="MIC3P0"/>
    <form suffix="birías" tag="MIC2S0"/>
    <form suffix="biste" tag="MIS2S0"/>
    <form suffix="bisteis" tag="MIS2P0"/>
    <form suffix="biéramos" tag="MSI1P0"/>
    <form suffix="biéremos" tag="MSF1P0"/>
    <form suffix="biésemos" tag="MSI1P0"/>
    <form suffix="bió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="bo" tag="MIP1S0"/>
    <form suffix="báis" tag="MSP2P0"/>
    <form suffix="bí" tag="MIS1S0"/>
    <form suffix="bía" tag="MII1S0"/>
    <form suffix="bía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="bíais" tag="MII2P0"/>
    <form suffix="bíamos" tag="MII1P0"/>
    <form suffix="bían" tag="MII3P0"/>
    <form suffix="bías" tag="MII2S0"/>
    <form suffix="bís" tag="MIP2P0"/>
    <form suffix="ta" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="tas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="to" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="tos" tag="MP00PM" synt="PastParticiple"/>
  </table>
  <table name="V28" rads=".*[uibre][lnuir]" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: u/i/b/r/e + l/n/u/i/r + gir -->
    <!-- 12 members -->
    <form suffix="gir" tag="MN0000" synt="Infinitive"/>
    <form suffix="ge" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ge" tag="MM02S0" synt="Imperative"/>
    <form suffix="gen" tag="MIP3P0"/>
    <form suffix="ges" tag="MIP2S0"/>
    <form suffix="gid" tag="MM02P0" synt="Imperative"/>
    <form suffix="gida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="gidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="gido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="gidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="giendo" tag="MG0000"/>
    <form suffix="giera" tag="MSI1S0"/>
    <form suffix="giera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="gierais" tag="MSI2P0"/>
    <form suffix="gieran" tag="MSI3P0"/>
    <form suffix="gieras" tag="MSI2S0"/>
    <form suffix="giere" tag="MSF1S0"/>
    <form suffix="giere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="giereis" tag="MSF2P0"/>
    <form suffix="gieren" tag="MSF3P0"/>
    <form suffix="gieres" tag="MSF2S0"/>
    <form suffix="gieron" tag="MIS3P0"/>
    <form suffix="giese" tag="MSI1S0"/>
    <form suffix="giese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="gieseis" tag="MSI2P0"/>
    <form suffix="giesen" tag="MSI3P0"/>
    <form suffix="gieses" tag="MSI2S0"/>
    <form suffix="gimos" tag="MIP1P0"/>
    <form suffix="gimos" tag="MIS1P0"/>
    <form suffix="giremos" tag="MIF1P0"/>
    <form suffix="girá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="girán" tag="MIF3P0"/>
    <form suffix="girás" tag="MIF2S0"/>
    <form suffix="giré" tag="MIF1S0"/>
    <form suffix="giréis" tag="MIF2P0"/>
    <form suffix="giría" tag="MIC1S0"/>
    <form suffix="giría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="giríais" tag="MIC2P0"/>
    <form suffix="giríamos" tag="MIC1P0"/>
    <form suffix="girían" tag="MIC3P0"/>
    <form suffix="girías" tag="MIC2S0"/>
    <form suffix="giste" tag="MIS2S0"/>
    <form suffix="gisteis" tag="MIS2P0"/>
    <form suffix="giéramos" tag="MSI1P0"/>
    <form suffix="giéremos" tag="MSF1P0"/>
    <form suffix="giésemos" tag="MSI1P0"/>
    <form suffix="gió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="gí" tag="MIS1S0"/>
    <form suffix="gía" tag="MII1S0"/>
    <form suffix="gía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="gíais" tag="MII2P0"/>
    <form suffix="gíamos" tag="MII1P0"/>
    <form suffix="gían" tag="MII3P0"/>
    <form suffix="gías" tag="MII2S0"/>
    <form suffix="gís" tag="MIP2P0"/>
    <form suffix="ja" tag="MSP1S0"/>
    <form suffix="ja" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ja" tag="MM03S0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="jamos" tag="MSP1P0"/>
    <form suffix="jamos" tag="MM01P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="jan" tag="MSP3P0"/>
    <form suffix="jan" tag="MM03P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="jas" tag="MSP2S0"/>
    <form suffix="jo" tag="MIP1S0"/>
    <form suffix="jáis" tag="MSP2P0"/>
  </table>
  <table name="V29" rads=".*[aoen]du" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: a/o/e/n + du + cir -->
    <!-- 10 members -->
    <form suffix="cir" tag="MN0000" synt="Infinitive"/>
    <form suffix="ce" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ce" tag="MM02S0" synt="Imperative"/>
    <form suffix="cen" tag="MIP3P0"/>
    <form suffix="ces" tag="MIP2S0"/>
    <form suffix="cid" tag="MM02P0" synt="Imperative"/>
    <form suffix="cida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="cidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="cido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="cidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ciendo" tag="MG0000"/>
    <form suffix="cimos" tag="MIP1P0"/>
    <form suffix="ciremos" tag="MIF1P0"/>
    <form suffix="cirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="cirán" tag="MIF3P0"/>
    <form suffix="cirás" tag="MIF2S0"/>
    <form suffix="ciré" tag="MIF1S0"/>
    <form suffix="ciréis" tag="MIF2P0"/>
    <form suffix="ciría" tag="MIC1S0"/>
    <form suffix="ciría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ciríais" tag="MIC2P0"/>
    <form suffix="ciríamos" tag="MIC1P0"/>
    <form suffix="cirían" tag="MIC3P0"/>
    <form suffix="cirías" tag="MIC2S0"/>
    <form suffix="cía" tag="MII1S0"/>
    <form suffix="cía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="cíais" tag="MII2P0"/>
    <form suffix="cíamos" tag="MII1P0"/>
    <form suffix="cían" tag="MII3P0"/>
    <form suffix="cías" tag="MII2S0"/>
    <form suffix="cís" tag="MIP2P0"/>
    <form suffix="je" tag="MIS1S0"/>
    <form suffix="jera" tag="MSI1S0"/>
    <form suffix="jera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="jerais" tag="MSI2P0"/>
    <form suffix="jeran" tag="MSI3P0"/>
    <form suffix="jeras" tag="MSI2S0"/>
    <form suffix="jere" tag="MSF1S0"/>
    <form suffix="jere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="jereis" tag="MSF2P0"/>
    <form suffix="jeren" tag="MSF3P0"/>
    <form suffix="jeres" tag="MSF2S0"/>
    <form suffix="jeron" tag="MIS3P0"/>
    <form suffix="jese" tag="MSI1S0"/>
    <form suffix="jese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="jeseis" tag="MSI2P0"/>
    <form suffix="jesen" tag="MSI3P0"/>
    <form suffix="jeses" tag="MSI2S0"/>
    <form suffix="jimos" tag="MIS1P0"/>
    <form suffix="jiste" tag="MIS2S0"/>
    <form suffix="jisteis" tag="MIS2P0"/>
    <form suffix="jo" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="jéramos" tag="MSI1P0"/>
    <form suffix="jéremos" tag="MSF1P0"/>
    <form suffix="jésemos" tag="MSI1P0"/>
    <form suffix="zca" tag="MM03S0" synt="Imperative"/>
    <form suffix="zca" tag="MSP1S0"/>
    <form suffix="zca" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="zcamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="zcamos" tag="MSP1P0"/>
    <form suffix="zcan" tag="MM03P0" synt="Imperative"/>
    <form suffix="zcan" tag="MSP3P0"/>
    <form suffix="zcas" tag="MSP2S0"/>
    <form suffix="zco" tag="MIP1S0"/>
    <form suffix="zcáis" tag="MSP2P0"/>
  </table>
  <table name="V30" rads=".*tra" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: tra + er -->
    <!-- 10 members -->
    <form suffix="er" tag="MN0000" synt="Infinitive"/>
    <form suffix="e" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="e" tag="MM02S0" synt="Imperative"/>
    <form suffix="ed" tag="MM02P0" synt="Imperative"/>
    <form suffix="emos" tag="MIP1P0"/>
    <form suffix="en" tag="MIP3P0"/>
    <form suffix="eremos" tag="MIF1P0"/>
    <form suffix="erá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="erán" tag="MIF3P0"/>
    <form suffix="erás" tag="MIF2S0"/>
    <form suffix="eré" tag="MIF1S0"/>
    <form suffix="eréis" tag="MIF2P0"/>
    <form suffix="ería" tag="MIC1S0"/>
    <form suffix="ería" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="eríais" tag="MIC2P0"/>
    <form suffix="eríamos" tag="MIC1P0"/>
    <form suffix="erían" tag="MIC3P0"/>
    <form suffix="erías" tag="MIC2S0"/>
    <form suffix="es" tag="MIP2S0"/>
    <form suffix="iga" tag="MM03S0" synt="Imperative"/>
    <form suffix="iga" tag="MSP1S0"/>
    <form suffix="iga" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="igamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="igamos" tag="MSP1P0"/>
    <form suffix="igan" tag="MM03P0" synt="Imperative"/>
    <form suffix="igan" tag="MSP3P0"/>
    <form suffix="igas" tag="MSP2S0"/>
    <form suffix="igo" tag="MIP1S0"/>
    <form suffix="igáis" tag="MSP2P0"/>
    <form suffix="je" tag="MIS1S0"/>
    <form suffix="jera" tag="MSI1S0"/>
    <form suffix="jera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="jerais" tag="MSI2P0"/>
    <form suffix="jeran" tag="MSI3P0"/>
    <form suffix="jeras" tag="MSI2S0"/>
    <form suffix="jere" tag="MSF1S0"/>
    <form suffix="jere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="jereis" tag="MSF2P0"/>
    <form suffix="jeren" tag="MSF3P0"/>
    <form suffix="jeres" tag="MSF2S0"/>
    <form suffix="jeron" tag="MIS3P0"/>
    <form suffix="jese" tag="MSI1S0"/>
    <form suffix="jese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="jeseis" tag="MSI2P0"/>
    <form suffix="jesen" tag="MSI3P0"/>
    <form suffix="jeses" tag="MSI2S0"/>
    <form suffix="jimos" tag="MIS1P0"/>
    <form suffix="jiste" tag="MIS2S0"/>
    <form suffix="jisteis" tag="MIS2P0"/>
    <form suffix="jo" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="jéramos" tag="MSI1P0"/>
    <form suffix="jéremos" tag="MSF1P0"/>
    <form suffix="jésemos" tag="MSI1P0"/>
    <form suffix="yendo" tag="MG0000"/>
    <form suffix="éis" tag="MIP2P0"/>
    <form suffix="ía" tag="MII1S0"/>
    <form suffix="ía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="íais" tag="MII2P0"/>
    <form suffix="íamos" tag="MII1P0"/>
    <form suffix="ían" tag="MII3P0"/>
    <form suffix="ías" tag="MII2S0"/>
    <form suffix="ída" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ídas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ído" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ídos" tag="MP00PM" synt="PastParticiple"/>
  </table>
  <table name="V31" rads=".*[lrntb]u" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: l/r/n/t/b + u + ir -->
    <!-- 9 members -->
    <form suffix="ir" tag="MN0000" synt="Infinitive"/>
    <form suffix="id" tag="MM02P0" synt="Imperative"/>
    <form suffix="ida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="idas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="idos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="imos" tag="MIP1P0"/>
    <form suffix="imos" tag="MIS1P0"/>
    <form suffix="iremos" tag="MIF1P0"/>
    <form suffix="irá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="irán" tag="MIF3P0"/>
    <form suffix="irás" tag="MIF2S0"/>
    <form suffix="iré" tag="MIF1S0"/>
    <form suffix="iréis" tag="MIF2P0"/>
    <form suffix="iría" tag="MIC1S0"/>
    <form suffix="iría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="iríais" tag="MIC2P0"/>
    <form suffix="iríamos" tag="MIC1P0"/>
    <form suffix="irían" tag="MIC3P0"/>
    <form suffix="irías" tag="MIC2S0"/>
    <form suffix="iste" tag="MIS2S0"/>
    <form suffix="isteis" tag="MIS2P0"/>
    <form suffix="ya" tag="MSP1S0"/>
    <form suffix="ya" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ya" tag="MM03S0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="yamos" tag="MSP1P0"/>
    <form suffix="yamos" tag="MM01P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="yan" tag="MSP3P0"/>
    <form suffix="yan" tag="MM03P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="yas" tag="MSP2S0"/>
    <form suffix="ye" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ye" tag="MM02S0" synt="Imperative"/>
    <form suffix="yen" tag="MIP3P0"/>
    <form suffix="yendo" tag="MG0000"/>
    <form suffix="yera" tag="MSI1S0"/>
    <form suffix="yera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="yerais" tag="MSI2P0"/>
    <form suffix="yeran" tag="MSI3P0"/>
    <form suffix="yeras" tag="MSI2S0"/>
    <form suffix="yere" tag="MSF1S0"/>
    <form suffix="yere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="yereis" tag="MSF2P0"/>
    <form suffix="yeren" tag="MSF3P0"/>
    <form suffix="yeres" tag="MSF2S0"/>
    <form suffix="yeron" tag="MIS3P0"/>
    <form suffix="yes" tag="MIP2S0"/>
    <form suffix="yese" tag="MSI1S0"/>
    <form suffix="yese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="yeseis" tag="MSI2P0"/>
    <form suffix="yesen" tag="MSI3P0"/>
    <form suffix="yeses" tag="MSI2S0"/>
    <form suffix="yo" tag="MIP1S0"/>
    <form suffix="yáis" tag="MSP2P0"/>
    <form suffix="yéramos" tag="MSI1P0"/>
    <form suffix="yéremos" tag="MSF1P0"/>
    <form suffix="yésemos" tag="MSI1P0"/>
    <form suffix="yó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="í" tag="MIS1S0"/>
    <form suffix="ía" tag="MII1S0"/>
    <form suffix="ía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="íais" tag="MII2P0"/>
    <form suffix="íamos" tag="MII1P0"/>
    <form suffix="ían" tag="MII3P0"/>
    <form suffix="ías" tag="MII2S0"/>
    <form suffix="ís" tag="MIP2P0"/>
  </table>
  <table name="V32" rads=".*[nesab]t" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: n/e/s/a/b + t + ener -->
    <!-- 9 members -->
    <form suffix="ener" tag="MN0000" synt="Infinitive"/>
    <form suffix="endremos" tag="MIF1P0"/>
    <form suffix="endrá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="endrán" tag="MIF3P0"/>
    <form suffix="endrás" tag="MIF2S0"/>
    <form suffix="endré" tag="MIF1S0"/>
    <form suffix="endréis" tag="MIF2P0"/>
    <form suffix="endría" tag="MIC1S0"/>
    <form suffix="endría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="endríais" tag="MIC2P0"/>
    <form suffix="endríamos" tag="MIC1P0"/>
    <form suffix="endrían" tag="MIC3P0"/>
    <form suffix="endrías" tag="MIC2S0"/>
    <form suffix="ened" tag="MM02P0" synt="Imperative"/>
    <form suffix="enemos" tag="MIP1P0"/>
    <form suffix="enga" tag="MM03S0" synt="Imperative"/>
    <form suffix="enga" tag="MSP1S0"/>
    <form suffix="enga" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="engamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="engamos" tag="MSP1P0"/>
    <form suffix="engan" tag="MM03P0" synt="Imperative"/>
    <form suffix="engan" tag="MSP3P0"/>
    <form suffix="engas" tag="MSP2S0"/>
    <form suffix="engo" tag="MIP1S0"/>
    <form suffix="engáis" tag="MSP2P0"/>
    <form suffix="enida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="enidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="enido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="enidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="eniendo" tag="MG0000"/>
    <form suffix="enéis" tag="MIP2P0"/>
    <form suffix="enía" tag="MII1S0"/>
    <form suffix="enía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="eníais" tag="MII2P0"/>
    <form suffix="eníamos" tag="MII1P0"/>
    <form suffix="enían" tag="MII3P0"/>
    <form suffix="enías" tag="MII2S0"/>
    <form suffix="iene" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ienen" tag="MIP3P0"/>
    <form suffix="ienes" tag="MIP2S0"/>
    <form suffix="uve" tag="MIS1S0"/>
    <form suffix="uviera" tag="MSI1S0"/>
    <form suffix="uviera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="uvierais" tag="MSI2P0"/>
    <form suffix="uvieran" tag="MSI3P0"/>
    <form suffix="uvieras" tag="MSI2S0"/>
    <form suffix="uviere" tag="MSF1S0"/>
    <form suffix="uviere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="uviereis" tag="MSF2P0"/>
    <form suffix="uvieren" tag="MSF3P0"/>
    <form suffix="uvieres" tag="MSF2S0"/>
    <form suffix="uvieron" tag="MIS3P0"/>
    <form suffix="uviese" tag="MSI1S0"/>
    <form suffix="uviese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="uvieseis" tag="MSI2P0"/>
    <form suffix="uviesen" tag="MSI3P0"/>
    <form suffix="uvieses" tag="MSI2S0"/>
    <form suffix="uvimos" tag="MIS1P0"/>
    <form suffix="uviste" tag="MIS2S0"/>
    <form suffix="uvisteis" tag="MIS2P0"/>
    <form suffix="uviéramos" tag="MSI1P0"/>
    <form suffix="uviéremos" tag="MSF1P0"/>
    <form suffix="uviésemos" tag="MSI1P0"/>
    <form suffix="uvo" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="én" tag="MM02S0" synt="Imperative"/>
  </table>
  <table name="V33" rads=".*[cet][ore]" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: c/e/t + o/r/e + ger -->
    <!-- 9 members -->
    <form suffix="ger" tag="MN0000" synt="Infinitive"/>
    <form suffix="ge" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ge" tag="MM02S0" synt="Imperative"/>
    <form suffix="ged" tag="MM02P0" synt="Imperative"/>
    <form suffix="gemos" tag="MIP1P0"/>
    <form suffix="gen" tag="MIP3P0"/>
    <form suffix="geremos" tag="MIF1P0"/>
    <form suffix="gerá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="gerán" tag="MIF3P0"/>
    <form suffix="gerás" tag="MIF2S0"/>
    <form suffix="geré" tag="MIF1S0"/>
    <form suffix="geréis" tag="MIF2P0"/>
    <form suffix="gería" tag="MIC1S0"/>
    <form suffix="gería" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="geríais" tag="MIC2P0"/>
    <form suffix="geríamos" tag="MIC1P0"/>
    <form suffix="gerían" tag="MIC3P0"/>
    <form suffix="gerías" tag="MIC2S0"/>
    <form suffix="ges" tag="MIP2S0"/>
    <form suffix="gida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="gidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="gido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="gidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="giendo" tag="MG0000"/>
    <form suffix="giera" tag="MSI1S0"/>
    <form suffix="giera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="gierais" tag="MSI2P0"/>
    <form suffix="gieran" tag="MSI3P0"/>
    <form suffix="gieras" tag="MSI2S0"/>
    <form suffix="giere" tag="MSF1S0"/>
    <form suffix="giere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="giereis" tag="MSF2P0"/>
    <form suffix="gieren" tag="MSF3P0"/>
    <form suffix="gieres" tag="MSF2S0"/>
    <form suffix="gieron" tag="MIS3P0"/>
    <form suffix="giese" tag="MSI1S0"/>
    <form suffix="giese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="gieseis" tag="MSI2P0"/>
    <form suffix="giesen" tag="MSI3P0"/>
    <form suffix="gieses" tag="MSI2S0"/>
    <form suffix="gimos" tag="MIS1P0"/>
    <form suffix="giste" tag="MIS2S0"/>
    <form suffix="gisteis" tag="MIS2P0"/>
    <form suffix="giéramos" tag="MSI1P0"/>
    <form suffix="giéremos" tag="MSF1P0"/>
    <form suffix="giésemos" tag="MSI1P0"/>
    <form suffix="gió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="géis" tag="MIP2P0"/>
    <form suffix="gí" tag="MIS1S0"/>
    <form suffix="gía" tag="MII1S0"/>
    <form suffix="gía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="gíais" tag="MII2P0"/>
    <form suffix="gíamos" tag="MII1P0"/>
    <form suffix="gían" tag="MII3P0"/>
    <form suffix="gías" tag="MII2S0"/>
    <form suffix="ja" tag="MM03S0" synt="Imperative"/>
    <form suffix="ja" tag="MSP1S0"/>
    <form suffix="ja" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="jamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="jamos" tag="MSP1P0"/>
    <form suffix="jan" tag="MM03P0" synt="Imperative"/>
    <form suffix="jan" tag="MSP3P0"/>
    <form suffix="jas" tag="MSP2S0"/>
    <form suffix="jo" tag="MIP1S0"/>
    <form suffix="jáis" tag="MSP2P0"/>
  </table>
  <table name="V34" rads=".*[smp]" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: s/m/p + entir -->
    <!-- 9 members -->
    <form suffix="entir" tag="MN0000" synt="Infinitive"/>
    <form suffix="entid" tag="MM02P0" synt="Imperative"/>
    <form suffix="entida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="entidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="entido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="entidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="entimos" tag="MIP1P0"/>
    <form suffix="entimos" tag="MIS1P0"/>
    <form suffix="entiremos" tag="MIF1P0"/>
    <form suffix="entirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="entirán" tag="MIF3P0"/>
    <form suffix="entirás" tag="MIF2S0"/>
    <form suffix="entiré" tag="MIF1S0"/>
    <form suffix="entiréis" tag="MIF2P0"/>
    <form suffix="entiría" tag="MIC1S0"/>
    <form suffix="entiría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="entiríais" tag="MIC2P0"/>
    <form suffix="entiríamos" tag="MIC1P0"/>
    <form suffix="entirían" tag="MIC3P0"/>
    <form suffix="entirías" tag="MIC2S0"/>
    <form suffix="entiste" tag="MIS2S0"/>
    <form suffix="entisteis" tag="MIS2P0"/>
    <form suffix="entí" tag="MIS1S0"/>
    <form suffix="entía" tag="MII1S0"/>
    <form suffix="entía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="entíais" tag="MII2P0"/>
    <form suffix="entíamos" tag="MII1P0"/>
    <form suffix="entían" tag="MII3P0"/>
    <form suffix="entías" tag="MII2S0"/>
    <form suffix="entís" tag="MIP2P0"/>
    <form suffix="ienta" tag="MM03S0" synt="Imperative"/>
    <form suffix="ienta" tag="MSP1S0"/>
    <form suffix="ienta" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ientan" tag="MM03P0" synt="Imperative"/>
    <form suffix="ientan" tag="MSP3P0"/>
    <form suffix="ientas" tag="MSP2S0"/>
    <form suffix="iente" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="iente" tag="MM02S0" synt="Imperative"/>
    <form suffix="ienten" tag="MIP3P0"/>
    <form suffix="ientes" tag="MIP2S0"/>
    <form suffix="iento" tag="MIP1S0"/>
    <form suffix="intamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="intamos" tag="MSP1P0"/>
    <form suffix="intiendo" tag="MG0000"/>
    <form suffix="intiera" tag="MSI1S0"/>
    <form suffix="intiera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="intierais" tag="MSI2P0"/>
    <form suffix="intieran" tag="MSI3P0"/>
    <form suffix="intieras" tag="MSI2S0"/>
    <form suffix="intiere" tag="MSF1S0"/>
    <form suffix="intiere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="intiereis" tag="MSF2P0"/>
    <form suffix="intieren" tag="MSF3P0"/>
    <form suffix="intieres" tag="MSF2S0"/>
    <form suffix="intieron" tag="MIS3P0"/>
    <form suffix="intiese" tag="MSI1S0"/>
    <form suffix="intiese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="intieseis" tag="MSI2P0"/>
    <form suffix="intiesen" tag="MSI3P0"/>
    <form suffix="intieses" tag="MSI2S0"/>
    <form suffix="intiéramos" tag="MSI1P0"/>
    <form suffix="intiéremos" tag="MSF1P0"/>
    <form suffix="intiésemos" tag="MSI1P0"/>
    <form suffix="intió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="intáis" tag="MSP2P0"/>
  </table>
  <table name="V35" rads=".*[oraen][lrntd]" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: o/r/a/e/n + l/r/n/t/d + ir -->
    <!-- 9 members -->
    <form suffix="ir" tag="MN0000" synt="Infinitive"/>
    <form suffix="id" tag="MM02P0" synt="Imperative"/> <!-- Nieves: The only form for Imperative -->
    <form suffix="ida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="idas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="idos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="iendo" tag="MG0000"/>
    <form suffix="iera" tag="MSI1S0"/>
    <form suffix="iera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ierais" tag="MSI2P0"/>
    <form suffix="ieran" tag="MSI3P0"/>
    <form suffix="ieras" tag="MSI2S0"/>
    <form suffix="iere" tag="MSF1S0"/>
    <form suffix="iere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="iereis" tag="MSF2P0"/>
    <form suffix="ieren" tag="MSF3P0"/>
    <form suffix="ieres" tag="MSF2S0"/>
    <form suffix="ieron" tag="MIS3P0"/>
    <form suffix="iese" tag="MSI1S0"/>
    <form suffix="iese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ieseis" tag="MSI2P0"/>
    <form suffix="iesen" tag="MSI3P0"/>
    <form suffix="ieses" tag="MSI2S0"/>
    <form suffix="imos" tag="MIP1P0"/>
    <form suffix="imos" tag="MIS1P0"/>
    <form suffix="iremos" tag="MIF1P0"/>
    <form suffix="irá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="irán" tag="MIF3P0"/>
    <form suffix="irás" tag="MIF2S0"/>
    <form suffix="iré" tag="MIF1S0"/>
    <form suffix="iréis" tag="MIF2P0"/>
    <form suffix="iría" tag="MIC1S0"/>
    <form suffix="iría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="iríais" tag="MIC2P0"/>
    <form suffix="iríamos" tag="MIC1P0"/>
    <form suffix="irían" tag="MIC3P0"/>
    <form suffix="irías" tag="MIC2S0"/>
    <form suffix="iste" tag="MIS2S0"/>
    <form suffix="isteis" tag="MIS2P0"/>
    <form suffix="iéramos" tag="MSI1P0"/>
    <form suffix="iéremos" tag="MSF1P0"/>
    <form suffix="iésemos" tag="MSI1P0"/>
    <form suffix="ió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="í" tag="MIS1S0"/>
    <form suffix="ía" tag="MII1S0"/>
    <form suffix="ía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="íais" tag="MII2P0"/>
    <form suffix="íamos" tag="MII1P0"/>
    <form suffix="ían" tag="MII3P0"/>
    <form suffix="ías" tag="MII2S0"/>
    <form suffix="ís" tag="MIP2P0"/>
  </table>
  <table name="V36" rads=".*" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: olar -->
    <!-- 9 members -->
    <form suffix="olar" tag="MN0000" synt="Infinitive"/>
    <form suffix="olaba" tag="MII1S0"/>
    <form suffix="olaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="olabais" tag="MII2P0"/>
    <form suffix="olaban" tag="MII3P0"/>
    <form suffix="olabas" tag="MII2S0"/>
    <form suffix="olad" tag="MM02P0" synt="Imperative"/>
    <form suffix="olada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="oladas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="olado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="olados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="olamos" tag="MIP1P0"/>
    <form suffix="olamos" tag="MIS1P0"/>
    <form suffix="olando" tag="MG0000"/>
    <form suffix="olara" tag="MSI1S0"/>
    <form suffix="olara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="olarais" tag="MSI2P0"/>
    <form suffix="olaran" tag="MSI3P0"/>
    <form suffix="olaras" tag="MSI2S0"/>
    <form suffix="olare" tag="MSF1S0"/>
    <form suffix="olare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="olareis" tag="MSF2P0"/>
    <form suffix="olaremos" tag="MIF1P0"/>
    <form suffix="olaren" tag="MSF3P0"/>
    <form suffix="olares" tag="MSF2S0"/>
    <form suffix="olaron" tag="MIS3P0"/>
    <form suffix="olará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="olarán" tag="MIF3P0"/>
    <form suffix="olarás" tag="MIF2S0"/>
    <form suffix="olaré" tag="MIF1S0"/>
    <form suffix="olaréis" tag="MIF2P0"/>
    <form suffix="olaría" tag="MIC1S0"/>
    <form suffix="olaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="olaríais" tag="MIC2P0"/>
    <form suffix="olaríamos" tag="MIC1P0"/>
    <form suffix="olarían" tag="MIC3P0"/>
    <form suffix="olarías" tag="MIC2S0"/>
    <form suffix="olase" tag="MSI1S0"/>
    <form suffix="olase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="olaseis" tag="MSI2P0"/>
    <form suffix="olasen" tag="MSI3P0"/>
    <form suffix="olases" tag="MSI2S0"/>
    <form suffix="olaste" tag="MIS2S0"/>
    <form suffix="olasteis" tag="MIS2P0"/>
    <form suffix="olemos" tag="MSP1P0"/>
    <form suffix="olemos" tag="MM01P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="olábamos" tag="MII1P0"/>
    <form suffix="oláis" tag="MIP2P0"/>
    <form suffix="oláramos" tag="MSI1P0"/>
    <form suffix="oláremos" tag="MSF1P0"/>
    <form suffix="olásemos" tag="MSI1P0"/>
    <form suffix="olé" tag="MIS1S0"/>
    <form suffix="oléis" tag="MSP2P0"/>
    <form suffix="oló" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="uela" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uela" tag="MM02S0" synt="Imperative"/>
    <form suffix="uelan" tag="MIP3P0"/>
    <form suffix="uelas" tag="MIP2S0"/>
    <form suffix="uele" tag="MSP1S0"/>
    <form suffix="uele" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uele" tag="MM03S0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="uelen" tag="MSP3P0"/>
    <form suffix="uelen" tag="MM03P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ueles" tag="MSP2S0"/>
    <form suffix="uelo" tag="MIP1S0"/>
  </table>
  <table name="V37" rads=".*[rlugt][iaul][ñl]" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: r/l/u/g/t + i/a/u/l + ñ/l + ir -->
    <!-- 9 members --> 
    <form suffix="ir" tag="MN0000" synt="Infinitive"/>
    <form suffix="a" tag="MSP1S0"/>
    <form suffix="a" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="a" tag="MM03S0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="amos" tag="MSP1P0"/>
    <form suffix="amos" tag="MM01P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="an" tag="MSP3P0"/>
    <form suffix="an" tag="MM03P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="as" tag="MSP2S0"/>
    <form suffix="e" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="e" tag="MM02S0" synt="Imperative"/>
    <form suffix="en" tag="MIP3P0"/>
    <form suffix="endo" tag="MG0000"/>
    <form suffix="era" tag="MSI1S0"/>
    <form suffix="era" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="erais" tag="MSI2P0"/>
    <form suffix="eran" tag="MSI3P0"/>
    <form suffix="eras" tag="MSI2S0"/>
    <form suffix="ere" tag="MSF1S0"/>
    <form suffix="ere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ereis" tag="MSF2P0"/>
    <form suffix="eren" tag="MSF3P0"/>
    <form suffix="eres" tag="MSF2S0"/>
    <form suffix="eron" tag="MIS3P0"/>
    <form suffix="es" tag="MIP2S0"/>
    <form suffix="ese" tag="MSI1S0"/>
    <form suffix="ese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="eseis" tag="MSI2P0"/>
    <form suffix="esen" tag="MSI3P0"/>
    <form suffix="eses" tag="MSI2S0"/>
    <form suffix="id" tag="MM02P0" synt="Imperative"/>
    <form suffix="ida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="idas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="idos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="imos" tag="MIP1P0"/>
    <form suffix="imos" tag="MIS1P0"/>
    <form suffix="iremos" tag="MIF1P0"/>
    <form suffix="irá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="irán" tag="MIF3P0"/>
    <form suffix="irás" tag="MIF2S0"/>
    <form suffix="iré" tag="MIF1S0"/>
    <form suffix="iréis" tag="MIF2P0"/>
    <form suffix="iría" tag="MIC1S0"/>
    <form suffix="iría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="iríais" tag="MIC2P0"/>
    <form suffix="iríamos" tag="MIC1P0"/>
    <form suffix="irían" tag="MIC3P0"/>
    <form suffix="irías" tag="MIC2S0"/>
    <form suffix="iste" tag="MIS2S0"/>
    <form suffix="isteis" tag="MIS2P0"/>
    <form suffix="o" tag="MIP1S0"/>
    <form suffix="áis" tag="MSP2P0"/>
    <form suffix="éramos" tag="MSI1P0"/>
    <form suffix="éremos" tag="MSF1P0"/>
    <form suffix="ésemos" tag="MSI1P0"/>
    <form suffix="í" tag="MIS1S0"/>
    <form suffix="ía" tag="MII1S0"/>
    <form suffix="ía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="íais" tag="MII2P0"/>
    <form suffix="íamos" tag="MII1P0"/>
    <form suffix="ían" tag="MII3P0"/>
    <form suffix="ías" tag="MII2S0"/>
    <form suffix="ís" tag="MIP2P0"/>
    <form suffix="ó" tag="MIS3S0" synt="ThirdSing"/>
  </table>
  <table name="V38" rads=".*v" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: v + ertir -->
    <!-- 8 members -->
    <form suffix="ertir" tag="MN0000" synt="Infinitive"/>
    <form suffix="ertid" tag="MM02P0" synt="Imperative"/>
    <form suffix="ertida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ertidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ertido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ertidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ertimos" tag="MIP1P0"/>
    <form suffix="ertimos" tag="MIS1P0"/>
    <form suffix="ertiremos" tag="MIF1P0"/>
    <form suffix="ertirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="ertirán" tag="MIF3P0"/>
    <form suffix="ertirás" tag="MIF2S0"/>
    <form suffix="ertiré" tag="MIF1S0"/>
    <form suffix="ertiréis" tag="MIF2P0"/>
    <form suffix="ertiría" tag="MIC1S0"/>
    <form suffix="ertiría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ertiríais" tag="MIC2P0"/>
    <form suffix="ertiríamos" tag="MIC1P0"/>
    <form suffix="ertirían" tag="MIC3P0"/>
    <form suffix="ertirías" tag="MIC2S0"/>
    <form suffix="ertiste" tag="MIS2S0"/>
    <form suffix="ertisteis" tag="MIS2P0"/>
    <form suffix="ertí" tag="MIS1S0"/>
    <form suffix="ertía" tag="MII1S0"/>
    <form suffix="ertía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="ertíais" tag="MII2P0"/>
    <form suffix="ertíamos" tag="MII1P0"/>
    <form suffix="ertían" tag="MII3P0"/>
    <form suffix="ertías" tag="MII2S0"/>
    <form suffix="ertís" tag="MIP2P0"/>
    <form suffix="ierta" tag="MM03S0" synt="Imperative"/>
    <form suffix="ierta" tag="MSP1S0"/>
    <form suffix="ierta" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="iertan" tag="MM03P0" synt="Imperative"/>
    <form suffix="iertan" tag="MSP3P0"/>
    <form suffix="iertas" tag="MSP2S0"/>
    <form suffix="ierte" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ierte" tag="MM02S0" synt="Imperative"/>
    <form suffix="ierten" tag="MIP3P0"/>
    <form suffix="iertes" tag="MIP2S0"/>
    <form suffix="ierto" tag="MIP1S0"/>
    <form suffix="irtamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="irtamos" tag="MSP1P0"/>
    <form suffix="irtiendo" tag="MG0000"/>
    <form suffix="irtiera" tag="MSI1S0"/>
    <form suffix="irtiera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="irtierais" tag="MSI2P0"/>
    <form suffix="irtieran" tag="MSI3P0"/>
    <form suffix="irtieras" tag="MSI2S0"/>
    <form suffix="irtiere" tag="MSF1S0"/>
    <form suffix="irtiere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="irtiereis" tag="MSF2P0"/>
    <form suffix="irtieren" tag="MSF3P0"/>
    <form suffix="irtieres" tag="MSF2S0"/>
    <form suffix="irtieron" tag="MIS3P0"/>
    <form suffix="irtiese" tag="MSI1S0"/>
    <form suffix="irtiese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="irtieseis" tag="MSI2P0"/>
    <form suffix="irtiesen" tag="MSI3P0"/>
    <form suffix="irtieses" tag="MSI2S0"/>
    <form suffix="irtiéramos" tag="MSI1P0"/>
    <form suffix="irtiéremos" tag="MSF1P0"/>
    <form suffix="irtiésemos" tag="MSI1P0"/>
    <form suffix="irtió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="irtáis" tag="MSP2P0"/>
  </table>
  <table name="V39" rads=".*[vs]" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: v/s + olver -->
    <!-- 8 members -->
    <form suffix="olver" tag="MN0000" synt="Infinitive"/>
    <form suffix="olvamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="olvamos" tag="MSP1P0"/>
    <form suffix="olved" tag="MM02P0" synt="Imperative"/>
    <form suffix="olvemos" tag="MIP1P0"/>
    <form suffix="olveremos" tag="MIF1P0"/>
    <form suffix="olverá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="olverán" tag="MIF3P0"/>
    <form suffix="olverás" tag="MIF2S0"/>
    <form suffix="olveré" tag="MIF1S0"/>
    <form suffix="olveréis" tag="MIF2P0"/>
    <form suffix="olvería" tag="MIC1S0"/>
    <form suffix="olvería" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="olveríais" tag="MIC2P0"/>
    <form suffix="olveríamos" tag="MIC1P0"/>
    <form suffix="olverían" tag="MIC3P0"/>
    <form suffix="olverías" tag="MIC2S0"/>
    <form suffix="olviendo" tag="MG0000"/>
    <form suffix="olviera" tag="MSI1S0"/>
    <form suffix="olviera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="olvierais" tag="MSI2P0"/>
    <form suffix="olvieran" tag="MSI3P0"/>
    <form suffix="olvieras" tag="MSI2S0"/>
    <form suffix="olviere" tag="MSF1S0"/>
    <form suffix="olviere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="olviereis" tag="MSF2P0"/>
    <form suffix="olvieren" tag="MSF3P0"/>
    <form suffix="olvieres" tag="MSF2S0"/>
    <form suffix="olvieron" tag="MIS3P0"/>
    <form suffix="olviese" tag="MSI1S0"/>
    <form suffix="olviese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="olvieseis" tag="MSI2P0"/>
    <form suffix="olviesen" tag="MSI3P0"/>
    <form suffix="olvieses" tag="MSI2S0"/>
    <form suffix="olvimos" tag="MIS1P0"/>
    <form suffix="olviste" tag="MIS2S0"/>
    <form suffix="olvisteis" tag="MIS2P0"/>
    <form suffix="olviéramos" tag="MSI1P0"/>
    <form suffix="olviéremos" tag="MSF1P0"/>
    <form suffix="olviésemos" tag="MSI1P0"/>
    <form suffix="olvió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="olváis" tag="MSP2P0"/>
    <form suffix="olvéis" tag="MIP2P0"/>
    <form suffix="olví" tag="MIS1S0"/>
    <form suffix="olvía" tag="MII1S0"/>
    <form suffix="olvía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="olvíais" tag="MII2P0"/>
    <form suffix="olvíamos" tag="MII1P0"/>
    <form suffix="olvían" tag="MII3P0"/>
    <form suffix="olvías" tag="MII2S0"/>
    <form suffix="uelta" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ueltas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="uelto" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ueltos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="uelva" tag="MM03S0" synt="Imperative"/>
    <form suffix="uelva" tag="MSP1S0"/>
    <form suffix="uelva" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uelvan" tag="MM03P0" synt="Imperative"/>
    <form suffix="uelvan" tag="MSP3P0"/>
    <form suffix="uelvas" tag="MSP2S0"/>
    <form suffix="uelve" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uelve" tag="MM02S0" synt="Imperative"/>
    <form suffix="uelven" tag="MIP3P0"/>
    <form suffix="uelves" tag="MIP2S0"/>
    <form suffix="uelvo" tag="MIP1S0"/>
  </table>
  <table name="V40" rads=".*[trcse][ian]g" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: t/r/c/s/e + i/a/n + g + uar -->
    <!-- 8 members -->
    <form suffix="uar" tag="MN0000" synt="Infinitive"/>
    <form suffix="ua" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ua" tag="MM02S0" synt="Imperative"/>
    <form suffix="uaba" tag="MII1S0"/>
    <form suffix="uaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="uabais" tag="MII2P0"/>
    <form suffix="uaban" tag="MII3P0"/>
    <form suffix="uabas" tag="MII2S0"/>
    <form suffix="uad" tag="MM02P0" synt="Imperative"/>
    <form suffix="uada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="uadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="uado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="uados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="uamos" tag="MIP1P0"/>
    <form suffix="uamos" tag="MIS1P0"/>
    <form suffix="uan" tag="MIP3P0"/>
    <form suffix="uando" tag="MG0000"/>
    <form suffix="uara" tag="MSI1S0"/>
    <form suffix="uara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="uarais" tag="MSI2P0"/>
    <form suffix="uaran" tag="MSI3P0"/>
    <form suffix="uaras" tag="MSI2S0"/>
    <form suffix="uare" tag="MSF1S0"/>
    <form suffix="uare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="uareis" tag="MSF2P0"/>
    <form suffix="uaremos" tag="MIF1P0"/>
    <form suffix="uaren" tag="MSF3P0"/>
    <form suffix="uares" tag="MSF2S0"/>
    <form suffix="uaron" tag="MIS3P0"/>
    <form suffix="uará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="uarán" tag="MIF3P0"/>
    <form suffix="uarás" tag="MIF2S0"/>
    <form suffix="uaré" tag="MIF1S0"/>
    <form suffix="uaréis" tag="MIF2P0"/>
    <form suffix="uaría" tag="MIC1S0"/>
    <form suffix="uaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="uaríais" tag="MIC2P0"/>
    <form suffix="uaríamos" tag="MIC1P0"/>
    <form suffix="uarían" tag="MIC3P0"/>
    <form suffix="uarías" tag="MIC2S0"/>
    <form suffix="uas" tag="MIP2S0"/>
    <form suffix="uase" tag="MSI1S0"/>
    <form suffix="uase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="uaseis" tag="MSI2P0"/>
    <form suffix="uasen" tag="MSI3P0"/>
    <form suffix="uases" tag="MSI2S0"/>
    <form suffix="uaste" tag="MIS2S0"/>
    <form suffix="uasteis" tag="MIS2P0"/>
    <form suffix="uo" tag="MIP1S0"/>
    <form suffix="uábamos" tag="MII1P0"/>
    <form suffix="uáis" tag="MIP2P0"/>
    <form suffix="uáramos" tag="MSI1P0"/>
    <form suffix="uáremos" tag="MSF1P0"/>
    <form suffix="uásemos" tag="MSI1P0"/>
    <form suffix="uó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="üe" tag="MM03S0" synt="Imperative"/>
    <form suffix="üe" tag="MSP1S0"/>
    <form suffix="üe" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="üemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="üemos" tag="MSP1P0"/>
    <form suffix="üen" tag="MM03P0" synt="Imperative"/>
    <form suffix="üen" tag="MSP3P0"/>
    <form suffix="ües" tag="MSP2S0"/>
    <form suffix="üé" tag="MIS1S0"/>
    <form suffix="üéis" tag="MSP2P0"/>
  </table>
  <table name="V41" rads=".*[nlai]g" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: n/l/a/i + g + uar -->
    <!-- 8 members -->
    <form suffix="uar" tag="MN0000" synt="Infinitive"/>
    <form suffix="ua" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ua" tag="MM02S0" synt="Imperative"/>
    <form suffix="uaba" tag="MII1S0"/>
    <form suffix="uaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="uabais" tag="MII2P0"/>
    <form suffix="uaban" tag="MII3P0"/>
    <form suffix="uabas" tag="MII2S0"/>
    <form suffix="uad" tag="MM02P0" synt="Imperative"/>
    <form suffix="uada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="uadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="uado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="uados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="uamos" tag="MIP1P0"/>
    <form suffix="uamos" tag="MIS1P0"/>
    <form suffix="uan" tag="MIP3P0"/>
    <form suffix="uando" tag="MG0000"/>
    <form suffix="uara" tag="MSI1S0"/>
    <form suffix="uara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="uarais" tag="MSI2P0"/>
    <form suffix="uaran" tag="MSI3P0"/>
    <form suffix="uaras" tag="MSI2S0"/>
    <form suffix="uare" tag="MSF1S0"/>
    <form suffix="uare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="uareis" tag="MSF2P0"/>
    <form suffix="uaremos" tag="MIF1P0"/>
    <form suffix="uaren" tag="MSF3P0"/>
    <form suffix="uares" tag="MSF2S0"/>
    <form suffix="uaron" tag="MIS3P0"/>
    <form suffix="uará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="uarán" tag="MIF3P0"/>
    <form suffix="uarás" tag="MIF2S0"/>
    <form suffix="uaré" tag="MIF1S0"/>
    <form suffix="uaréis" tag="MIF2P0"/>
    <form suffix="uaría" tag="MIC1S0"/>
    <form suffix="uaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="uaríais" tag="MIC2P0"/>
    <form suffix="uaríamos" tag="MIC1P0"/>
    <form suffix="uarían" tag="MIC3P0"/>
    <form suffix="uarías" tag="MIC2S0"/>
    <form suffix="uas" tag="MIP2S0"/>
    <form suffix="uase" tag="MSI1S0"/>
    <form suffix="uase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="uaseis" tag="MSI2P0"/>
    <form suffix="uasen" tag="MSI3P0"/>
    <form suffix="uases" tag="MSI2S0"/>
    <form suffix="uaste" tag="MIS2S0"/>
    <form suffix="uasteis" tag="MIS2P0"/>
    <form suffix="uo" tag="MIP1S0"/>
    <form suffix="uábamos" tag="MII1P0"/>
    <form suffix="uáis" tag="MIP2P0"/>
    <form suffix="uáramos" tag="MSI1P0"/>
    <form suffix="uáremos" tag="MSF1P0"/>
    <form suffix="uásemos" tag="MSI1P0"/>
    <form suffix="uó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="üe" tag="MSP1S0"/>
    <form suffix="üe" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="üe" tag="MM03S0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="üemos" tag="MSP1P0"/>
    <form suffix="üemos" tag="MM01P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="üen" tag="MSP3P0"/>
    <form suffix="üen" tag="MM03P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ües" tag="MSP2S0"/>
    <form suffix="üé" tag="MIS1S0"/>
    <form suffix="üéis" tag="MSP2P0"/>
  </table>
  <table name="V42" rads=".*[mfgb][uar][lu][lñ]" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: m/f/g/b + /u/a/r + l/u + l/ñ + ir -->
    <!-- 8 members -->
    <form suffix="ir" tag="MN0000" synt="Infinitive"/>
    <form suffix="a" tag="MM03S0" synt="Imperative"/>
    <form suffix="a" tag="MSP1S0"/>
    <form suffix="a" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="amos" tag="MM01P0" synt="Imperative"/>
    <form suffix="amos" tag="MSP1P0"/>
    <form suffix="an" tag="MM03P0" synt="Imperative"/>
    <form suffix="an" tag="MSP3P0"/>
    <form suffix="as" tag="MSP2S0"/>
    <form suffix="e" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="e" tag="MM02S0" synt="Imperative"/>
    <form suffix="en" tag="MIP3P0"/>
    <form suffix="endo" tag="MG0000"/>
    <form suffix="era" tag="MSI1S0"/>
    <form suffix="era" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="erais" tag="MSI2P0"/>
    <form suffix="eran" tag="MSI3P0"/>
    <form suffix="eras" tag="MSI2S0"/>
    <form suffix="ere" tag="MSF1S0"/>
    <form suffix="ere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ereis" tag="MSF2P0"/>
    <form suffix="eren" tag="MSF3P0"/>
    <form suffix="eres" tag="MSF2S0"/>
    <form suffix="eron" tag="MIS3P0"/>
    <form suffix="es" tag="MIP2S0"/>
    <form suffix="ese" tag="MSI1S0"/>
    <form suffix="ese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="eseis" tag="MSI2P0"/>
    <form suffix="esen" tag="MSI3P0"/>
    <form suffix="eses" tag="MSI2S0"/>
    <form suffix="id" tag="MM02P0" synt="Imperative"/>
    <form suffix="ida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="idas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="idos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="imos" tag="MIP1P0"/>
    <form suffix="imos" tag="MIS1P0"/>
    <form suffix="iremos" tag="MIF1P0"/>
    <form suffix="irá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="irán" tag="MIF3P0"/>
    <form suffix="irás" tag="MIF2S0"/>
    <form suffix="iré" tag="MIF1S0"/>
    <form suffix="iréis" tag="MIF2P0"/>
    <form suffix="iría" tag="MIC1S0"/>
    <form suffix="iría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="iríais" tag="MIC2P0"/>
    <form suffix="iríamos" tag="MIC1P0"/>
    <form suffix="irían" tag="MIC3P0"/>
    <form suffix="irías" tag="MIC2S0"/>
    <form suffix="iste" tag="MIS2S0"/>
    <form suffix="isteis" tag="MIS2P0"/>
    <form suffix="o" tag="MIP1S0"/>
    <form suffix="áis" tag="MSP2P0"/>
    <form suffix="éramos" tag="MSI1P0"/>
    <form suffix="éremos" tag="MSF1P0"/>
    <form suffix="ésemos" tag="MSI1P0"/>
    <form suffix="í" tag="MIS1S0"/>
    <form suffix="ía" tag="MII1S0"/>
    <form suffix="ía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="íais" tag="MII2P0"/>
    <form suffix="íamos" tag="MII1P0"/>
    <form suffix="ían" tag="MII3P0"/>
    <form suffix="ías" tag="MII2S0"/>
    <form suffix="ís" tag="MIP2P0"/>
    <form suffix="ó" tag="MIS3S0" synt="ThirdSing"/>
  </table>
  <table name="V43" rads=".*v" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: v + enir -->
    <!-- 8 members -->
    <form suffix="enir" tag="MN0000" synt="Infinitive"/>
    <form suffix="en" tag="MM02S0" synt="Imperative"/>
    <form suffix="endremos" tag="MIF1P0"/>
    <form suffix="endrá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="endrán" tag="MIF3P0"/>
    <form suffix="endrás" tag="MIF2S0"/>
    <form suffix="endré" tag="MIF1S0"/>
    <form suffix="endréis" tag="MIF2P0"/>
    <form suffix="endría" tag="MIC1S0"/>
    <form suffix="endría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="endríais" tag="MIC2P0"/>
    <form suffix="endríamos" tag="MIC1P0"/>
    <form suffix="endrían" tag="MIC3P0"/>
    <form suffix="endrías" tag="MIC2S0"/>
    <form suffix="enga" tag="MM03S0" synt="Imperative"/>
    <form suffix="enga" tag="MSP1S0"/>
    <form suffix="enga" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="engamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="engamos" tag="MSP1P0"/>
    <form suffix="engan" tag="MM03P0" synt="Imperative"/>
    <form suffix="engan" tag="MSP3P0"/>
    <form suffix="engas" tag="MSP2S0"/>
    <form suffix="engo" tag="MIP1S0"/>
    <form suffix="engáis" tag="MSP2P0"/>
    <form suffix="enid" tag="MM02P0" synt="Imperative"/>
    <form suffix="enida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="enidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="enido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="enidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="enimos" tag="MIP1P0"/>
    <form suffix="enía" tag="MII1S0"/>
    <form suffix="enía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="eníais" tag="MII2P0"/>
    <form suffix="eníamos" tag="MII1P0"/>
    <form suffix="enían" tag="MII3P0"/>
    <form suffix="enías" tag="MII2S0"/>
    <form suffix="enís" tag="MIP2P0"/>
    <form suffix="iene" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ienen" tag="MIP3P0"/>
    <form suffix="ienes" tag="MIP2S0"/>
    <form suffix="ine" tag="MIS1S0"/>
    <form suffix="iniendo" tag="MG0000"/>
    <form suffix="iniera" tag="MSI1S0"/>
    <form suffix="iniera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="inierais" tag="MSI2P0"/>
    <form suffix="inieran" tag="MSI3P0"/>
    <form suffix="inieras" tag="MSI2S0"/>
    <form suffix="iniere" tag="MSF1S0"/>
    <form suffix="iniere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="iniereis" tag="MSF2P0"/>
    <form suffix="inieren" tag="MSF3P0"/>
    <form suffix="inieres" tag="MSF2S0"/>
    <form suffix="inieron" tag="MIS3P0"/>
    <form suffix="iniese" tag="MSI1S0"/>
    <form suffix="iniese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="inieseis" tag="MSI2P0"/>
    <form suffix="iniesen" tag="MSI3P0"/>
    <form suffix="inieses" tag="MSI2S0"/>
    <form suffix="inimos" tag="MIS1P0"/>
    <form suffix="iniste" tag="MIS2S0"/>
    <form suffix="inisteis" tag="MIS2P0"/>
    <form suffix="iniéramos" tag="MSI1P0"/>
    <form suffix="iniéremos" tag="MSF1P0"/>
    <form suffix="iniésemos" tag="MSI1P0"/>
    <form suffix="ino" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="én" tag="MM02S0" synt="Imperative"/>
  </table>
  <table name="V44" rads=".*[thsc]" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: t/h/s + errar -->
    <!-- 7 members -->
    <form suffix="errar" tag="MN0000" synt="Infinitive"/>
    <form suffix="erraba" tag="MII1S0"/>
    <form suffix="erraba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="errabais" tag="MII2P0"/>
    <form suffix="erraban" tag="MII3P0"/>
    <form suffix="errabas" tag="MII2S0"/>
    <form suffix="errad" tag="MM02P0" synt="Imperative"/>
    <form suffix="errada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="erradas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="errado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="errados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="erramos" tag="MIP1P0"/>
    <form suffix="erramos" tag="MIS1P0"/>
    <form suffix="errando" tag="MG0000"/>
    <form suffix="errara" tag="MSI1S0"/>
    <form suffix="errara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="errarais" tag="MSI2P0"/>
    <form suffix="erraran" tag="MSI3P0"/>
    <form suffix="erraras" tag="MSI2S0"/>
    <form suffix="errare" tag="MSF1S0"/>
    <form suffix="errare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="errareis" tag="MSF2P0"/>
    <form suffix="erraremos" tag="MIF1P0"/>
    <form suffix="erraren" tag="MSF3P0"/>
    <form suffix="errares" tag="MSF2S0"/>
    <form suffix="erraron" tag="MIS3P0"/>
    <form suffix="errará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="errarán" tag="MIF3P0"/>
    <form suffix="errarás" tag="MIF2S0"/>
    <form suffix="erraré" tag="MIF1S0"/>
    <form suffix="erraréis" tag="MIF2P0"/>
    <form suffix="erraría" tag="MIC1S0"/>
    <form suffix="erraría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="erraríais" tag="MIC2P0"/>
    <form suffix="erraríamos" tag="MIC1P0"/>
    <form suffix="errarían" tag="MIC3P0"/>
    <form suffix="errarías" tag="MIC2S0"/>
    <form suffix="errase" tag="MSI1S0"/>
    <form suffix="errase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="erraseis" tag="MSI2P0"/>
    <form suffix="errasen" tag="MSI3P0"/>
    <form suffix="errases" tag="MSI2S0"/>
    <form suffix="erraste" tag="MIS2S0"/>
    <form suffix="errasteis" tag="MIS2P0"/>
    <form suffix="erremos" tag="MM01P0" synt="Imperative"/>
    <form suffix="erremos" tag="MSP1P0"/>
    <form suffix="errábamos" tag="MII1P0"/>
    <form suffix="erráis" tag="MIP2P0"/>
    <form suffix="erráramos" tag="MSI1P0"/>
    <form suffix="erráremos" tag="MSF1P0"/>
    <form suffix="errásemos" tag="MSI1P0"/>
    <form suffix="erré" tag="MIS1S0"/>
    <form suffix="erréis" tag="MSP2P0"/>
    <form suffix="erró" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ierra" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ierra" tag="MM02S0" synt="Imperative"/>
    <form suffix="ierran" tag="MIP3P0"/>
    <form suffix="ierras" tag="MIP2S0"/>
    <form suffix="ierre" tag="MM03S0" synt="Imperative"/>
    <form suffix="ierre" tag="MSP1S0"/>
    <form suffix="ierre" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ierren" tag="MM03P0" synt="Imperative"/>
    <form suffix="ierren" tag="MSP3P0"/>
    <form suffix="ierres" tag="MSP2S0"/>
    <form suffix="ierro" tag="MIP1S0"/>
  </table>
  <!--table name="V45" rads=".*[osare][nader]v" fast="-"--> <!-- Nieves: verbos tercera conjugación en infinitivo: o/s/a/r/e + n/a/d/e/r + v + ir -->
  <table name="V45" rads=".*v" fast="-">
    <!-- Nieves: tabla para el verbo venir (actualmente único verbo) -->
    <form suffix="enir" tag="MN0000" synt="Infinitive"/>
    <form suffix="endremos" tag="MIF1P0"/>
    <form suffix="endrá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="endrán" tag="MIF3P0"/>
    <form suffix="endrás" tag="MIF2S0"/>
    <form suffix="endré" tag="MIF1S0"/>
    <form suffix="endréis" tag="MIF2P0"/>
    <form suffix="endría" tag="MIC1S0"/>
    <form suffix="endría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="endríais" tag="MIC2P0"/>
    <form suffix="endríamos" tag="MIC1P0"/>
    <form suffix="endrían" tag="MIC3P0"/>
    <form suffix="endrías" tag="MIC2S0"/>
    <form suffix="enga" tag="MSP1S0"/>
    <form suffix="enga" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="enga" tag="MM03S0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="engamos" tag="MSP1P0"/>
    <form suffix="engamos" tag="MM01P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="engan" tag="MSP3P0"/>
    <form suffix="engan" tag="MM03P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="engas" tag="MSP2S0"/>
    <form suffix="engo" tag="MIP1S0"/>
    <form suffix="engáis" tag="MSP2P0"/>
    <form suffix="enid" tag="MM02P0" synt="Imperative"/>
    <form suffix="enida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="enidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="enido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="enidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="enimos" tag="MIP1P0"/>
    <form suffix="enía" tag="MII1S0"/>
    <form suffix="enía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="eníais" tag="MII2P0"/>
    <form suffix="eníamos" tag="MII1P0"/>
    <form suffix="enían" tag="MII3P0"/>
    <form suffix="enías" tag="MII2S0"/>
    <form suffix="enís" tag="MIP2P0"/>
    <form suffix="iene" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ienen" tag="MIP3P0"/>
    <form suffix="ienes" tag="MIP2S0"/>
    <form suffix="ine" tag="MIS1S0"/>
    <form suffix="iniendo" tag="MG0000"/>
    <form suffix="iniera" tag="MSI1S0"/>
    <form suffix="iniera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="inierais" tag="MSI2P0"/>
    <form suffix="inieran" tag="MSI3P0"/>
    <form suffix="inieras" tag="MSI2S0"/>
    <form suffix="iniere" tag="MSF1S0"/>
    <form suffix="iniere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="iniereis" tag="MSF2P0"/>
    <form suffix="inieren" tag="MSF3P0"/>
    <form suffix="inieres" tag="MSF2S0"/>
    <form suffix="inieron" tag="MIS3P0"/>
    <form suffix="iniese" tag="MSI1S0"/>
    <form suffix="iniese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="inieseis" tag="MSI2P0"/>
    <form suffix="iniesen" tag="MSI3P0"/>
    <form suffix="inieses" tag="MSI2S0"/>
    <form suffix="inimos" tag="MIS1P0"/>
    <form suffix="iniste" tag="MIS2S0"/>
    <form suffix="inisteis" tag="MIS2P0"/>
    <form suffix="iniéramos" tag="MSI1P0"/>
    <form suffix="iniéremos" tag="MSF1P0"/>
    <form suffix="iniésemos" tag="MSI1P0"/>
    <form suffix="ino" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="én" tag="MM02S0" synt="Imperative"/>
  </table>
  <table name="V46" rads=".*(as|desh|f|desenc|semic|entrec)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: as/desh/f/desenc/semic/entrec + errar -->
    <!-- 6 members -->
    <form suffix="errar" tag="MN0000" synt="Infinitive"/>
    <form suffix="erraba" tag="MII1S0"/>
    <form suffix="erraba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="errabais" tag="MII2P0"/>
    <form suffix="erraban" tag="MII3P0"/>
    <form suffix="errabas" tag="MII2S0"/>
    <form suffix="errad" tag="MM02P0" synt="Imperative"/>
    <form suffix="errada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="erradas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="errado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="errados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="erramos" tag="MIP1P0"/>
    <form suffix="erramos" tag="MIS1P0"/>
    <form suffix="errando" tag="MG0000"/>
    <form suffix="errara" tag="MSI1S0"/>
    <form suffix="errara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="errarais" tag="MSI2P0"/>
    <form suffix="erraran" tag="MSI3P0"/>
    <form suffix="erraras" tag="MSI2S0"/>
    <form suffix="errare" tag="MSF1S0"/>
    <form suffix="errare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="errareis" tag="MSF2P0"/>
    <form suffix="erraremos" tag="MIF1P0"/>
    <form suffix="erraren" tag="MSF3P0"/>
    <form suffix="errares" tag="MSF2S0"/>
    <form suffix="erraron" tag="MIS3P0"/>
    <form suffix="errará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="errarán" tag="MIF3P0"/>
    <form suffix="errarás" tag="MIF2S0"/>
    <form suffix="erraré" tag="MIF1S0"/>
    <form suffix="erraréis" tag="MIF2P0"/>
    <form suffix="erraría" tag="MIC1S0"/>
    <form suffix="erraría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="erraríais" tag="MIC2P0"/>
    <form suffix="erraríamos" tag="MIC1P0"/>
    <form suffix="errarían" tag="MIC3P0"/>
    <form suffix="errarías" tag="MIC2S0"/>
    <form suffix="errase" tag="MSI1S0"/>
    <form suffix="errase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="erraseis" tag="MSI2P0"/>
    <form suffix="errasen" tag="MSI3P0"/>
    <form suffix="errases" tag="MSI2S0"/>
    <form suffix="erraste" tag="MIS2S0"/>
    <form suffix="errasteis" tag="MIS2P0"/>
    <form suffix="erremos" tag="MSP1P0"/>
    <form suffix="erremos" tag="MM01P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="errábamos" tag="MII1P0"/>
    <form suffix="erráis" tag="MIP2P0"/>
    <form suffix="erráramos" tag="MSI1P0"/>
    <form suffix="erráremos" tag="MSF1P0"/>
    <form suffix="errásemos" tag="MSI1P0"/>
    <form suffix="erré" tag="MIS1S0"/>
    <form suffix="erréis" tag="MSP2P0"/>
    <form suffix="erró" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ierra" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ierra" tag="MM02S0" synt="Imperative"/>
    <form suffix="ierran" tag="MIP3P0"/>
    <form suffix="ierras" tag="MIP2S0"/>
    <form suffix="ierre" tag="MSP1S0"/>
    <form suffix="ierre" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ierre" tag="MM03S0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ierren" tag="MSP3P0"/>
    <form suffix="ierren" tag="MM03P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ierres" tag="MSP2S0"/>
    <form suffix="ierro" tag="MIP1S0"/>
  </table>
  <table name="V47" rads=".*(costr|estr|astr|ret|h|desc)" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: costr/estr/astr/ret/h/desc + ir -->
    <!-- 6 members -->
    <form suffix="eñir" tag="MN0000" synt="Infinitive"/>
    <form suffix="eñid" tag="MM02P0" synt="Imperative"/>
    <form suffix="eñida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="eñidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="eñido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="eñidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="eñimos" tag="MIP1P0"/>
    <form suffix="eñimos" tag="MIS1P0"/>
    <form suffix="eñiremos" tag="MIF1P0"/>
    <form suffix="eñirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="eñirán" tag="MIF3P0"/>
    <form suffix="eñirás" tag="MIF2S0"/>
    <form suffix="eñiré" tag="MIF1S0"/>
    <form suffix="eñiréis" tag="MIF2P0"/>
    <form suffix="eñiría" tag="MIC1S0"/>
    <form suffix="eñiría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="eñiríais" tag="MIC2P0"/>
    <form suffix="eñiríamos" tag="MIC1P0"/>
    <form suffix="eñirían" tag="MIC3P0"/>
    <form suffix="eñirías" tag="MIC2S0"/>
    <form suffix="eñiste" tag="MIS2S0"/>
    <form suffix="eñisteis" tag="MIS2P0"/>
    <form suffix="eñí" tag="MIS1S0"/>
    <form suffix="eñía" tag="MII1S0"/>
    <form suffix="eñía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="eñíais" tag="MII2P0"/>
    <form suffix="eñíamos" tag="MII1P0"/>
    <form suffix="eñían" tag="MII3P0"/>
    <form suffix="eñías" tag="MII2S0"/>
    <form suffix="eñís" tag="MIP2P0"/>
    <form suffix="iña" tag="MSP1S0"/>
    <form suffix="iña" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="iña" tag="MM03S0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="iñamos" tag="MSP1P0"/>
    <form suffix="iñamos" tag="MM01P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="iñan" tag="MSP3P0"/>
    <form suffix="iñan" tag="MM03P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="iñas" tag="MSP2S0"/>
    <form suffix="iñe" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="iñe" tag="MM02S0" synt="Imperative"/>
    <form suffix="iñen" tag="MIP3P0"/>
    <form suffix="iñendo" tag="MG0000"/>
    <form suffix="iñera" tag="MSI1S0"/>
    <form suffix="iñera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="iñerais" tag="MSI2P0"/>
    <form suffix="iñeran" tag="MSI3P0"/>
    <form suffix="iñeras" tag="MSI2S0"/>
    <form suffix="iñere" tag="MSF1S0"/>
    <form suffix="iñere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="iñereis" tag="MSF2P0"/>
    <form suffix="iñeren" tag="MSF3P0"/>
    <form suffix="iñeres" tag="MSF2S0"/>
    <form suffix="iñeron" tag="MIS3P0"/>
    <form suffix="iñes" tag="MIP2S0"/>
    <form suffix="iñese" tag="MSI1S0"/>
    <form suffix="iñese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="iñeseis" tag="MSI2P0"/>
    <form suffix="iñesen" tag="MSI3P0"/>
    <form suffix="iñeses" tag="MSI2S0"/>
    <form suffix="iño" tag="MIP1S0"/>
    <form suffix="iñáis" tag="MSP2P0"/>
    <form suffix="iñéramos" tag="MSI1P0"/>
    <form suffix="iñéremos" tag="MSF1P0"/>
    <form suffix="iñésemos" tag="MSI1P0"/>
    <form suffix="iñó" tag="MIS3S0" synt="ThirdSing"/>
  </table>
  <table name="V48" rads=".*(conver|tergiver|disper|cur|concur|malver|discur)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: conver/tergiver/disper/cur/concur/malver + sar -->
    <!-- 6 members -->
    <form suffix="sar" tag="MN0000" synt="Infinitive"/>
    <form suffix="se" tag="MSP1S0"/>
    <form suffix="se" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="sa" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="sa" tag="MM02S0" synt="Imperative"/>
    <form suffix="saba" tag="MII1S0"/>
    <form suffix="saba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="sabais" tag="MII2P0"/>
    <form suffix="saban" tag="MII3P0"/>
    <form suffix="sabas" tag="MII2S0"/>
    <form suffix="sad" tag="MM02P0" synt="Imperative"/>
    <form suffix="sada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="sadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="sado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="sados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="samos" tag="MIP1P0"/>
    <form suffix="samos" tag="MIS1P0"/>
    <form suffix="san" tag="MIP3P0"/>
    <form suffix="sando" tag="MG0000"/>
    <form suffix="sara" tag="MSI1S0"/>
    <form suffix="sara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="sarais" tag="MSI2P0"/>
    <form suffix="saran" tag="MSI3P0"/>
    <form suffix="saras" tag="MSI2S0"/>
    <form suffix="sare" tag="MSF1S0"/>
    <form suffix="sare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="sareis" tag="MSF2P0"/>
    <form suffix="saremos" tag="MIF1P0"/>
    <form suffix="saren" tag="MSF3P0"/>
    <form suffix="sares" tag="MSF2S0"/>
    <form suffix="saron" tag="MIS3P0"/>
    <form suffix="sará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="sarán" tag="MIF3P0"/>
    <form suffix="sarás" tag="MIF2S0"/>
    <form suffix="saré" tag="MIF1S0"/>
    <form suffix="saréis" tag="MIF2P0"/>
    <form suffix="saría" tag="MIC1S0"/>
    <form suffix="saría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="saríais" tag="MIC2P0"/>
    <form suffix="saríamos" tag="MIC1P0"/>
    <form suffix="sarían" tag="MIC3P0"/>
    <form suffix="sarías" tag="MIC2S0"/>
    <form suffix="sas" tag="MIP2S0"/>
    <form suffix="sase" tag="MSI1S0"/>
    <form suffix="sase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="saseis" tag="MSI2P0"/>
    <form suffix="sasen" tag="MSI3P0"/>
    <form suffix="sases" tag="MSI2S0"/>
    <form suffix="saste" tag="MIS2S0"/>
    <form suffix="sasteis" tag="MIS2P0"/>
    <form suffix="se" tag="MM03S0" synt="Imperative"/>
    <form suffix="se" tag="MSP1S0"/>
    <form suffix="se" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="semos" tag="MM01P0" synt="Imperative"/>
    <form suffix="semos" tag="MSP1P0"/>
    <form suffix="sen" tag="MM03P0" synt="Imperative"/>
    <form suffix="sen" tag="MSP3P0"/>
    <form suffix="ses" tag="MSP2S0"/>
    <form suffix="so" tag="MIP1S0"/>
    <form suffix="sábamos" tag="MII1P0"/>
    <form suffix="sáis" tag="MIP2P0"/>
    <form suffix="sáramos" tag="MSI1P0"/>
    <form suffix="sáremos" tag="MSF1P0"/>
    <form suffix="sásemos" tag="MSI1P0"/>
    <form suffix="sé" tag="MIS1S0"/>
    <form suffix="séis" tag="MSP2P0"/>
    <form suffix="só" tag="MIS3S0" synt="ThirdSing"/>
  </table>
  <table name="V49" rads=".*(desenco|asper|abster|desco|deter|desprote)" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: desenco/asper/abster/desco/deter/desprote + ger -->
    <!-- 6 members -->
    <form suffix="ger" tag="MN0000" synt="Infinitive"/>
    <form suffix="ge" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ge" tag="MM02S0" synt="Imperative"/>
    <form suffix="ged" tag="MM02P0" synt="Imperative"/>
    <form suffix="gemos" tag="MIP1P0"/>
    <form suffix="gen" tag="MIP3P0"/>
    <form suffix="geremos" tag="MIF1P0"/>
    <form suffix="gerá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="gerán" tag="MIF3P0"/>
    <form suffix="gerás" tag="MIF2S0"/>
    <form suffix="geré" tag="MIF1S0"/>
    <form suffix="geréis" tag="MIF2P0"/>
    <form suffix="gería" tag="MIC1S0"/>
    <form suffix="gería" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="geríais" tag="MIC2P0"/>
    <form suffix="geríamos" tag="MIC1P0"/>
    <form suffix="gerían" tag="MIC3P0"/>
    <form suffix="gerías" tag="MIC2S0"/>
    <form suffix="ges" tag="MIP2S0"/>
    <form suffix="gida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="gidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="gido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="gidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="giendo" tag="MG0000"/>
    <form suffix="giera" tag="MSI1S0"/>
    <form suffix="giera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="gierais" tag="MSI2P0"/>
    <form suffix="gieran" tag="MSI3P0"/>
    <form suffix="gieras" tag="MSI2S0"/>
    <form suffix="giere" tag="MSF1S0"/>
    <form suffix="giere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="giereis" tag="MSF2P0"/>
    <form suffix="gieren" tag="MSF3P0"/>
    <form suffix="gieres" tag="MSF2S0"/>
    <form suffix="gieron" tag="MIS3P0"/>
    <form suffix="giese" tag="MSI1S0"/>
    <form suffix="giese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="gieseis" tag="MSI2P0"/>
    <form suffix="giesen" tag="MSI3P0"/>
    <form suffix="gieses" tag="MSI2S0"/>
    <form suffix="gimos" tag="MIS1P0"/>
    <form suffix="giste" tag="MIS2S0"/>
    <form suffix="gisteis" tag="MIS2P0"/>
    <form suffix="giéramos" tag="MSI1P0"/>
    <form suffix="giéremos" tag="MSF1P0"/>
    <form suffix="giésemos" tag="MSI1P0"/>
    <form suffix="gió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="géis" tag="MIP2P0"/>
    <form suffix="gí" tag="MIS1S0"/>
    <form suffix="gía" tag="MII1S0"/>
    <form suffix="gía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="gíais" tag="MII2P0"/>
    <form suffix="gíamos" tag="MII1P0"/>
    <form suffix="gían" tag="MII3P0"/>
    <form suffix="gías" tag="MII2S0"/>
    <form suffix="ja" tag="MSP1S0"/>
    <form suffix="ja" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ja" tag="MM03S0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="jamos" tag="MSP1P0"/>
    <form suffix="jamos" tag="MM01P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="jan" tag="MSP3P0"/>
    <form suffix="jan" tag="MM03P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="jas" tag="MSP2S0"/>
    <form suffix="jo" tag="MIP1S0"/>
    <form suffix="jáis" tag="MSP2P0"/>
  </table>
  <table name="V50" rads=".*(mer|enm|arr|rem|recom|encom|desarr|hac|subarr)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: mer/enm/arr/rem/recom/encom + endar -->
    <!-- 6 members -->
    <form suffix="endar" tag="MN0000" synt="Infinitive"/>
    <form suffix="endaba" tag="MII1S0"/>
    <form suffix="endaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="endabais" tag="MII2P0"/>
    <form suffix="endaban" tag="MII3P0"/>
    <form suffix="endabas" tag="MII2S0"/>
    <form suffix="endad" tag="MM02P0" synt="Imperative"/>
    <form suffix="endada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="endadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="endado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="endados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="endamos" tag="MIP1P0"/>
    <form suffix="endamos" tag="MIS1P0"/>
    <form suffix="endando" tag="MG0000"/>
    <form suffix="endara" tag="MSI1S0"/>
    <form suffix="endara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="endarais" tag="MSI2P0"/>
    <form suffix="endaran" tag="MSI3P0"/>
    <form suffix="endaras" tag="MSI2S0"/>
    <form suffix="endare" tag="MSF1S0"/>
    <form suffix="endare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="endareis" tag="MSF2P0"/>
    <form suffix="endaremos" tag="MIF1P0"/>
    <form suffix="endaren" tag="MSF3P0"/>
    <form suffix="endares" tag="MSF2S0"/>
    <form suffix="endaron" tag="MIS3P0"/>
    <form suffix="endará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="endarán" tag="MIF3P0"/>
    <form suffix="endarás" tag="MIF2S0"/>
    <form suffix="endaré" tag="MIF1S0"/>
    <form suffix="endaréis" tag="MIF2P0"/>
    <form suffix="endaría" tag="MIC1S0"/>
    <form suffix="endaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="endaríais" tag="MIC2P0"/>
    <form suffix="endaríamos" tag="MIC1P0"/>
    <form suffix="endarían" tag="MIC3P0"/>
    <form suffix="endarías" tag="MIC2S0"/>
    <form suffix="endase" tag="MSI1S0"/>
    <form suffix="endase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="endaseis" tag="MSI2P0"/>
    <form suffix="endasen" tag="MSI3P0"/>
    <form suffix="endases" tag="MSI2S0"/>
    <form suffix="endaste" tag="MIS2S0"/>
    <form suffix="endasteis" tag="MIS2P0"/>
    <form suffix="endemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="endemos" tag="MSP1P0"/>
    <form suffix="endábamos" tag="MII1P0"/>
    <form suffix="endáis" tag="MIP2P0"/>
    <form suffix="endáramos" tag="MSI1P0"/>
    <form suffix="endáremos" tag="MSF1P0"/>
    <form suffix="endásemos" tag="MSI1P0"/>
    <form suffix="endé" tag="MIS1S0"/>
    <form suffix="endéis" tag="MSP2P0"/>
    <form suffix="endó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ienda" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ienda" tag="MM02S0" synt="Imperative"/>
    <form suffix="iendan" tag="MIP3P0"/>
    <form suffix="iendas" tag="MIP2S0"/>
    <form suffix="iende" tag="MM03S0" synt="Imperative"/>
    <form suffix="iende" tag="MSP1S0"/>
    <form suffix="iende" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ienden" tag="MM03P0" synt="Imperative"/>
    <form suffix="ienden" tag="MSP3P0"/>
    <form suffix="iendes" tag="MSP2S0"/>
    <form suffix="iendo" tag="MIP1S0"/>
  </table>
  <table name="V51" rads=".*(rele|despose|sobrese|pose|le|cre|pe)" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: rele/despose/sobrese/pose/le/cre + er -->
    <!-- 6 members -->
    <form suffix="er" tag="MN0000" synt="Infinitive"/>
    <form suffix="a" tag="MM03S0" synt="Imperative"/>
    <form suffix="a" tag="MSP1S0"/>
    <form suffix="a" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="amos" tag="MM01P0" synt="Imperative"/>
    <form suffix="amos" tag="MSP1P0"/>
    <form suffix="an" tag="MM03P0" synt="Imperative"/>
    <form suffix="an" tag="MSP3P0"/>
    <form suffix="as" tag="MSP2S0"/>
    <form suffix="e" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="e" tag="MM02S0" synt="Imperative"/>
    <form suffix="ed" tag="MM02P0" synt="Imperative"/>
    <form suffix="emos" tag="MIP1P0"/>
    <form suffix="en" tag="MIP3P0"/>
    <form suffix="eremos" tag="MIF1P0"/>
    <form suffix="erá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="erán" tag="MIF3P0"/>
    <form suffix="erás" tag="MIF2S0"/>
    <form suffix="eré" tag="MIF1S0"/>
    <form suffix="eréis" tag="MIF2P0"/>
    <form suffix="ería" tag="MIC1S0"/>
    <form suffix="ería" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="eríais" tag="MIC2P0"/>
    <form suffix="eríamos" tag="MIC1P0"/>
    <form suffix="erían" tag="MIC3P0"/>
    <form suffix="erías" tag="MIC2S0"/>
    <form suffix="es" tag="MIP2S0"/>
    <form suffix="o" tag="MIP1S0"/>
    <form suffix="yendo" tag="MG0000"/>
    <form suffix="yera" tag="MSI1S0"/>
    <form suffix="yera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="yerais" tag="MSI2P0"/>
    <form suffix="yeran" tag="MSI3P0"/>
    <form suffix="yeras" tag="MSI2S0"/>
    <form suffix="yere" tag="MSF1S0"/>
    <form suffix="yere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="yereis" tag="MSF2P0"/>
    <form suffix="yeren" tag="MSF3P0"/>
    <form suffix="yeres" tag="MSF2S0"/>
    <form suffix="yeron" tag="MIS3P0"/>
    <form suffix="yese" tag="MSI1S0"/>
    <form suffix="yese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="yeseis" tag="MSI2P0"/>
    <form suffix="yesen" tag="MSI3P0"/>
    <form suffix="yeses" tag="MSI2S0"/>
    <form suffix="yéramos" tag="MSI1P0"/>
    <form suffix="yéremos" tag="MSF1P0"/>
    <form suffix="yésemos" tag="MSI1P0"/>
    <form suffix="yó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="áis" tag="MSP2P0"/>
    <form suffix="éis" tag="MIP2P0"/>
    <form suffix="í" tag="MIS1S0"/>
    <form suffix="ía" tag="MII1S0"/>
    <form suffix="ía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="íais" tag="MII2P0"/>
    <form suffix="íamos" tag="MII1P0"/>
    <form suffix="ían" tag="MII3P0"/>
    <form suffix="ías" tag="MII2S0"/>
    <form suffix="ída" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ídas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ído" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ídos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ímos" tag="MIS1P0"/>
    <form suffix="íste" tag="MIS2S0"/>
    <form suffix="ísteis" tag="MIS2P0"/>
  </table>
  <table name="V52" rads=".*(sorr|res|estr|refr|desasos)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: sorr/res/estr/refr/desasos + egar -->
    <!-- 5 members -->
    <form suffix="egar" tag="MN0000" synt="Infinitive"/>
    <form suffix="egaba" tag="MII1S0"/>
    <form suffix="egaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="egabais" tag="MII2P0"/>
    <form suffix="egaban" tag="MII3P0"/>
    <form suffix="egabas" tag="MII2S0"/>
    <form suffix="egad" tag="MM02P0" synt="Imperative"/>
    <form suffix="egada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="egadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="egado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="egados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="egamos" tag="MIP1P0"/>
    <form suffix="egamos" tag="MIS1P0"/>
    <form suffix="egando" tag="MG0000"/>
    <form suffix="egara" tag="MSI1S0"/>
    <form suffix="egara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="egarais" tag="MSI2P0"/>
    <form suffix="egaran" tag="MSI3P0"/>
    <form suffix="egaras" tag="MSI2S0"/>
    <form suffix="egare" tag="MSF1S0"/>
    <form suffix="egare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="egareis" tag="MSF2P0"/>
    <form suffix="egaremos" tag="MIF1P0"/>
    <form suffix="egaren" tag="MSF3P0"/>
    <form suffix="egares" tag="MSF2S0"/>
    <form suffix="egaron" tag="MIS3P0"/>
    <form suffix="egará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="egarán" tag="MIF3P0"/>
    <form suffix="egarás" tag="MIF2S0"/>
    <form suffix="egaré" tag="MIF1S0"/>
    <form suffix="egaréis" tag="MIF2P0"/>
    <form suffix="egaría" tag="MIC1S0"/>
    <form suffix="egaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="egaríais" tag="MIC2P0"/>
    <form suffix="egaríamos" tag="MIC1P0"/>
    <form suffix="egarían" tag="MIC3P0"/>
    <form suffix="egarías" tag="MIC2S0"/>
    <form suffix="egase" tag="MSI1S0"/>
    <form suffix="egase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="egaseis" tag="MSI2P0"/>
    <form suffix="egasen" tag="MSI3P0"/>
    <form suffix="egases" tag="MSI2S0"/>
    <form suffix="egaste" tag="MIS2S0"/>
    <form suffix="egasteis" tag="MIS2P0"/>
    <form suffix="eguemos" tag="MSP1P0"/>
    <form suffix="eguemos" tag="MM01P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="egué" tag="MIS1S0"/>
    <form suffix="eguéis" tag="MSP2P0"/>
    <form suffix="egábamos" tag="MII1P0"/>
    <form suffix="egáis" tag="MIP2P0"/>
    <form suffix="egáramos" tag="MSI1P0"/>
    <form suffix="egáremos" tag="MSF1P0"/>
    <form suffix="egásemos" tag="MSI1P0"/>
    <form suffix="egó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="iega" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="iega" tag="MM02S0" synt="Imperative"/>
    <form suffix="iegan" tag="MIP3P0"/>
    <form suffix="iegas" tag="MIP2S0"/>
    <form suffix="iego" tag="MIP1S0"/>
    <form suffix="iegue" tag="MSP1S0"/>
    <form suffix="iegue" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="iegue" tag="MM03S0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ieguen" tag="MSP3P0"/>
    <form suffix="ieguen" tag="MM03P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="iegues" tag="MSP2S0"/>
  </table>
  <table name="V53" rads=".*(cons|sobrev|c|d|v)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: cons/sobrev/c/d/v + olar -->
    <!-- 5 members -->
    <form suffix="olar" tag="MN0000" synt="Infinitive"/>
    <form suffix="olaba" tag="MII1S0"/>
    <form suffix="olaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="olabais" tag="MII2P0"/>
    <form suffix="olaban" tag="MII3P0"/>
    <form suffix="olabas" tag="MII2S0"/>
    <form suffix="olad" tag="MM02P0" synt="Imperative"/>
    <form suffix="olada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="oladas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="olado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="olados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="olamos" tag="MIP1P0"/>
    <form suffix="olamos" tag="MIS1P0"/>
    <form suffix="olando" tag="MG0000"/>
    <form suffix="olara" tag="MSI1S0"/>
    <form suffix="olara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="olarais" tag="MSI2P0"/>
    <form suffix="olaran" tag="MSI3P0"/>
    <form suffix="olaras" tag="MSI2S0"/>
    <form suffix="olare" tag="MSF1S0"/>
    <form suffix="olare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="olareis" tag="MSF2P0"/>
    <form suffix="olaremos" tag="MIF1P0"/>
    <form suffix="olaren" tag="MSF3P0"/>
    <form suffix="olares" tag="MSF2S0"/>
    <form suffix="olaron" tag="MIS3P0"/>
    <form suffix="olará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="olarán" tag="MIF3P0"/>
    <form suffix="olarás" tag="MIF2S0"/>
    <form suffix="olaré" tag="MIF1S0"/>
    <form suffix="olaréis" tag="MIF2P0"/>
    <form suffix="olaría" tag="MIC1S0"/>
    <form suffix="olaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="olaríais" tag="MIC2P0"/>
    <form suffix="olaríamos" tag="MIC1P0"/>
    <form suffix="olarían" tag="MIC3P0"/>
    <form suffix="olarías" tag="MIC2S0"/>
    <form suffix="olase" tag="MSI1S0"/>
    <form suffix="olase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="olaseis" tag="MSI2P0"/>
    <form suffix="olasen" tag="MSI3P0"/>
    <form suffix="olases" tag="MSI2S0"/>
    <form suffix="olaste" tag="MIS2S0"/>
    <form suffix="olasteis" tag="MIS2P0"/>
    <form suffix="olemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="olemos" tag="MSP1P0"/>
    <form suffix="olábamos" tag="MII1P0"/>
    <form suffix="oláis" tag="MIP2P0"/>
    <form suffix="oláramos" tag="MSI1P0"/>
    <form suffix="oláremos" tag="MSF1P0"/>
    <form suffix="olásemos" tag="MSI1P0"/>
    <form suffix="olé" tag="MIS1S0"/>
    <form suffix="oléis" tag="MSP2P0"/>
    <form suffix="oló" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="uela" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uela" tag="MM02S0" synt="Imperative"/>
    <form suffix="uelan" tag="MIP3P0"/>
    <form suffix="uelas" tag="MIP2S0"/>
    <form suffix="uele" tag="MM03S0" synt="Imperative"/>
    <form suffix="uele" tag="MSP1S0"/>
    <form suffix="uele" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uelen" tag="MM03P0" synt="Imperative"/>
    <form suffix="uelen" tag="MSP3P0"/>
    <form suffix="ueles" tag="MSP2S0"/>
    <form suffix="uelo" tag="MIP1S0"/>
  </table>
  <table name="V54" rads=".*(emb|inv|v|rev|desv|trav)" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: emb/inv/v/rev/desv + estir -->
    <!-- 5 members -->
    <form suffix="estir" tag="MN0000" synt="Infinitive"/>
    <form suffix="estid" tag="MM02P0" synt="Imperative"/>
    <form suffix="estida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="estidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="estido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="estidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="estimos" tag="MIP1P0"/>
    <form suffix="estimos" tag="MIS1P0"/>
    <form suffix="estiremos" tag="MIF1P0"/>
    <form suffix="estirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="estirán" tag="MIF3P0"/>
    <form suffix="estirás" tag="MIF2S0"/>
    <form suffix="estiré" tag="MIF1S0"/>
    <form suffix="estiréis" tag="MIF2P0"/>
    <form suffix="estiría" tag="MIC1S0"/>
    <form suffix="estiría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="estiríais" tag="MIC2P0"/>
    <form suffix="estiríamos" tag="MIC1P0"/>
    <form suffix="estirían" tag="MIC3P0"/>
    <form suffix="estirías" tag="MIC2S0"/>
    <form suffix="estiste" tag="MIS2S0"/>
    <form suffix="estisteis" tag="MIS2P0"/>
    <form suffix="estí" tag="MIS1S0"/>
    <form suffix="estía" tag="MII1S0"/>
    <form suffix="estía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="estíais" tag="MII2P0"/>
    <form suffix="estíamos" tag="MII1P0"/>
    <form suffix="estían" tag="MII3P0"/>
    <form suffix="estías" tag="MII2S0"/>
    <form suffix="estís" tag="MIP2P0"/>
    <form suffix="ista" tag="MM03S0" synt="Imperative"/>
    <form suffix="ista" tag="MSP1S0"/>
    <form suffix="ista" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="istamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="istamos" tag="MSP1P0"/>
    <form suffix="istan" tag="MM03P0" synt="Imperative"/>
    <form suffix="istan" tag="MSP3P0"/>
    <form suffix="istas" tag="MSP2S0"/>
    <form suffix="iste" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="iste" tag="MM02S0" synt="Imperative"/>
    <form suffix="isten" tag="MIP3P0"/>
    <form suffix="istes" tag="MIP2S0"/>
    <form suffix="istiendo" tag="MG0000"/>
    <form suffix="istiera" tag="MSI1S0"/>
    <form suffix="istiera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="istierais" tag="MSI2P0"/>
    <form suffix="istieran" tag="MSI3P0"/>
    <form suffix="istieras" tag="MSI2S0"/>
    <form suffix="istiere" tag="MSF1S0"/>
    <form suffix="istiere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="istiereis" tag="MSF2P0"/>
    <form suffix="istieren" tag="MSF3P0"/>
    <form suffix="istieres" tag="MSF2S0"/>
    <form suffix="istieron" tag="MIS3P0"/>
    <form suffix="istiese" tag="MSI1S0"/>
    <form suffix="istiese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="istieseis" tag="MSI2P0"/>
    <form suffix="istiesen" tag="MSI3P0"/>
    <form suffix="istieses" tag="MSI2S0"/>
    <form suffix="istiéramos" tag="MSI1P0"/>
    <form suffix="istiéremos" tag="MSF1P0"/>
    <form suffix="istiésemos" tag="MSI1P0"/>
    <form suffix="istió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="isto" tag="MIP1S0"/>
    <form suffix="istáis" tag="MSP2P0"/>
  </table>
  <table name="V55" rads=".*(constr|t|dest|r|c)" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: const/t/dest/r/c + eñir -->
    <!-- 5 members -->
    <form suffix="eñir" tag="MN0000" synt="Infinitive"/>
    <form suffix="eñid" tag="MM02P0" synt="Imperative"/>
    <form suffix="eñida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="eñidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="eñido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="eñidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="eñimos" tag="MIP1P0"/>
    <form suffix="eñimos" tag="MIS1P0"/>
    <form suffix="eñiremos" tag="MIF1P0"/>
    <form suffix="eñirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="eñirán" tag="MIF3P0"/>
    <form suffix="eñirás" tag="MIF2S0"/>
    <form suffix="eñiré" tag="MIF1S0"/>
    <form suffix="eñiréis" tag="MIF2P0"/>
    <form suffix="eñiría" tag="MIC1S0"/>
    <form suffix="eñiría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="eñiríais" tag="MIC2P0"/>
    <form suffix="eñiríamos" tag="MIC1P0"/>
    <form suffix="eñirían" tag="MIC3P0"/>
    <form suffix="eñirías" tag="MIC2S0"/>
    <form suffix="eñiste" tag="MIS2S0"/>
    <form suffix="eñisteis" tag="MIS2P0"/>
    <form suffix="eñí" tag="MIS1S0"/>
    <form suffix="eñía" tag="MII1S0"/>
    <form suffix="eñía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="eñíais" tag="MII2P0"/>
    <form suffix="eñíamos" tag="MII1P0"/>
    <form suffix="eñían" tag="MII3P0"/>
    <form suffix="eñías" tag="MII2S0"/>
    <form suffix="eñís" tag="MIP2P0"/>
    <form suffix="iña" tag="MM03S0" synt="Imperative"/>
    <form suffix="iña" tag="MSP1S0"/>
    <form suffix="iña" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="iñamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="iñamos" tag="MSP1P0"/>
    <form suffix="iñan" tag="MM03P0" synt="Imperative"/>
    <form suffix="iñan" tag="MSP3P0"/>
    <form suffix="iñas" tag="MSP2S0"/>
    <form suffix="iñe" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="iñe" tag="MM02S0" synt="Imperative"/>
    <form suffix="iñen" tag="MIP3P0"/>
    <form suffix="iñendo" tag="MG0000"/>
    <form suffix="iñera" tag="MSI1S0"/>
    <form suffix="iñera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="iñerais" tag="MSI2P0"/>
    <form suffix="iñeran" tag="MSI3P0"/>
    <form suffix="iñeras" tag="MSI2S0"/>
    <form suffix="iñere" tag="MSF1S0"/>
    <form suffix="iñere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="iñereis" tag="MSF2P0"/>
    <form suffix="iñeren" tag="MSF3P0"/>
    <form suffix="iñeres" tag="MSF2S0"/>
    <form suffix="iñeron" tag="MIS3P0"/>
    <form suffix="iñes" tag="MIP2S0"/>
    <form suffix="iñese" tag="MSI1S0"/>
    <form suffix="iñese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="iñeseis" tag="MSI2P0"/>
    <form suffix="iñesen" tag="MSI3P0"/>
    <form suffix="iñeses" tag="MSI2S0"/>
    <form suffix="iño" tag="MIP1S0"/>
    <form suffix="iñáis" tag="MSP2P0"/>
    <form suffix="iñéramos" tag="MSI1P0"/>
    <form suffix="iñéremos" tag="MSF1P0"/>
    <form suffix="iñésemos" tag="MSI1P0"/>
    <form suffix="iñó" tag="MIS3S0" synt="ThirdSing"/>
  </table>
  <table name="V56" rads=".*(conc|enc|ac|desac|rec)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: conc/enc/ac/desac/rec + ordar -->
    <!-- 5 members -->
    <form suffix="ordar" tag="MN0000" synt="Infinitive"/>
    <form suffix="ordaba" tag="MII1S0"/>
    <form suffix="ordaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="ordabais" tag="MII2P0"/>
    <form suffix="ordaban" tag="MII3P0"/>
    <form suffix="ordabas" tag="MII2S0"/>
    <form suffix="ordad" tag="MM02P0" synt="Imperative"/>
    <form suffix="ordada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ordadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ordado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ordados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ordamos" tag="MIP1P0"/>
    <form suffix="ordamos" tag="MIS1P0"/>
    <form suffix="ordando" tag="MG0000"/>
    <form suffix="ordara" tag="MSI1S0"/>
    <form suffix="ordara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ordarais" tag="MSI2P0"/>
    <form suffix="ordaran" tag="MSI3P0"/>
    <form suffix="ordaras" tag="MSI2S0"/>
    <form suffix="ordare" tag="MSF1S0"/>
    <form suffix="ordare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ordareis" tag="MSF2P0"/>
    <form suffix="ordaremos" tag="MIF1P0"/>
    <form suffix="ordaren" tag="MSF3P0"/>
    <form suffix="ordares" tag="MSF2S0"/>
    <form suffix="ordaron" tag="MIS3P0"/>
    <form suffix="ordará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="ordarán" tag="MIF3P0"/>
    <form suffix="ordarás" tag="MIF2S0"/>
    <form suffix="ordaré" tag="MIF1S0"/>
    <form suffix="ordaréis" tag="MIF2P0"/>
    <form suffix="ordaría" tag="MIC1S0"/>
    <form suffix="ordaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ordaríais" tag="MIC2P0"/>
    <form suffix="ordaríamos" tag="MIC1P0"/>
    <form suffix="ordarían" tag="MIC3P0"/>
    <form suffix="ordarías" tag="MIC2S0"/>
    <form suffix="ordase" tag="MSI1S0"/>
    <form suffix="ordase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ordaseis" tag="MSI2P0"/>
    <form suffix="ordasen" tag="MSI3P0"/>
    <form suffix="ordases" tag="MSI2S0"/>
    <form suffix="ordaste" tag="MIS2S0"/>
    <form suffix="ordasteis" tag="MIS2P0"/>
    <form suffix="ordemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="ordemos" tag="MSP1P0"/>
    <form suffix="ordábamos" tag="MII1P0"/>
    <form suffix="ordáis" tag="MIP2P0"/>
    <form suffix="ordáramos" tag="MSI1P0"/>
    <form suffix="ordáremos" tag="MSF1P0"/>
    <form suffix="ordásemos" tag="MSI1P0"/>
    <form suffix="ordé" tag="MIS1S0"/>
    <form suffix="ordéis" tag="MSP2P0"/>
    <form suffix="ordó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="uerda" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uerda" tag="MM02S0" synt="Imperative"/>
    <form suffix="uerdan" tag="MIP3P0"/>
    <form suffix="uerdas" tag="MIP2S0"/>
    <form suffix="uerde" tag="MM03S0" synt="Imperative"/>
    <form suffix="uerde" tag="MSP1S0"/>
    <form suffix="uerde" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uerden" tag="MM03P0" synt="Imperative"/>
    <form suffix="uerden" tag="MSP3P0"/>
    <form suffix="uerdes" tag="MSP2S0"/>
    <form suffix="uerdo" tag="MIP1S0"/>
  </table>
  <!--table name="V57" rads=".*(encl|acl|trastr|cl|destr)" fast="-"--> 
  <table name="V57" rads=".*(encl|cl|trastr|tr)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: encl/cl + ocar -->
    <!-- 5 members -->
    <form suffix="ocar" tag="MN0000" synt="Infinitive"/>
    <form suffix="ocaba" tag="MII1S0"/>
    <form suffix="ocaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="ocabais" tag="MII2P0"/>
    <form suffix="ocaban" tag="MII3P0"/>
    <form suffix="ocabas" tag="MII2S0"/>
    <form suffix="ocad" tag="MM02P0" synt="Imperative"/>
    <form suffix="ocada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ocadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ocado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ocados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ocamos" tag="MIP1P0"/>
    <form suffix="ocamos" tag="MIS1P0"/>
    <form suffix="ocando" tag="MG0000"/>
    <form suffix="ocara" tag="MSI1S0"/>
    <form suffix="ocara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ocarais" tag="MSI2P0"/>
    <form suffix="ocaran" tag="MSI3P0"/>
    <form suffix="ocaras" tag="MSI2S0"/>
    <form suffix="ocare" tag="MSF1S0"/>
    <form suffix="ocare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ocareis" tag="MSF2P0"/>
    <form suffix="ocaremos" tag="MIF1P0"/>
    <form suffix="ocaren" tag="MSF3P0"/>
    <form suffix="ocares" tag="MSF2S0"/>
    <form suffix="ocaron" tag="MIS3P0"/>
    <form suffix="ocará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="ocarán" tag="MIF3P0"/>
    <form suffix="ocarás" tag="MIF2S0"/>
    <form suffix="ocaré" tag="MIF1S0"/>
    <form suffix="ocaréis" tag="MIF2P0"/>
    <form suffix="ocaría" tag="MIC1S0"/>
    <form suffix="ocaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ocaríais" tag="MIC2P0"/>
    <form suffix="ocaríamos" tag="MIC1P0"/>
    <form suffix="ocarían" tag="MIC3P0"/>
    <form suffix="ocarías" tag="MIC2S0"/>
    <form suffix="ocase" tag="MSI1S0"/>
    <form suffix="ocase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ocaseis" tag="MSI2P0"/>
    <form suffix="ocasen" tag="MSI3P0"/>
    <form suffix="ocases" tag="MSI2S0"/>
    <form suffix="ocaste" tag="MIS2S0"/>
    <form suffix="ocasteis" tag="MIS2P0"/>
    <form suffix="ocábamos" tag="MII1P0"/>
    <form suffix="ocáis" tag="MIP2P0"/>
    <form suffix="ocáramos" tag="MSI1P0"/>
    <form suffix="ocáremos" tag="MSF1P0"/>
    <form suffix="ocásemos" tag="MSI1P0"/>
    <form suffix="ocó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="oquemos" tag="MSP1P0"/>
    <form suffix="oquemos" tag="MM01P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="oqué" tag="MIS1S0"/>
    <form suffix="oquéis" tag="MSP2P0"/>
    <form suffix="ueca" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ueca" tag="MM02S0" synt="Imperative"/>
    <form suffix="uecan" tag="MIP3P0"/>
    <form suffix="uecas" tag="MIP2S0"/>
    <form suffix="ueco" tag="MIP1S0"/>
    <form suffix="ueque" tag="MSP1S0"/>
    <form suffix="ueque" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ueque" tag="MM03S0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="uequen" tag="MSP3P0"/>
    <form suffix="uequen" tag="MM03P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ueques" tag="MSP2S0"/>
  </table>
  <table name="V58" rads=".*(res|af|reh|h|ac|desc|des)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: res/af/reh/h/ac + ollar -->
    <!-- 5 members -->
    <form suffix="ollar" tag="MN0000" synt="Infinitive"/>
    <form suffix="ollaba" tag="MII1S0"/>
    <form suffix="ollaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="ollabais" tag="MII2P0"/>
    <form suffix="ollaban" tag="MII3P0"/>
    <form suffix="ollabas" tag="MII2S0"/>
    <form suffix="ollad" tag="MM02P0" synt="Imperative"/>
    <form suffix="ollada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="olladas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ollado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ollados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ollamos" tag="MIP1P0"/>
    <form suffix="ollamos" tag="MIS1P0"/>
    <form suffix="ollando" tag="MG0000"/>
    <form suffix="ollara" tag="MSI1S0"/>
    <form suffix="ollara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ollarais" tag="MSI2P0"/>
    <form suffix="ollaran" tag="MSI3P0"/>
    <form suffix="ollaras" tag="MSI2S0"/>
    <form suffix="ollare" tag="MSF1S0"/>
    <form suffix="ollare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ollareis" tag="MSF2P0"/>
    <form suffix="ollaremos" tag="MIF1P0"/>
    <form suffix="ollaren" tag="MSF3P0"/>
    <form suffix="ollares" tag="MSF2S0"/>
    <form suffix="ollaron" tag="MIS3P0"/>
    <form suffix="ollará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="ollarán" tag="MIF3P0"/>
    <form suffix="ollarás" tag="MIF2S0"/>
    <form suffix="ollaré" tag="MIF1S0"/>
    <form suffix="ollaréis" tag="MIF2P0"/>
    <form suffix="ollaría" tag="MIC1S0"/>
    <form suffix="ollaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ollaríais" tag="MIC2P0"/>
    <form suffix="ollaríamos" tag="MIC1P0"/>
    <form suffix="ollarían" tag="MIC3P0"/>
    <form suffix="ollarías" tag="MIC2S0"/>
    <form suffix="ollase" tag="MSI1S0"/>
    <form suffix="ollase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ollaseis" tag="MSI2P0"/>
    <form suffix="ollasen" tag="MSI3P0"/>
    <form suffix="ollases" tag="MSI2S0"/>
    <form suffix="ollaste" tag="MIS2S0"/>
    <form suffix="ollasteis" tag="MIS2P0"/>
    <form suffix="ollemos" tag="MSP1P0"/>
    <form suffix="ollemos" tag="MM01P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ollábamos" tag="MII1P0"/>
    <form suffix="olláis" tag="MIP2P0"/>
    <form suffix="olláramos" tag="MSI1P0"/>
    <form suffix="olláremos" tag="MSF1P0"/>
    <form suffix="ollásemos" tag="MSI1P0"/>
    <form suffix="ollé" tag="MIS1S0"/>
    <form suffix="olléis" tag="MSP2P0"/>
    <form suffix="olló" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="uella" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uella" tag="MM02S0" synt="Imperative"/>
    <form suffix="uellan" tag="MIP3P0"/>
    <form suffix="uellas" tag="MIP2S0"/>
    <form suffix="uelle" tag="MSP1S0"/>
    <form suffix="uelle" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uelle" tag="MM03S0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="uellen" tag="MSP3P0"/>
    <form suffix="uellen" tag="MM03P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="uelles" tag="MSP2S0"/>
    <form suffix="uello" tag="MIP1S0"/>
  </table>
  <table name="V59" rads=".*(repr|pr|desapr|apr|compr|impr)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: repr/pr/desapr/apr/compr + obar -->
    <!-- 5 members -->
    <form suffix="obar" tag="MN0000" synt="Infinitive"/>
    <form suffix="obaba" tag="MII1S0"/>
    <form suffix="obaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="obabais" tag="MII2P0"/>
    <form suffix="obaban" tag="MII3P0"/>
    <form suffix="obabas" tag="MII2S0"/>
    <form suffix="obad" tag="MM02P0" synt="Imperative"/>
    <form suffix="obada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="obadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="obado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="obados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="obamos" tag="MIP1P0"/>
    <form suffix="obamos" tag="MIS1P0"/>
    <form suffix="obando" tag="MG0000"/>
    <form suffix="obara" tag="MSI1S0"/>
    <form suffix="obara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="obarais" tag="MSI2P0"/>
    <form suffix="obaran" tag="MSI3P0"/>
    <form suffix="obaras" tag="MSI2S0"/>
    <form suffix="obare" tag="MSF1S0"/>
    <form suffix="obare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="obareis" tag="MSF2P0"/>
    <form suffix="obaremos" tag="MIF1P0"/>
    <form suffix="obaren" tag="MSF3P0"/>
    <form suffix="obares" tag="MSF2S0"/>
    <form suffix="obaron" tag="MIS3P0"/>
    <form suffix="obará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="obarán" tag="MIF3P0"/>
    <form suffix="obarás" tag="MIF2S0"/>
    <form suffix="obaré" tag="MIF1S0"/>
    <form suffix="obaréis" tag="MIF2P0"/>
    <form suffix="obaría" tag="MIC1S0"/>
    <form suffix="obaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="obaríais" tag="MIC2P0"/>
    <form suffix="obaríamos" tag="MIC1P0"/>
    <form suffix="obarían" tag="MIC3P0"/>
    <form suffix="obarías" tag="MIC2S0"/>
    <form suffix="obase" tag="MSI1S0"/>
    <form suffix="obase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="obaseis" tag="MSI2P0"/>
    <form suffix="obasen" tag="MSI3P0"/>
    <form suffix="obases" tag="MSI2S0"/>
    <form suffix="obaste" tag="MIS2S0"/>
    <form suffix="obasteis" tag="MIS2P0"/>
    <form suffix="obemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="obemos" tag="MSP1P0"/>
    <form suffix="obábamos" tag="MII1P0"/>
    <form suffix="obáis" tag="MIP2P0"/>
    <form suffix="obáramos" tag="MSI1P0"/>
    <form suffix="obáremos" tag="MSF1P0"/>
    <form suffix="obásemos" tag="MSI1P0"/>
    <form suffix="obé" tag="MIS1S0"/>
    <form suffix="obéis" tag="MSP2P0"/>
    <form suffix="obó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ueba" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ueba" tag="MM02S0" synt="Imperative"/>
    <form suffix="ueban" tag="MIP3P0"/>
    <form suffix="uebas" tag="MIP2S0"/>
    <form suffix="uebe" tag="MM03S0" synt="Imperative"/>
    <form suffix="uebe" tag="MSP1S0"/>
    <form suffix="uebe" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ueben" tag="MM03P0" synt="Imperative"/>
    <form suffix="ueben" tag="MSP3P0"/>
    <form suffix="uebes" tag="MSP2S0"/>
    <form suffix="uebo" tag="MIP1S0"/>
  </table>
  <table name="V60" rads=".*(coer|me|ven|ejer|conven|reme)" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: coer/me/ven/ejer/conven + cer -->
    <form suffix="cer" tag="MN0000" synt="Infinitive"/>
    <form suffix="ce" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ce" tag="MM02S0" synt="Imperative"/>
    <form suffix="ced" tag="MM02P0" synt="Imperative"/>
    <form suffix="cemos" tag="MIP1P0"/>
    <form suffix="cen" tag="MIP3P0"/>
    <form suffix="ceremos" tag="MIF1P0"/>
    <form suffix="cerá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="cerán" tag="MIF3P0"/>
    <form suffix="cerás" tag="MIF2S0"/>
    <form suffix="ceré" tag="MIF1S0"/>
    <form suffix="ceréis" tag="MIF2P0"/>
    <form suffix="cería" tag="MIC1S0"/>
    <form suffix="cería" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ceríais" tag="MIC2P0"/>
    <form suffix="ceríamos" tag="MIC1P0"/>
    <form suffix="cerían" tag="MIC3P0"/>
    <form suffix="cerías" tag="MIC2S0"/>
    <form suffix="ces" tag="MIP2S0"/>
    <form suffix="cida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="cidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="cido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="cidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ciendo" tag="MG0000"/>
    <form suffix="ciera" tag="MSI1S0"/>
    <form suffix="ciera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="cierais" tag="MSI2P0"/>
    <form suffix="cieran" tag="MSI3P0"/>
    <form suffix="cieras" tag="MSI2S0"/>
    <form suffix="ciere" tag="MSF1S0"/>
    <form suffix="ciere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ciereis" tag="MSF2P0"/>
    <form suffix="cieren" tag="MSF3P0"/>
    <form suffix="cieres" tag="MSF2S0"/>
    <form suffix="cieron" tag="MIS3P0"/>
    <form suffix="ciese" tag="MSI1S0"/>
    <form suffix="ciese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="cieseis" tag="MSI2P0"/>
    <form suffix="ciesen" tag="MSI3P0"/>
    <form suffix="cieses" tag="MSI2S0"/>
    <form suffix="cimos" tag="MIS1P0"/>
    <form suffix="ciste" tag="MIS2S0"/>
    <form suffix="cisteis" tag="MIS2P0"/>
    <form suffix="ciéramos" tag="MSI1P0"/>
    <form suffix="ciéremos" tag="MSF1P0"/>
    <form suffix="ciésemos" tag="MSI1P0"/>
    <form suffix="ció" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="céis" tag="MIP2P0"/>
    <form suffix="cí" tag="MIS1S0"/>
    <form suffix="cía" tag="MII1S0"/>
    <form suffix="cía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="cíais" tag="MII2P0"/>
    <form suffix="cíamos" tag="MII1P0"/>
    <form suffix="cían" tag="MII3P0"/>
    <form suffix="cías" tag="MII2S0"/>
    <form suffix="za" tag="MM03S0" synt="Imperative"/>
    <form suffix="za" tag="MSP1S0"/>
    <form suffix="za" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="zamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="zamos" tag="MSP1P0"/>
    <form suffix="zan" tag="MM03P0" synt="Imperative"/>
    <form suffix="zan" tag="MSP3P0"/>
    <form suffix="zas" tag="MSP2S0"/>
    <form suffix="zo" tag="MIP1S0"/>
    <form suffix="záis" tag="MSP2P0"/>
  </table>
  <table name="V61" rads=".*(deslu|lu|translu|traslu|relu|balbu)" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: deslu/lu/translu/traslu/relu + cir -->
    <form suffix="cir" tag="MN0000" synt="Infinitive"/>
    <form suffix="ce" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ce" tag="MM02S0" synt="Imperative"/>
    <form suffix="cen" tag="MIP3P0"/>
    <form suffix="ces" tag="MIP2S0"/>
    <form suffix="cid" tag="MM02P0" synt="Imperative"/>
    <form suffix="cida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="cidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="cido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="cidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ciendo" tag="MG0000"/>
    <form suffix="ciera" tag="MSI1S0"/>
    <form suffix="ciera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="cierais" tag="MSI2P0"/>
    <form suffix="cieran" tag="MSI3P0"/>
    <form suffix="cieras" tag="MSI2S0"/>
    <form suffix="ciere" tag="MSF1S0"/>
    <form suffix="ciere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ciereis" tag="MSF2P0"/>
    <form suffix="cieren" tag="MSF3P0"/>
    <form suffix="cieres" tag="MSF2S0"/>
    <form suffix="cieron" tag="MIS3P0"/>
    <form suffix="ciese" tag="MSI1S0"/>
    <form suffix="ciese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="cieseis" tag="MSI2P0"/>
    <form suffix="ciesen" tag="MSI3P0"/>
    <form suffix="cieses" tag="MSI2S0"/>
    <form suffix="cimos" tag="MIP1P0"/>
    <form suffix="cimos" tag="MIS1P0"/>
    <form suffix="ciremos" tag="MIF1P0"/>
    <form suffix="cirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="cirán" tag="MIF3P0"/>
    <form suffix="cirás" tag="MIF2S0"/>
    <form suffix="ciré" tag="MIF1S0"/>
    <form suffix="ciréis" tag="MIF2P0"/>
    <form suffix="ciría" tag="MIC1S0"/>
    <form suffix="ciría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ciríais" tag="MIC2P0"/>
    <form suffix="ciríamos" tag="MIC1P0"/>
    <form suffix="cirían" tag="MIC3P0"/>
    <form suffix="cirías" tag="MIC2S0"/>
    <form suffix="ciste" tag="MIS2S0"/>
    <form suffix="cisteis" tag="MIS2P0"/>
    <form suffix="ciéramos" tag="MSI1P0"/>
    <form suffix="ciéremos" tag="MSF1P0"/>
    <form suffix="ciésemos" tag="MSI1P0"/>
    <form suffix="ció" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="cí" tag="MIS1S0"/>
    <form suffix="cía" tag="MII1S0"/>
    <form suffix="cía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="cíais" tag="MII2P0"/>
    <form suffix="cíamos" tag="MII1P0"/>
    <form suffix="cían" tag="MII3P0"/>
    <form suffix="cías" tag="MII2S0"/>
    <form suffix="cís" tag="MIP2P0"/>
    <form suffix="zca" tag="MM03S0" synt="Imperative"/>
    <form suffix="zca" tag="MSP1S0"/>
    <form suffix="zca" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="zcamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="zcamos" tag="MSP1P0"/>
    <form suffix="zcan" tag="MM03P0" synt="Imperative"/>
    <form suffix="zcan" tag="MSP3P0"/>
    <form suffix="zcas" tag="MSP2S0"/>
    <form suffix="zco" tag="MIP1S0"/>
    <form suffix="zcáis" tag="MSP2P0"/>
  </table>
  <table name="V62" rads=".*(descub|cub|ab|encub|recub|entreab|reab|redescub)" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: descub/cub/ab/encub/recub + rir -->
    <!-- 5 members -->
    <form suffix="rir" tag="MN0000" synt="Infinitive"/>
    <form suffix="ierta" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="iertas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ierto" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="iertos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ra" tag="MM03S0" synt="Imperative"/>
    <form suffix="ra" tag="MSP1S0"/>
    <form suffix="ra" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ramos" tag="MM01P0" synt="Imperative"/>
    <form suffix="ramos" tag="MSP1P0"/>
    <form suffix="ran" tag="MM03P0" synt="Imperative"/>
    <form suffix="ran" tag="MSP3P0"/>
    <form suffix="ras" tag="MSP2S0"/>
    <form suffix="re" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="re" tag="MM02S0" synt="Imperative"/>
    <form suffix="ren" tag="MIP3P0"/>
    <form suffix="res" tag="MIP2S0"/>
    <form suffix="rid" tag="MM02P0" synt="Imperative"/>
    <form suffix="riendo" tag="MG0000"/>
    <form suffix="riera" tag="MSI1S0"/>
    <form suffix="riera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="rierais" tag="MSI2P0"/>
    <form suffix="rieran" tag="MSI3P0"/>
    <form suffix="rieras" tag="MSI2S0"/>
    <form suffix="riere" tag="MSF1S0"/>
    <form suffix="riere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="riereis" tag="MSF2P0"/>
    <form suffix="rieren" tag="MSF3P0"/>
    <form suffix="rieres" tag="MSF2S0"/>
    <form suffix="rieron" tag="MIS3P0"/>
    <form suffix="riese" tag="MSI1S0"/>
    <form suffix="riese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="rieseis" tag="MSI2P0"/>
    <form suffix="riesen" tag="MSI3P0"/>
    <form suffix="rieses" tag="MSI2S0"/>
    <form suffix="rimos" tag="MIP1P0"/>
    <form suffix="rimos" tag="MIS1P0"/>
    <form suffix="riremos" tag="MIF1P0"/>
    <form suffix="rirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="rirán" tag="MIF3P0"/>
    <form suffix="rirás" tag="MIF2S0"/>
    <form suffix="riré" tag="MIF1S0"/>
    <form suffix="riréis" tag="MIF2P0"/>
    <form suffix="riría" tag="MIC1S0"/>
    <form suffix="riría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="riríais" tag="MIC2P0"/>
    <form suffix="riríamos" tag="MIC1P0"/>
    <form suffix="rirían" tag="MIC3P0"/>
    <form suffix="rirías" tag="MIC2S0"/>
    <form suffix="riste" tag="MIS2S0"/>
    <form suffix="risteis" tag="MIS2P0"/>
    <form suffix="riéramos" tag="MSI1P0"/>
    <form suffix="riéremos" tag="MSF1P0"/>
    <form suffix="riésemos" tag="MSI1P0"/>
    <form suffix="rió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ro" tag="MIP1S0"/>
    <form suffix="ráis" tag="MSP2P0"/>
    <form suffix="rí" tag="MIS1S0"/>
    <form suffix="ría" tag="MII1S0"/>
    <form suffix="ría" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="ríais" tag="MII2P0"/>
    <form suffix="ríamos" tag="MII1P0"/>
    <form suffix="rían" tag="MII3P0"/>
    <form suffix="rías" tag="MII2S0"/>
    <form suffix="rís" tag="MIP2P0"/>
  </table>
  <table name="V63" rads=".*(cons|s|pros|pers|res|subs)" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: cons/s/pors/pers/res + eguir -->
    <!-- 5 members -->
    <form suffix="eguir" tag="MN0000" synt="Infinitive"/>
    <form suffix="eguid" tag="MM02P0" synt="Imperative"/>
    <form suffix="eguida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="eguidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="eguido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="eguidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="eguimos" tag="MIP1P0"/>
    <form suffix="eguimos" tag="MIS1P0"/>
    <form suffix="eguiremos" tag="MIF1P0"/>
    <form suffix="eguirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="eguirán" tag="MIF3P0"/>
    <form suffix="eguirás" tag="MIF2S0"/>
    <form suffix="eguiré" tag="MIF1S0"/>
    <form suffix="eguiréis" tag="MIF2P0"/>
    <form suffix="eguiría" tag="MIC1S0"/>
    <form suffix="eguiría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="eguiríais" tag="MIC2P0"/>
    <form suffix="eguiríamos" tag="MIC1P0"/>
    <form suffix="eguirían" tag="MIC3P0"/>
    <form suffix="eguirías" tag="MIC2S0"/>
    <form suffix="eguiste" tag="MIS2S0"/>
    <form suffix="eguisteis" tag="MIS2P0"/>
    <form suffix="eguí" tag="MIS1S0"/>
    <form suffix="eguía" tag="MII1S0"/>
    <form suffix="eguía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="eguíais" tag="MII2P0"/>
    <form suffix="eguíamos" tag="MII1P0"/>
    <form suffix="eguían" tag="MII3P0"/>
    <form suffix="eguías" tag="MII2S0"/>
    <form suffix="eguís" tag="MIP2P0"/>
    <form suffix="iga" tag="MM03S0" synt="Imperative"/>
    <form suffix="iga" tag="MSP1S0"/>
    <form suffix="iga" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="igamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="igamos" tag="MSP1P0"/>
    <form suffix="igan" tag="MM03P0" synt="Imperative"/>
    <form suffix="igan" tag="MSP3P0"/>
    <form suffix="igas" tag="MSP2S0"/>
    <form suffix="igo" tag="MIP1S0"/>
    <form suffix="igue" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="igue" tag="MM02S0" synt="Imperative"/>
    <form suffix="iguen" tag="MIP3P0"/>
    <form suffix="igues" tag="MIP2S0"/>
    <form suffix="iguiendo" tag="MG0000"/>
    <form suffix="iguiera" tag="MSI1S0"/>
    <form suffix="iguiera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="iguierais" tag="MSI2P0"/>
    <form suffix="iguieran" tag="MSI3P0"/>
    <form suffix="iguieras" tag="MSI2S0"/>
    <form suffix="iguiere" tag="MSF1S0"/>
    <form suffix="iguiere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="iguiereis" tag="MSF2P0"/>
    <form suffix="iguieren" tag="MSF3P0"/>
    <form suffix="iguieres" tag="MSF2S0"/>
    <form suffix="iguieron" tag="MIS3P0"/>
    <form suffix="iguiese" tag="MSI1S0"/>
    <form suffix="iguiese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="iguieseis" tag="MSI2P0"/>
    <form suffix="iguiesen" tag="MSI3P0"/>
    <form suffix="iguieses" tag="MSI2S0"/>
    <form suffix="iguiéramos" tag="MSI1P0"/>
    <form suffix="iguiéremos" tag="MSF1P0"/>
    <form suffix="iguiésemos" tag="MSI1P0"/>
    <form suffix="iguió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="igáis" tag="MSP2P0"/>
  </table>
  <table name="V64" rads=".*(desp|entrep|cogob|desgob|ap|gob)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: desp/entrep/cogob/desgob/ap + ernar -->
    <!-- 5 members -->
    <form suffix="ernar" tag="MN0000" synt="Infinitive"/>
    <form suffix="ernaba" tag="MII1S0"/>
    <form suffix="ernaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="ernabais" tag="MII2P0"/>
    <form suffix="ernaban" tag="MII3P0"/>
    <form suffix="ernabas" tag="MII2S0"/>
    <form suffix="ernad" tag="MM02P0" synt="Imperative"/>
    <form suffix="ernada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ernadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ernado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ernados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ernamos" tag="MIP1P0"/>
    <form suffix="ernamos" tag="MIS1P0"/>
    <form suffix="ernando" tag="MG0000"/>
    <form suffix="ernara" tag="MSI1S0"/>
    <form suffix="ernara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ernarais" tag="MSI2P0"/>
    <form suffix="ernaran" tag="MSI3P0"/>
    <form suffix="ernaras" tag="MSI2S0"/>
    <form suffix="ernare" tag="MSF1S0"/>
    <form suffix="ernare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ernareis" tag="MSF2P0"/>
    <form suffix="ernaremos" tag="MIF1P0"/>
    <form suffix="ernaren" tag="MSF3P0"/>
    <form suffix="ernares" tag="MSF2S0"/>
    <form suffix="ernaron" tag="MIS3P0"/>
    <form suffix="ernará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="ernarán" tag="MIF3P0"/>
    <form suffix="ernarás" tag="MIF2S0"/>
    <form suffix="ernaré" tag="MIF1S0"/>
    <form suffix="ernaréis" tag="MIF2P0"/>
    <form suffix="ernaría" tag="MIC1S0"/>
    <form suffix="ernaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ernaríais" tag="MIC2P0"/>
    <form suffix="ernaríamos" tag="MIC1P0"/>
    <form suffix="ernarían" tag="MIC3P0"/>
    <form suffix="ernarías" tag="MIC2S0"/>
    <form suffix="ernase" tag="MSI1S0"/>
    <form suffix="ernase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ernaseis" tag="MSI2P0"/>
    <form suffix="ernasen" tag="MSI3P0"/>
    <form suffix="ernases" tag="MSI2S0"/>
    <form suffix="ernaste" tag="MIS2S0"/>
    <form suffix="ernasteis" tag="MIS2P0"/>
    <form suffix="ernemos" tag="MSP1P0"/>
    <form suffix="ernemos" tag="MM01P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ernábamos" tag="MII1P0"/>
    <form suffix="ernáis" tag="MIP2P0"/>
    <form suffix="ernáramos" tag="MSI1P0"/>
    <form suffix="ernáremos" tag="MSF1P0"/>
    <form suffix="ernásemos" tag="MSI1P0"/>
    <form suffix="erné" tag="MIS1S0"/>
    <form suffix="ernéis" tag="MSP2P0"/>
    <form suffix="ernó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ierna" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ierna" tag="MM02S0" synt="Imperative"/>
    <form suffix="iernan" tag="MIP3P0"/>
    <form suffix="iernas" tag="MIP2S0"/>
    <form suffix="ierne" tag="MSP1S0"/>
    <form suffix="ierne" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ierne" tag="MM03S0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="iernen" tag="MSP3P0"/>
    <form suffix="iernen" tag="MM03P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="iernes" tag="MSP2S0"/>
    <form suffix="ierno" tag="MIP1S0"/>
  </table>
  <table name="V65" rads=".*(acom|desm|descom|reexp|rem)" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: acom/desm/descom/reexp/rem + edir -->
    <!-- 5 members -->
    <form suffix="edir" tag="MN0000" synt="Infinitive"/>
    <form suffix="edid" tag="MM02P0" synt="Imperative"/>
    <form suffix="edida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="edidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="edido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="edidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="edimos" tag="MIP1P0"/>
    <form suffix="edimos" tag="MIS1P0"/>
    <form suffix="ediremos" tag="MIF1P0"/>
    <form suffix="edirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="edirán" tag="MIF3P0"/>
    <form suffix="edirás" tag="MIF2S0"/>
    <form suffix="ediré" tag="MIF1S0"/>
    <form suffix="ediréis" tag="MIF2P0"/>
    <form suffix="ediría" tag="MIC1S0"/>
    <form suffix="ediría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ediríais" tag="MIC2P0"/>
    <form suffix="ediríamos" tag="MIC1P0"/>
    <form suffix="edirían" tag="MIC3P0"/>
    <form suffix="edirías" tag="MIC2S0"/>
    <form suffix="ediste" tag="MIS2S0"/>
    <form suffix="edisteis" tag="MIS2P0"/>
    <form suffix="edí" tag="MIS1S0"/>
    <form suffix="edía" tag="MII1S0"/>
    <form suffix="edía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="edíais" tag="MII2P0"/>
    <form suffix="edíamos" tag="MII1P0"/>
    <form suffix="edían" tag="MII3P0"/>
    <form suffix="edías" tag="MII2S0"/>
    <form suffix="edís" tag="MIP2P0"/>
    <form suffix="ida" tag="MSP1S0"/>
    <form suffix="ida" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ida" tag="MM03S0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="idamos" tag="MSP1P0"/>
    <form suffix="idamos" tag="MM01P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="idan" tag="MSP3P0"/>
    <form suffix="idan" tag="MM03P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="idas" tag="MSP2S0"/>
    <form suffix="ide" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ide" tag="MM02S0" synt="Imperative"/>
    <form suffix="iden" tag="MIP3P0"/>
    <form suffix="ides" tag="MIP2S0"/>
    <form suffix="idiendo" tag="MG0000"/>
    <form suffix="idiera" tag="MSI1S0"/>
    <form suffix="idiera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="idierais" tag="MSI2P0"/>
    <form suffix="idieran" tag="MSI3P0"/>
    <form suffix="idieras" tag="MSI2S0"/>
    <form suffix="idiere" tag="MSF1S0"/>
    <form suffix="idiere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="idiereis" tag="MSF2P0"/>
    <form suffix="idieren" tag="MSF3P0"/>
    <form suffix="idieres" tag="MSF2S0"/>
    <form suffix="idieron" tag="MIS3P0"/>
    <form suffix="idiese" tag="MSI1S0"/>
    <form suffix="idiese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="idieseis" tag="MSI2P0"/>
    <form suffix="idiesen" tag="MSI3P0"/>
    <form suffix="idieses" tag="MSI2S0"/>
    <form suffix="idiéramos" tag="MSI1P0"/>
    <form suffix="idiéremos" tag="MSF1P0"/>
    <form suffix="idiésemos" tag="MSI1P0"/>
    <form suffix="idió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ido" tag="MIP1S0"/>
    <form suffix="idáis" tag="MSP2P0"/>
  </table>
  <table name="V66" rads=".*(ac|den|rec|c|t|ap|ret)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: ac/den/rec/c/t + ostar -->
    <!-- 5 members -->
    <form suffix="ostar" tag="MN0000" synt="Infinitive"/>
    <form suffix="ostaba" tag="MII1S0"/>
    <form suffix="ostaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="ostabais" tag="MII2P0"/>
    <form suffix="ostaban" tag="MII3P0"/>
    <form suffix="ostabas" tag="MII2S0"/>
    <form suffix="ostad" tag="MM02P0" synt="Imperative"/>
    <form suffix="ostada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ostadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ostado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ostados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ostamos" tag="MIP1P0"/>
    <form suffix="ostamos" tag="MIS1P0"/>
    <form suffix="ostando" tag="MG0000"/>
    <form suffix="ostara" tag="MSI1S0"/>
    <form suffix="ostara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ostarais" tag="MSI2P0"/>
    <form suffix="ostaran" tag="MSI3P0"/>
    <form suffix="ostaras" tag="MSI2S0"/>
    <form suffix="ostare" tag="MSF1S0"/>
    <form suffix="ostare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ostareis" tag="MSF2P0"/>
    <form suffix="ostaremos" tag="MIF1P0"/>
    <form suffix="ostaren" tag="MSF3P0"/>
    <form suffix="ostares" tag="MSF2S0"/>
    <form suffix="ostaron" tag="MIS3P0"/>
    <form suffix="ostará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="ostarán" tag="MIF3P0"/>
    <form suffix="ostarás" tag="MIF2S0"/>
    <form suffix="ostaré" tag="MIF1S0"/>
    <form suffix="ostaréis" tag="MIF2P0"/>
    <form suffix="ostaría" tag="MIC1S0"/>
    <form suffix="ostaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ostaríais" tag="MIC2P0"/>
    <form suffix="ostaríamos" tag="MIC1P0"/>
    <form suffix="ostarían" tag="MIC3P0"/>
    <form suffix="ostarías" tag="MIC2S0"/>
    <form suffix="ostase" tag="MSI1S0"/>
    <form suffix="ostase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ostaseis" tag="MSI2P0"/>
    <form suffix="ostasen" tag="MSI3P0"/>
    <form suffix="ostases" tag="MSI2S0"/>
    <form suffix="ostaste" tag="MIS2S0"/>
    <form suffix="ostasteis" tag="MIS2P0"/>
    <form suffix="ostemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="ostemos" tag="MSP1P0"/>
    <form suffix="ostábamos" tag="MII1P0"/>
    <form suffix="ostáis" tag="MIP2P0"/>
    <form suffix="ostáramos" tag="MSI1P0"/>
    <form suffix="ostáremos" tag="MSF1P0"/>
    <form suffix="ostásemos" tag="MSI1P0"/>
    <form suffix="osté" tag="MIS1S0"/>
    <form suffix="ostéis" tag="MSP2P0"/>
    <form suffix="ostó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="uesta" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uesta" tag="MM02S0" synt="Imperative"/>
    <form suffix="uestan" tag="MIP3P0"/>
    <form suffix="uestas" tag="MIP2S0"/>
    <form suffix="ueste" tag="MM03S0" synt="Imperative"/>
    <form suffix="ueste" tag="MSP1S0"/>
    <form suffix="ueste" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uesten" tag="MM03P0" synt="Imperative"/>
    <form suffix="uesten" tag="MSP3P0"/>
    <form suffix="uestes" tag="MSP2S0"/>
    <form suffix="uesto" tag="MIP1S0"/>
  </table>
  <table name="V67" rads=".*(cont|malent|sobrent|transc|condesc)" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: cont/malent/sobrent/transc/condesc + ender -->
    <!-- 5 members -->
    <form suffix="ender" tag="MN0000" synt="Infinitive"/>
    <form suffix="endamos" tag="MSP1P0"/>
    <form suffix="endamos" tag="MM01P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ended" tag="MM02P0" synt="Imperative"/>
    <form suffix="endemos" tag="MIP1P0"/>
    <form suffix="enderemos" tag="MIF1P0"/>
    <form suffix="enderá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="enderán" tag="MIF3P0"/>
    <form suffix="enderás" tag="MIF2S0"/>
    <form suffix="enderé" tag="MIF1S0"/>
    <form suffix="enderéis" tag="MIF2P0"/>
    <form suffix="endería" tag="MIC1S0"/>
    <form suffix="endería" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="enderíais" tag="MIC2P0"/>
    <form suffix="enderíamos" tag="MIC1P0"/>
    <form suffix="enderían" tag="MIC3P0"/>
    <form suffix="enderías" tag="MIC2S0"/>
    <form suffix="endida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="endidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="endido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="endidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="endiendo" tag="MG0000"/>
    <form suffix="endiera" tag="MSI1S0"/>
    <form suffix="endiera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="endierais" tag="MSI2P0"/>
    <form suffix="endieran" tag="MSI3P0"/>
    <form suffix="endieras" tag="MSI2S0"/>
    <form suffix="endiere" tag="MSF1S0"/>
    <form suffix="endiere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="endiereis" tag="MSF2P0"/>
    <form suffix="endieren" tag="MSF3P0"/>
    <form suffix="endieres" tag="MSF2S0"/>
    <form suffix="endieron" tag="MIS3P0"/>
    <form suffix="endiese" tag="MSI1S0"/>
    <form suffix="endiese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="endieseis" tag="MSI2P0"/>
    <form suffix="endiesen" tag="MSI3P0"/>
    <form suffix="endieses" tag="MSI2S0"/>
    <form suffix="endimos" tag="MIS1P0"/>
    <form suffix="endiste" tag="MIS2S0"/>
    <form suffix="endisteis" tag="MIS2P0"/>
    <form suffix="endiéramos" tag="MSI1P0"/>
    <form suffix="endiéremos" tag="MSF1P0"/>
    <form suffix="endiésemos" tag="MSI1P0"/>
    <form suffix="endió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="endáis" tag="MSP2P0"/>
    <form suffix="endéis" tag="MIP2P0"/>
    <form suffix="endí" tag="MIS1S0"/>
    <form suffix="endía" tag="MII1S0"/>
    <form suffix="endía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="endíais" tag="MII2P0"/>
    <form suffix="endíamos" tag="MII1P0"/>
    <form suffix="endían" tag="MII3P0"/>
    <form suffix="endías" tag="MII2S0"/>
    <form suffix="ienda" tag="MSP1S0"/>
    <form suffix="ienda" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ienda" tag="MM03S0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="iendan" tag="MSP3P0"/>
    <form suffix="iendan" tag="MM03P0" synt="Imperative"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="iendas" tag="MSP2S0"/>
    <form suffix="iende" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="iende" tag="MM02S0" synt="Imperative"/>
    <form suffix="ienden" tag="MIP3P0"/>
    <form suffix="iendes" tag="MIP2S0"/>
    <form suffix="iendo" tag="MIP1S0"/>
  </table>
  <table name="V68" rads=".*(desp|exp|imp|p|m|com)" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: desp/exp/impr/p/m + edir -->
    <!-- 5 members -->
    <form suffix="edir" tag="MN0000" synt="Infinitive"/>
    <form suffix="edid" tag="MM02P0" synt="Imperative"/>
    <form suffix="edida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="edidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="edido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="edidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="edimos" tag="MIP1P0"/>
    <form suffix="edimos" tag="MIS1P0"/>
    <form suffix="ediremos" tag="MIF1P0"/>
    <form suffix="edirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="edirán" tag="MIF3P0"/>
    <form suffix="edirás" tag="MIF2S0"/>
    <form suffix="ediré" tag="MIF1S0"/>
    <form suffix="ediréis" tag="MIF2P0"/>
    <form suffix="ediría" tag="MIC1S0"/>
    <form suffix="ediría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ediríais" tag="MIC2P0"/>
    <form suffix="ediríamos" tag="MIC1P0"/>
    <form suffix="edirían" tag="MIC3P0"/>
    <form suffix="edirías" tag="MIC2S0"/>
    <form suffix="ediste" tag="MIS2S0"/>
    <form suffix="edisteis" tag="MIS2P0"/>
    <form suffix="edí" tag="MIS1S0"/>
    <form suffix="edía" tag="MII1S0"/>
    <form suffix="edía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="edíais" tag="MII2P0"/>
    <form suffix="edíamos" tag="MII1P0"/>
    <form suffix="edían" tag="MII3P0"/>
    <form suffix="edías" tag="MII2S0"/>
    <form suffix="edís" tag="MIP2P0"/>
    <form suffix="ida" tag="MM03S0" synt="Imperative"/>
    <form suffix="ida" tag="MSP1S0"/>
    <form suffix="ida" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="idamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="idamos" tag="MSP1P0"/>
    <form suffix="idan" tag="MM03P0" synt="Imperative"/>
    <form suffix="idan" tag="MSP3P0"/>
    <form suffix="idas" tag="MSP2S0"/>
    <form suffix="ide" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ide" tag="MM02S0" synt="Imperative"/>
    <form suffix="iden" tag="MIP3P0"/>
    <form suffix="ides" tag="MIP2S0"/>
    <form suffix="idiendo" tag="MG0000"/>
    <form suffix="idiera" tag="MSI1S0"/>
    <form suffix="idiera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="idierais" tag="MSI2P0"/>
    <form suffix="idieran" tag="MSI3P0"/>
    <form suffix="idieras" tag="MSI2S0"/>
    <form suffix="idiere" tag="MSF1S0"/>
    <form suffix="idiere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="idiereis" tag="MSF2P0"/>
    <form suffix="idieren" tag="MSF3P0"/>
    <form suffix="idieres" tag="MSF2S0"/>
    <form suffix="idieron" tag="MIS3P0"/>
    <form suffix="idiese" tag="MSI1S0"/>
    <form suffix="idiese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="idieseis" tag="MSI2P0"/>
    <form suffix="idiesen" tag="MSI3P0"/>
    <form suffix="idieses" tag="MSI2S0"/>
    <form suffix="idiéramos" tag="MSI1P0"/>
    <form suffix="idiéremos" tag="MSF1P0"/>
    <form suffix="idiésemos" tag="MSI1P0"/>
    <form suffix="idió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ido" tag="MIP1S0"/>
    <form suffix="idáis" tag="MSP2P0"/>
  </table>
  <table name="V69" rads=".*(conc|ac|desconc|desac|desp)" fast="-">
    <form suffix="ertar" tag="MN0000" synt="Infinitive"/>
    <form suffix="ertaba" tag="MII1S0"/>
    <form suffix="ertaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="ertabais" tag="MII2P0"/>
    <form suffix="ertaban" tag="MII3P0"/>
    <form suffix="ertabas" tag="MII2S0"/>
    <form suffix="ertad" tag="MM02P0" synt="Imperative"/>
    <form suffix="ertada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ertadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ertado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ertados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ertamos" tag="MIP1P0"/>
    <form suffix="ertamos" tag="MIS1P0"/>
    <form suffix="ertando" tag="MG0000"/>
    <form suffix="ertara" tag="MSI1S0"/>
    <form suffix="ertara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ertarais" tag="MSI2P0"/>
    <form suffix="ertaran" tag="MSI3P0"/>
    <form suffix="ertaras" tag="MSI2S0"/>
    <form suffix="ertare" tag="MSF1S0"/>
    <form suffix="ertare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ertareis" tag="MSF2P0"/>
    <form suffix="ertaremos" tag="MIF1P0"/>
    <form suffix="ertaren" tag="MSF3P0"/>
    <form suffix="ertares" tag="MSF2S0"/>
    <form suffix="ertaron" tag="MIS3P0"/>
    <form suffix="ertará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="ertarán" tag="MIF3P0"/>
    <form suffix="ertarás" tag="MIF2S0"/>
    <form suffix="ertaré" tag="MIF1S0"/>
    <form suffix="ertaréis" tag="MIF2P0"/>
    <form suffix="ertaría" tag="MIC1S0"/>
    <form suffix="ertaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ertaríais" tag="MIC2P0"/>
    <form suffix="ertaríamos" tag="MIC1P0"/>
    <form suffix="ertarían" tag="MIC3P0"/>
    <form suffix="ertarías" tag="MIC2S0"/>
    <form suffix="ertase" tag="MSI1S0"/>
    <form suffix="ertase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ertaseis" tag="MSI2P0"/>
    <form suffix="ertasen" tag="MSI3P0"/>
    <form suffix="ertases" tag="MSI2S0"/>
    <form suffix="ertaste" tag="MIS2S0"/>
    <form suffix="ertasteis" tag="MIS2P0"/>
    <form suffix="ertemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="ertemos" tag="MSP1P0"/>
    <form suffix="ertábamos" tag="MII1P0"/>
    <form suffix="ertáis" tag="MIP2P0"/>
    <form suffix="ertáramos" tag="MSI1P0"/>
    <form suffix="ertáremos" tag="MSF1P0"/>
    <form suffix="ertásemos" tag="MSI1P0"/>
    <form suffix="erté" tag="MIS1S0"/>
    <form suffix="ertéis" tag="MSP2P0"/>
    <form suffix="ertó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ierta" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ierta" tag="MM02S0" synt="Imperative"/>
    <form suffix="iertan" tag="MIP3P0"/>
    <form suffix="iertas" tag="MIP2S0"/>
    <form suffix="ierte" tag="MM03S0" synt="Imperative"/>
    <form suffix="ierte" tag="MSP1S0"/>
    <form suffix="ierte" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ierten" tag="MM03P0" synt="Imperative"/>
    <form suffix="ierten" tag="MSP3P0"/>
    <form suffix="iertes" tag="MSP2S0"/>
    <form suffix="ierto" tag="MIP1S0"/>
  </table>
  <table name="V71" rads=".*(el|preel|r|corr|reel)" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: el/preel/r/corr + egir -->
    <!-- 4 members -->
    <form suffix="egir" tag="MN0000" synt="Infinitive"/>
    <form suffix="egid" tag="MM02P0" synt="Imperative"/>
    <form suffix="egida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="egidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="egido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="egidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="egimos" tag="MIP1P0"/>
    <form suffix="egimos" tag="MIS1P0"/>
    <form suffix="egiremos" tag="MIF1P0"/>
    <form suffix="egirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="egirán" tag="MIF3P0"/>
    <form suffix="egirás" tag="MIF2S0"/>
    <form suffix="egiré" tag="MIF1S0"/>
    <form suffix="egiréis" tag="MIF2P0"/>
    <form suffix="egiría" tag="MIC1S0"/>
    <form suffix="egiría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="egiríais" tag="MIC2P0"/>
    <form suffix="egiríamos" tag="MIC1P0"/>
    <form suffix="egirían" tag="MIC3P0"/>
    <form suffix="egirías" tag="MIC2S0"/>
    <form suffix="egiste" tag="MIS2S0"/>
    <form suffix="egisteis" tag="MIS2P0"/>
    <form suffix="egí" tag="MIS1S0"/>
    <form suffix="egía" tag="MII1S0"/>
    <form suffix="egía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="egíais" tag="MII2P0"/>
    <form suffix="egíamos" tag="MII1P0"/>
    <form suffix="egían" tag="MII3P0"/>
    <form suffix="egías" tag="MII2S0"/>
    <form suffix="egís" tag="MIP2P0"/>
    <form suffix="ige" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ige" tag="MM02S0" synt="Imperative"/>
    <form suffix="igen" tag="MIP3P0"/>
    <form suffix="iges" tag="MIP2S0"/>
    <form suffix="igiendo" tag="MG0000"/>
    <form suffix="igiera" tag="MSI1S0"/>
    <form suffix="igiera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="igierais" tag="MSI2P0"/>
    <form suffix="igieran" tag="MSI3P0"/>
    <form suffix="igieras" tag="MSI2S0"/>
    <form suffix="igiere" tag="MSF1S0"/>
    <form suffix="igiere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="igiereis" tag="MSF2P0"/>
    <form suffix="igieren" tag="MSF3P0"/>
    <form suffix="igieres" tag="MSF2S0"/>
    <form suffix="igieron" tag="MIS3P0"/>
    <form suffix="igiese" tag="MSI1S0"/>
    <form suffix="igiese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="igieseis" tag="MSI2P0"/>
    <form suffix="igiesen" tag="MSI3P0"/>
    <form suffix="igieses" tag="MSI2S0"/>
    <form suffix="igiéramos" tag="MSI1P0"/>
    <form suffix="igiéremos" tag="MSF1P0"/>
    <form suffix="igiésemos" tag="MSI1P0"/>
    <form suffix="igió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ija" tag="MM03S0" synt="Imperative"/>
    <form suffix="ija" tag="MSP1S0"/>
    <form suffix="ija" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ijamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="ijamos" tag="MSP1P0"/>
    <form suffix="ijan" tag="MM03P0" synt="Imperative"/>
    <form suffix="ijan" tag="MSP3P0"/>
    <form suffix="ijas" tag="MSP2S0"/>
    <form suffix="ijo" tag="MIP1S0"/>
    <form suffix="ijáis" tag="MSP2P0"/>
  </table>
  <table name="V72" rads=".*(f|alm|esf|ref)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: f/alm/esf/ref + orzar -->
    <!-- 4 members -->
    <form suffix="orzar" tag="MN0000" synt="Infinitive"/>
    <form suffix="orcemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="orcemos" tag="MSP1P0"/>
    <form suffix="orcé" tag="MIS1S0"/>
    <form suffix="orcéis" tag="MSP2P0"/>
    <form suffix="orzaba" tag="MII1S0"/>
    <form suffix="orzaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="orzabais" tag="MII2P0"/>
    <form suffix="orzaban" tag="MII3P0"/>
    <form suffix="orzabas" tag="MII2S0"/>
    <form suffix="orzad" tag="MM02P0" synt="Imperative"/>
    <form suffix="orzada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="orzadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="orzado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="orzados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="orzamos" tag="MIP1P0"/>
    <form suffix="orzamos" tag="MIS1P0"/>
    <form suffix="orzando" tag="MG0000"/>
    <form suffix="orzara" tag="MSI1S0"/>
    <form suffix="orzara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="orzarais" tag="MSI2P0"/>
    <form suffix="orzaran" tag="MSI3P0"/>
    <form suffix="orzaras" tag="MSI2S0"/>
    <form suffix="orzare" tag="MSF1S0"/>
    <form suffix="orzare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="orzareis" tag="MSF2P0"/>
    <form suffix="orzaremos" tag="MIF1P0"/>
    <form suffix="orzaren" tag="MSF3P0"/>
    <form suffix="orzares" tag="MSF2S0"/>
    <form suffix="orzaron" tag="MIS3P0"/>
    <form suffix="orzará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="orzarán" tag="MIF3P0"/>
    <form suffix="orzarás" tag="MIF2S0"/>
    <form suffix="orzaré" tag="MIF1S0"/>
    <form suffix="orzaréis" tag="MIF2P0"/>
    <form suffix="orzaría" tag="MIC1S0"/>
    <form suffix="orzaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="orzaríais" tag="MIC2P0"/>
    <form suffix="orzaríamos" tag="MIC1P0"/>
    <form suffix="orzarían" tag="MIC3P0"/>
    <form suffix="orzarías" tag="MIC2S0"/>
    <form suffix="orzase" tag="MSI1S0"/>
    <form suffix="orzase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="orzaseis" tag="MSI2P0"/>
    <form suffix="orzasen" tag="MSI3P0"/>
    <form suffix="orzases" tag="MSI2S0"/>
    <form suffix="orzaste" tag="MIS2S0"/>
    <form suffix="orzasteis" tag="MIS2P0"/>
    <form suffix="orzábamos" tag="MII1P0"/>
    <form suffix="orzáis" tag="MIP2P0"/>
    <form suffix="orzáramos" tag="MSI1P0"/>
    <form suffix="orzáremos" tag="MSF1P0"/>
    <form suffix="orzásemos" tag="MSI1P0"/>
    <form suffix="orzó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="uerce" tag="MM03S0" synt="Imperative"/>
    <form suffix="uerce" tag="MSP1S0"/>
    <form suffix="uerce" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uercen" tag="MM03P0" synt="Imperative"/>
    <form suffix="uercen" tag="MSP3P0"/>
    <form suffix="uerces" tag="MSP2S0"/>
    <form suffix="uerza" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uerza" tag="MM02S0" synt="Imperative"/>
    <form suffix="uerzan" tag="MIP3P0"/>
    <form suffix="uerzas" tag="MIP2S0"/>
    <form suffix="uerzo" tag="MIP1S0"/>
  </table>
  <table name="V74" rads=".*(trasf|zah|circunf|eng)" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: trasf/zah/circunf/eng + erir -->
    <!-- 4 members -->
    <form suffix="erir" tag="MN0000" synt="Infinitive"/>
    <form suffix="erid" tag="MM02P0" synt="Imperative"/>
    <form suffix="erida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="eridas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="erido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="eridos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="erimos" tag="MIP1P0"/>
    <form suffix="erimos" tag="MIS1P0"/>
    <form suffix="eriremos" tag="MIF1P0"/>
    <form suffix="erirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="erirán" tag="MIF3P0"/>
    <form suffix="erirás" tag="MIF2S0"/>
    <form suffix="eriré" tag="MIF1S0"/>
    <form suffix="eriréis" tag="MIF2P0"/>
    <form suffix="eriría" tag="MIC1S0"/>
    <form suffix="eriría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="eriríais" tag="MIC2P0"/>
    <form suffix="eriríamos" tag="MIC1P0"/>
    <form suffix="erirían" tag="MIC3P0"/>
    <form suffix="erirías" tag="MIC2S0"/>
    <form suffix="eriste" tag="MIS2S0"/>
    <form suffix="eristeis" tag="MIS2P0"/>
    <form suffix="erí" tag="MIS1S0"/>
    <form suffix="ería" tag="MII1S0"/>
    <form suffix="ería" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="eríais" tag="MII2P0"/>
    <form suffix="eríamos" tag="MII1P0"/>
    <form suffix="erían" tag="MII3P0"/>
    <form suffix="erías" tag="MII2S0"/>
    <form suffix="erís" tag="MIP2P0"/>
    <form suffix="iera" tag="MSP1S0"/>
    <form suffix="iera" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="iera" tag="MM02S0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ieran" tag="MSP3P0"/>
    <form suffix="ieran" tag="MM03P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ieras" tag="MSP2S0"/>
    <form suffix="iere" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="iere" tag="MM02S0" synt="Imperative"/>
    <form suffix="ieren" tag="MIP3P0"/>
    <form suffix="ieres" tag="MIP2S0"/>
    <form suffix="iero" tag="MIP1S0"/>
    <form suffix="iramos" tag="MSP1P0"/>
    <form suffix="iramos" tag="MM01P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="iriendo" tag="MG0000"/>
    <form suffix="iriera" tag="MSI1S0"/>
    <form suffix="iriera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="irierais" tag="MSI2P0"/>
    <form suffix="irieran" tag="MSI3P0"/>
    <form suffix="irieras" tag="MSI2S0"/>
    <form suffix="iriere" tag="MSF1S0"/>
    <form suffix="iriere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="iriereis" tag="MSF2P0"/>
    <form suffix="irieren" tag="MSF3P0"/>
    <form suffix="irieres" tag="MSF2S0"/>
    <form suffix="irieron" tag="MIS3P0"/>
    <form suffix="iriese" tag="MSI1S0"/>
    <form suffix="iriese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="irieseis" tag="MSI2P0"/>
    <form suffix="iriesen" tag="MSI3P0"/>
    <form suffix="irieses" tag="MSI2S0"/>
    <form suffix="iriéramos" tag="MSI1P0"/>
    <form suffix="iriéremos" tag="MSF1P0"/>
    <form suffix="iriésemos" tag="MSI1P0"/>
    <form suffix="irió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="iráis" tag="MSP2P0"/>
  </table>
  <table name="V75" rads=".*(inqui|adqui|readqui|perqui)" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: inqui/adqui/readqui/perqui + rir -->
    <!-- 4 members -->
    <form suffix="rir" tag="MN0000" synt="Infinitive"/>
    <form suffix="era" tag="MM03S0" synt="Imperative"/>
    <form suffix="era" tag="MSP1S0"/>
    <form suffix="era" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="eran" tag="MM03P0" synt="Imperative"/>
    <form suffix="eran" tag="MSP3P0"/>
    <form suffix="eras" tag="MSP2S0"/>
    <form suffix="ere" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ere" tag="MM02S0" synt="Imperative"/>
    <form suffix="eren" tag="MIP3P0"/>
    <form suffix="eres" tag="MIP2S0"/>
    <form suffix="ero" tag="MIP1S0"/>
    <form suffix="ramos" tag="MM01P0" synt="Imperative"/>
    <form suffix="ramos" tag="MSP1P0"/>
    <form suffix="rid" tag="MM02P0" synt="Imperative"/>
    <form suffix="rida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ridas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="rido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ridos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="riendo" tag="MG0000"/>
    <form suffix="riera" tag="MSI1S0"/>
    <form suffix="riera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="rierais" tag="MSI2P0"/>
    <form suffix="rieran" tag="MSI3P0"/>
    <form suffix="rieras" tag="MSI2S0"/>
    <form suffix="riere" tag="MSF1S0"/>
    <form suffix="riere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="riereis" tag="MSF2P0"/>
    <form suffix="rieren" tag="MSF3P0"/>
    <form suffix="rieres" tag="MSF2S0"/>
    <form suffix="rieron" tag="MIS3P0"/>
    <form suffix="riese" tag="MSI1S0"/>
    <form suffix="riese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="rieseis" tag="MSI2P0"/>
    <form suffix="riesen" tag="MSI3P0"/>
    <form suffix="rieses" tag="MSI2S0"/>
    <form suffix="rimos" tag="MIP1P0"/>
    <form suffix="rimos" tag="MIS1P0"/>
    <form suffix="riremos" tag="MIF1P0"/>
    <form suffix="rirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="rirán" tag="MIF3P0"/>
    <form suffix="rirás" tag="MIF2S0"/>
    <form suffix="riré" tag="MIF1S0"/>
    <form suffix="riréis" tag="MIF2P0"/>
    <form suffix="riría" tag="MIC1S0"/>
    <form suffix="riría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="riríais" tag="MIC2P0"/>
    <form suffix="riríamos" tag="MIC1P0"/>
    <form suffix="rirían" tag="MIC3P0"/>
    <form suffix="rirías" tag="MIC2S0"/>
    <form suffix="riste" tag="MIS2S0"/>
    <form suffix="risteis" tag="MIS2P0"/>
    <form suffix="riéramos" tag="MSI1P0"/>
    <form suffix="riéremos" tag="MSF1P0"/>
    <form suffix="riésemos" tag="MSI1P0"/>
    <form suffix="rió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ráis" tag="MSP2P0"/>
    <form suffix="rí" tag="MIS1S0"/>
    <form suffix="ría" tag="MII1S0"/>
    <form suffix="ría" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="ríais" tag="MII2P0"/>
    <form suffix="ríamos" tag="MII1P0"/>
    <form suffix="rían" tag="MII3P0"/>
    <form suffix="rías" tag="MII2S0"/>
    <form suffix="rís" tag="MIP2P0"/>
  </table>
  <table name="V76" rads=".*(frun|zur|resar|espar)" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: frun/zur/resar/espar + cir -->
    <form suffix="cir" tag="MN0000" synt="Infinitive"/>
    <form suffix="ce" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ce" tag="MM02S0" synt="Imperative"/>
    <form suffix="cen" tag="MIP3P0"/>
    <form suffix="ces" tag="MIP2S0"/>
    <form suffix="cid" tag="MM02P0" synt="Imperative"/>
    <form suffix="cida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="cidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="cido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="cidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ciendo" tag="MG0000"/>
    <form suffix="ciera" tag="MSI1S0"/>
    <form suffix="ciera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="cierais" tag="MSI2P0"/>
    <form suffix="cieran" tag="MSI3P0"/>
    <form suffix="cieras" tag="MSI2S0"/>
    <form suffix="ciere" tag="MSF1S0"/>
    <form suffix="ciere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ciereis" tag="MSF2P0"/>
    <form suffix="cieren" tag="MSF3P0"/>
    <form suffix="cieres" tag="MSF2S0"/>
    <form suffix="cieron" tag="MIS3P0"/>
    <form suffix="ciese" tag="MSI1S0"/>
    <form suffix="ciese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="cieseis" tag="MSI2P0"/>
    <form suffix="ciesen" tag="MSI3P0"/>
    <form suffix="cieses" tag="MSI2S0"/>
    <form suffix="cimos" tag="MIP1P0"/>
    <form suffix="cimos" tag="MIS1P0"/>
    <form suffix="ciremos" tag="MIF1P0"/>
    <form suffix="cirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="cirán" tag="MIF3P0"/>
    <form suffix="cirás" tag="MIF2S0"/>
    <form suffix="ciré" tag="MIF1S0"/>
    <form suffix="ciréis" tag="MIF2P0"/>
    <form suffix="ciría" tag="MIC1S0"/>
    <form suffix="ciría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ciríais" tag="MIC2P0"/>
    <form suffix="ciríamos" tag="MIC1P0"/>
    <form suffix="cirían" tag="MIC3P0"/>
    <form suffix="cirías" tag="MIC2S0"/>
    <form suffix="ciste" tag="MIS2S0"/>
    <form suffix="cisteis" tag="MIS2P0"/>
    <form suffix="ciéramos" tag="MSI1P0"/>
    <form suffix="ciéremos" tag="MSF1P0"/>
    <form suffix="ciésemos" tag="MSI1P0"/>
    <form suffix="ció" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="cí" tag="MIS1S0"/>
    <form suffix="cía" tag="MII1S0"/>
    <form suffix="cía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="cíais" tag="MII2P0"/>
    <form suffix="cíamos" tag="MII1P0"/>
    <form suffix="cían" tag="MII3P0"/>
    <form suffix="cías" tag="MII2S0"/>
    <form suffix="cís" tag="MIP2P0"/>
    <form suffix="za" tag="MM03S0" synt="Imperative"/>
    <form suffix="za" tag="MSP1S0"/>
    <form suffix="za" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="zamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="zamos" tag="MSP1P0"/>
    <form suffix="zan" tag="MM03P0" synt="Imperative"/>
    <form suffix="zan" tag="MSP3P0"/>
    <form suffix="zas" tag="MSP2S0"/>
    <form suffix="zo" tag="MIP1S0"/>
    <form suffix="záis" tag="MSP2P0"/>
  </table>
  <!--table name="V77" rads=".*(sol|tej|repel|prend)" fast="-"-->
  <table name="V77" rads=".*s" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: s + olar -->
    <!-- 4 members -->
    <form suffix="olar" tag="MN0000" synt="Infinitive"/>
    <form suffix="olaba" tag="MII1S0"/>
    <form suffix="olaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="olábamos" tag="MII1P0"/> <!-- Nieves: added all the forms for Imperfective -->
    <form suffix="olabais" tag="MII2P0"/>
    <form suffix="olaban" tag="MII3P0"/>
    <form suffix="olabas" tag="MII2S0"/>
    <form suffix="olad" tag="MM02P0" synt="Imperative"/>
    <form suffix="olada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="oladas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="olado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="olados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="oláis" tag="MIP2P0"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="olamos" tag="MIP1P0"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="olamos" tag="MIS1P0"/> <!-- Nieves: added all the forms for Past -->
    <form suffix="olando" tag="MG0000"/>
    <form suffix="olara" tag="MSI1S0"/>
    <form suffix="olara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="olarais" tag="MSI2P0"/>
    <form suffix="olaran" tag="MSI3P0"/>
    <form suffix="olaras" tag="MSI2S0"/>
    <form suffix="olare" tag="MSF1S0"/>
    <form suffix="olare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="olareis" tag="MSF2P0"/>
    <form suffix="olaremos" tag="MIF1P0"/>
    <form suffix="olaren" tag="MSF3P0"/>
    <form suffix="olares" tag="MSF2S0"/>
    <form suffix="olaron" tag="MIS3P0"/>
    <form suffix="olará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="olarán" tag="MIF3P0"/>
    <form suffix="olarás" tag="MIF2S0"/>
    <form suffix="olaré" tag="MIF1S0"/>
    <form suffix="olaréis" tag="MIF2P0"/>
    <form suffix="olaría" tag="MIC1S0"/>
    <form suffix="olaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="olaríais" tag="MIC2P0"/>
    <form suffix="olaríamos" tag="MIC1P0"/>
    <form suffix="olarían" tag="MIC3P0"/>
    <form suffix="olarías" tag="MIC2S0"/>
    <form suffix="olase" tag="MSI1S0"/>
    <form suffix="olase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="olaseis" tag="MSI2P0"/>
    <form suffix="olasen" tag="MSI3P0"/>
    <form suffix="olases" tag="MSI2S0"/>
    <form suffix="olaste" tag="MIS2S0"/>
    <form suffix="olasteis" tag="MIS2P0"/>
    <form suffix="olemos" tag="MSI1P0"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="olemos" tag="MM01P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="olábamos" tag="MII1P0"/>
    <form suffix="oláramos" tag="MSI1P0"/>
    <form suffix="oláremos" tag="MSF1P0"/>
    <form suffix="olásemos" tag="MSI1P0"/>
    <form suffix="olé" tag="MIS1S0"/>
    <form suffix="oló" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="uela" tag="MIP3S0" synt="ThirSing"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="uela" tag="MM02S0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="uelan" tag="MIP3P0"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="uelas" tag="MIP2S0"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="uele" tag="MSP1S0"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="uele" tag="MM03S0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="uele" tag="MSP3S0" synt="ThirSing"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="uelen" tag="MSP3P0"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="uelen" tag="MM03P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ueles" tag="MSP2S0"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="uelo" tag="MIP1S0"/> <!-- Nieves: added all the forms for Present -->
  </table>
  <table name="V78" rads=".*(unis|dis|t|atr)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: unis/dis/t/atr + onar -->
    <!-- 4 members -->
    <form suffix="onar" tag="MN0000" synt="Infinitive"/>
    <form suffix="onaba" tag="MII1S0"/>
    <form suffix="onaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="onabais" tag="MII2P0"/>
    <form suffix="onaban" tag="MII3P0"/>
    <form suffix="onabas" tag="MII2S0"/>
    <form suffix="onad" tag="MM02P0" synt="Imperative"/>
    <form suffix="onada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="onadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="onado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="onados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="onamos" tag="MIP1P0"/>
    <form suffix="onamos" tag="MIS1P0"/>
    <form suffix="onando" tag="MG0000"/>
    <form suffix="onara" tag="MSI1S0"/>
    <form suffix="onara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="onarais" tag="MSI2P0"/>
    <form suffix="onaran" tag="MSI3P0"/>
    <form suffix="onaras" tag="MSI2S0"/>
    <form suffix="onare" tag="MSF1S0"/>
    <form suffix="onare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="onareis" tag="MSF2P0"/>
    <form suffix="onaremos" tag="MIF1P0"/>
    <form suffix="onaren" tag="MSF3P0"/>
    <form suffix="onares" tag="MSF2S0"/>
    <form suffix="onaron" tag="MIS3P0"/>
    <form suffix="onará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="onarán" tag="MIF3P0"/>
    <form suffix="onarás" tag="MIF2S0"/>
    <form suffix="onaré" tag="MIF1S0"/>
    <form suffix="onaréis" tag="MIF2P0"/>
    <form suffix="onaría" tag="MIC1S0"/>
    <form suffix="onaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="onaríais" tag="MIC2P0"/>
    <form suffix="onaríamos" tag="MIC1P0"/>
    <form suffix="onarían" tag="MIC3P0"/>
    <form suffix="onarías" tag="MIC2S0"/>
    <form suffix="onase" tag="MSI1S0"/>
    <form suffix="onase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="onaseis" tag="MSI2P0"/>
    <form suffix="onasen" tag="MSI3P0"/>
    <form suffix="onases" tag="MSI2S0"/>
    <form suffix="onaste" tag="MIS2S0"/>
    <form suffix="onasteis" tag="MIS2P0"/>
    <form suffix="onemos" tag="MSP1P0"/>
    <form suffix="onemos" tag="MM01P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="onábamos" tag="MII1P0"/>
    <form suffix="onáis" tag="MIP2P0"/>
    <form suffix="onáramos" tag="MSI1P0"/>
    <form suffix="onáremos" tag="MSF1P0"/>
    <form suffix="onásemos" tag="MSI1P0"/>
    <form suffix="oné" tag="MIS1S0"/>
    <form suffix="onéis" tag="MSP2P0"/>
    <form suffix="onó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="uena" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uena" tag="MM02S0" synt="Imperative"/>
    <form suffix="uenan" tag="MIP3P0"/>
    <form suffix="uenas" tag="MIP2S0"/>
    <form suffix="uene" tag="MSP1S0"/>
    <form suffix="uene" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uene" tag="MM03S0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="uenen" tag="MSP3P0"/>
    <form suffix="uenen" tag="MM03P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="uenes" tag="MSP2S0"/>
    <form suffix="ueno" tag="MIP1S0"/>
  </table>
  <table name="V79" rads=".*(requ|perniqu|patiqu|resqu|qu)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: requ/perniqu/patiqu/resqu + ebrar -->
    <!-- 4 members -->
    <form suffix="ebrar" tag="MN0000" synt="Infinitive"/>
    <form suffix="ebraba" tag="MII1S0"/>
    <form suffix="ebraba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="ebrabais" tag="MII2P0"/>
    <form suffix="ebraban" tag="MII3P0"/>
    <form suffix="ebrabas" tag="MII2S0"/>
    <form suffix="ebrad" tag="MM02P0" synt="Imperative"/>
    <form suffix="ebrada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ebradas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ebrado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ebrados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ebramos" tag="MIP1P0"/>
    <form suffix="ebramos" tag="MIS1P0"/>
    <form suffix="ebrando" tag="MG0000"/>
    <form suffix="ebrara" tag="MSI1S0"/>
    <form suffix="ebrara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ebrarais" tag="MSI2P0"/>
    <form suffix="ebraran" tag="MSI3P0"/>
    <form suffix="ebraras" tag="MSI2S0"/>
    <form suffix="ebrare" tag="MSF1S0"/>
    <form suffix="ebrare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ebrareis" tag="MSF2P0"/>
    <form suffix="ebraremos" tag="MIF1P0"/>
    <form suffix="ebraren" tag="MSF3P0"/>
    <form suffix="ebrares" tag="MSF2S0"/>
    <form suffix="ebraron" tag="MIS3P0"/>
    <form suffix="ebrará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="ebrarán" tag="MIF3P0"/>
    <form suffix="ebrarás" tag="MIF2S0"/>
    <form suffix="ebraré" tag="MIF1S0"/>
    <form suffix="ebraréis" tag="MIF2P0"/>
    <form suffix="ebraría" tag="MIC1S0"/>
    <form suffix="ebraría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ebraríais" tag="MIC2P0"/>
    <form suffix="ebraríamos" tag="MIC1P0"/>
    <form suffix="ebrarían" tag="MIC3P0"/>
    <form suffix="ebrarías" tag="MIC2S0"/>
    <form suffix="ebrase" tag="MSI1S0"/>
    <form suffix="ebrase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ebraseis" tag="MSI2P0"/>
    <form suffix="ebrasen" tag="MSI3P0"/>
    <form suffix="ebrases" tag="MSI2S0"/>
    <form suffix="ebraste" tag="MIS2S0"/>
    <form suffix="ebrasteis" tag="MIS2P0"/>
    <form suffix="ebremos" tag="MSP1P0"/>
    <form suffix="ebremos" tag="MM01P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ebrábamos" tag="MII1P0"/>
    <form suffix="ebráis" tag="MIP2P0"/>
    <form suffix="ebráramos" tag="MSI1P0"/>
    <form suffix="ebráremos" tag="MSF1P0"/>
    <form suffix="ebrásemos" tag="MSI1P0"/>
    <form suffix="ebré" tag="MIS1S0"/>
    <form suffix="ebréis" tag="MSP2P0"/>
    <form suffix="ebró" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="iebra" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="iebra" tag="MM02S0" synt="Imperative"/>
    <form suffix="iebran" tag="MIP3P0"/>
    <form suffix="iebras" tag="MIP2S0"/>
    <form suffix="iebre" tag="MSP1S0"/>
    <form suffix="iebre" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="iebre" tag="MM03S0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="iebren" tag="MSP3P0"/>
    <form suffix="iebren" tag="MM03P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="iebres" tag="MSP2S0"/>
    <form suffix="iebro" tag="MIP1S0"/>
  </table>
  <table name="V80" rads=".*(prom|m|conm|rem|ll)" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: prom/m/conm/rem + over -->
    <!-- 4 members -->
    <form suffix="over" tag="MN0000" synt="Infinitive"/>
    <form suffix="ovamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="ovamos" tag="MSP1P0"/>
    <form suffix="oved" tag="MM02P0" synt="Imperative"/>
    <form suffix="ovemos" tag="MIP1P0"/>
    <form suffix="overemos" tag="MIF1P0"/>
    <form suffix="overá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="overán" tag="MIF3P0"/>
    <form suffix="overás" tag="MIF2S0"/>
    <form suffix="overé" tag="MIF1S0"/>
    <form suffix="overéis" tag="MIF2P0"/>
    <form suffix="overía" tag="MIC1S0"/>
    <form suffix="overía" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="overíais" tag="MIC2P0"/>
    <form suffix="overíamos" tag="MIC1P0"/>
    <form suffix="overían" tag="MIC3P0"/>
    <form suffix="overías" tag="MIC2S0"/>
    <form suffix="ovida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ovidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ovido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ovidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="oviendo" tag="MG0000"/>
    <form suffix="oviera" tag="MSI1S0"/>
    <form suffix="oviera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ovierais" tag="MSI2P0"/>
    <form suffix="ovieran" tag="MSI3P0"/>
    <form suffix="ovieras" tag="MSI2S0"/>
    <form suffix="oviere" tag="MSF1S0"/>
    <form suffix="oviere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="oviereis" tag="MSF2P0"/>
    <form suffix="ovieren" tag="MSF3P0"/>
    <form suffix="ovieres" tag="MSF2S0"/>
    <form suffix="ovieron" tag="MIS3P0"/>
    <form suffix="oviese" tag="MSI1S0"/>
    <form suffix="oviese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ovieseis" tag="MSI2P0"/>
    <form suffix="oviesen" tag="MSI3P0"/>
    <form suffix="ovieses" tag="MSI2S0"/>
    <form suffix="ovimos" tag="MIS1P0"/>
    <form suffix="oviste" tag="MIS2S0"/>
    <form suffix="ovisteis" tag="MIS2P0"/>
    <form suffix="oviéramos" tag="MSI1P0"/>
    <form suffix="oviéremos" tag="MSF1P0"/>
    <form suffix="oviésemos" tag="MSI1P0"/>
    <form suffix="ovió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ováis" tag="MSP2P0"/>
    <form suffix="ovéis" tag="MIP2P0"/>
    <form suffix="oví" tag="MIS1S0"/>
    <form suffix="ovía" tag="MII1S0"/>
    <form suffix="ovía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="ovíais" tag="MII2P0"/>
    <form suffix="ovíamos" tag="MII1P0"/>
    <form suffix="ovían" tag="MII3P0"/>
    <form suffix="ovías" tag="MII2S0"/>
    <form suffix="ueva" tag="MM03S0" synt="Imperative"/>
    <form suffix="ueva" tag="MSP1S0"/>
    <form suffix="ueva" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uevan" tag="MM03P0" synt="Imperative"/>
    <form suffix="uevan" tag="MSP3P0"/>
    <form suffix="uevas" tag="MSP2S0"/>
    <form suffix="ueve" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ueve" tag="MM02S0" synt="Imperative"/>
    <form suffix="ueven" tag="MIP3P0"/>
    <form suffix="ueves" tag="MIP2S0"/>
    <form suffix="uevo" tag="MIP1S0"/>
  </table>
  <table name="V81" rads=".*(un|desun|desfrun|estar)" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: un/desun/desfrun/estar + cir -->
    <form suffix="cir" tag="MN0000" synt="Infinitive"/>
    <form suffix="ce" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ce" tag="MM02S0" synt="Imperative"/>
    <form suffix="cen" tag="MIP3P0"/>
    <form suffix="ces" tag="MIP2S0"/>
    <form suffix="cid" tag="MM02P0" synt="Imperative"/>
    <form suffix="cida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="cidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="cido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="cidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ciendo" tag="MG0000"/>
    <form suffix="ciera" tag="MSI1S0"/>
    <form suffix="ciera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="cierais" tag="MSI2P0"/>
    <form suffix="cieran" tag="MSI3P0"/>
    <form suffix="cieras" tag="MSI2S0"/>
    <form suffix="ciere" tag="MSF1S0"/>
    <form suffix="ciere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ciereis" tag="MSF2P0"/>
    <form suffix="cieren" tag="MSF3P0"/>
    <form suffix="cieres" tag="MSF2S0"/>
    <form suffix="cieron" tag="MIS3P0"/>
    <form suffix="ciese" tag="MSI1S0"/>
    <form suffix="ciese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="cieseis" tag="MSI2P0"/>
    <form suffix="ciesen" tag="MSI3P0"/>
    <form suffix="cieses" tag="MSI2S0"/>
    <form suffix="cimos" tag="MIP1P0"/>
    <form suffix="cimos" tag="MIS1P0"/>
    <form suffix="ciremos" tag="MIF1P0"/>
    <form suffix="cirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="cirán" tag="MIF3P0"/>
    <form suffix="cirás" tag="MIF2S0"/>
    <form suffix="ciré" tag="MIF1S0"/>
    <form suffix="ciréis" tag="MIF2P0"/>
    <form suffix="ciría" tag="MIC1S0"/>
    <form suffix="ciría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ciríais" tag="MIC2P0"/>
    <form suffix="ciríamos" tag="MIC1P0"/>
    <form suffix="cirían" tag="MIC3P0"/>
    <form suffix="cirías" tag="MIC2S0"/>
    <form suffix="ciste" tag="MIS2S0"/>
    <form suffix="cisteis" tag="MIS2P0"/>
    <form suffix="ciéramos" tag="MSI1P0"/>
    <form suffix="ciéremos" tag="MSF1P0"/>
    <form suffix="ciésemos" tag="MSI1P0"/>
    <form suffix="ció" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="cí" tag="MIS1S0"/>
    <form suffix="cía" tag="MII1S0"/>
    <form suffix="cía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="cíais" tag="MII2P0"/>
    <form suffix="cíamos" tag="MII1P0"/>
    <form suffix="cían" tag="MII3P0"/>
    <form suffix="cías" tag="MII2S0"/>
    <form suffix="cís" tag="MIP2P0"/>
    <form suffix="za" tag="MSP1S0"/>
    <form suffix="za" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="za" tag="MM03S0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="zamos" tag="MSP1P0"/>
    <form suffix="zamos" tag="MM01P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="zan" tag="MSP3P0"/>
    <form suffix="zan" tag="MM03P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="zas" tag="MSP2S0"/>
    <form suffix="zo" tag="MIP1S0"/>
    <form suffix="záis" tag="MSP2P0"/>
  </table>
  <table name="V82" rads=".*(superp|am|p|desp|rep|m)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: superp/am/p/desp + oblar -->
    <!-- 4 members -->
    <form suffix="oblar" tag="MN0000" synt="Infinitive"/>
    <form suffix="oblaba" tag="MII1S0"/>
    <form suffix="oblaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="oblabais" tag="MII2P0"/>
    <form suffix="oblaban" tag="MII3P0"/>
    <form suffix="oblabas" tag="MII2S0"/>
    <form suffix="oblad" tag="MM02P0" synt="Imperative"/>
    <form suffix="oblada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="obladas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="oblado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="oblados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="oblamos" tag="MIP1P0"/>
    <form suffix="oblamos" tag="MIS1P0"/>
    <form suffix="oblando" tag="MG0000"/>
    <form suffix="oblara" tag="MSI1S0"/>
    <form suffix="oblara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="oblarais" tag="MSI2P0"/>
    <form suffix="oblaran" tag="MSI3P0"/>
    <form suffix="oblaras" tag="MSI2S0"/>
    <form suffix="oblare" tag="MSF1S0"/>
    <form suffix="oblare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="oblareis" tag="MSF2P0"/>
    <form suffix="oblaremos" tag="MIF1P0"/>
    <form suffix="oblaren" tag="MSF3P0"/>
    <form suffix="oblares" tag="MSF2S0"/>
    <form suffix="oblaron" tag="MIS3P0"/>
    <form suffix="oblará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="oblarán" tag="MIF3P0"/>
    <form suffix="oblarás" tag="MIF2S0"/>
    <form suffix="oblaré" tag="MIF1S0"/>
    <form suffix="oblaréis" tag="MIF2P0"/>
    <form suffix="oblaría" tag="MIC1S0"/>
    <form suffix="oblaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="oblaríais" tag="MIC2P0"/>
    <form suffix="oblaríamos" tag="MIC1P0"/>
    <form suffix="oblarían" tag="MIC3P0"/>
    <form suffix="oblarías" tag="MIC2S0"/>
    <form suffix="oblase" tag="MSI1S0"/>
    <form suffix="oblase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="oblaseis" tag="MSI2P0"/>
    <form suffix="oblasen" tag="MSI3P0"/>
    <form suffix="oblases" tag="MSI2S0"/>
    <form suffix="oblaste" tag="MIS2S0"/>
    <form suffix="oblasteis" tag="MIS2P0"/>
    <form suffix="oblemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="oblemos" tag="MSP1P0"/>
    <form suffix="oblábamos" tag="MII1P0"/>
    <form suffix="obláis" tag="MIP2P0"/>
    <form suffix="obláramos" tag="MSI1P0"/>
    <form suffix="obláremos" tag="MSF1P0"/>
    <form suffix="oblásemos" tag="MSI1P0"/>
    <form suffix="oblé" tag="MIS1S0"/>
    <form suffix="obléis" tag="MSP2P0"/>
    <form suffix="obló" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="uebla" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uebla" tag="MM02S0" synt="Imperative"/>
    <form suffix="ueblan" tag="MIP3P0"/>
    <form suffix="ueblas" tag="MIP2S0"/>
    <form suffix="ueble" tag="MM03S0" synt="Imperative"/>
    <form suffix="ueble" tag="MSP1S0"/>
    <form suffix="ueble" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ueblen" tag="MM03P0" synt="Imperative"/>
    <form suffix="ueblen" tag="MSP3P0"/>
    <form suffix="uebles" tag="MSP2S0"/>
    <form suffix="ueblo" tag="MIP1S0"/>
  </table>
  <table name="V83" rads=".*(dem|m|d)" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: dem/m/d + oler -->
    <!-- 3 members -->
    <form suffix="oler" tag="MN0000" synt="Infinitive"/>
    <form suffix="olamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="olamos" tag="MSP1P0"/>
    <form suffix="oled" tag="MM02P0" synt="Imperative"/>
    <form suffix="olemos" tag="MIP1P0"/>
    <form suffix="oleremos" tag="MIF1P0"/>
    <form suffix="olerá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="olerán" tag="MIF3P0"/>
    <form suffix="olerás" tag="MIF2S0"/>
    <form suffix="oleré" tag="MIF1S0"/>
    <form suffix="oleréis" tag="MIF2P0"/>
    <form suffix="olería" tag="MIC1S0"/>
    <form suffix="olería" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="oleríais" tag="MIC2P0"/>
    <form suffix="oleríamos" tag="MIC1P0"/>
    <form suffix="olerían" tag="MIC3P0"/>
    <form suffix="olerías" tag="MIC2S0"/>
    <form suffix="olida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="olidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="olido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="olidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="oliendo" tag="MG0000"/>
    <form suffix="oliera" tag="MSI1S0"/>
    <form suffix="oliera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="olierais" tag="MSI2P0"/>
    <form suffix="olieran" tag="MSI3P0"/>
    <form suffix="olieras" tag="MSI2S0"/>
    <form suffix="oliere" tag="MSF1S0"/>
    <form suffix="oliere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="oliereis" tag="MSF2P0"/>
    <form suffix="olieren" tag="MSF3P0"/>
    <form suffix="olieres" tag="MSF2S0"/>
    <form suffix="olieron" tag="MIS3P0"/>
    <form suffix="oliese" tag="MSI1S0"/>
    <form suffix="oliese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="olieseis" tag="MSI2P0"/>
    <form suffix="oliesen" tag="MSI3P0"/>
    <form suffix="olieses" tag="MSI2S0"/>
    <form suffix="olimos" tag="MIS1P0"/>
    <form suffix="oliste" tag="MIS2S0"/>
    <form suffix="olisteis" tag="MIS2P0"/>
    <form suffix="oliéramos" tag="MSI1P0"/>
    <form suffix="oliéremos" tag="MSF1P0"/>
    <form suffix="oliésemos" tag="MSI1P0"/>
    <form suffix="olió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="oláis" tag="MSP2P0"/>
    <form suffix="oléis" tag="MIP2P0"/>
    <form suffix="olí" tag="MIS1S0"/>
    <form suffix="olía" tag="MII1S0"/>
    <form suffix="olía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="olíais" tag="MII2P0"/>
    <form suffix="olíamos" tag="MII1P0"/>
    <form suffix="olían" tag="MII3P0"/>
    <form suffix="olías" tag="MII2S0"/>
    <form suffix="uela" tag="MM03S0" synt="Imperative"/>
    <form suffix="uela" tag="MSP1S0"/>
    <form suffix="uela" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uelan" tag="MM03P0" synt="Imperative"/>
    <form suffix="uelan" tag="MSP3P0"/>
    <form suffix="uelas" tag="MSP2S0"/>
    <form suffix="uele" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uele" tag="MM02S0" synt="Imperative"/>
    <form suffix="uelen" tag="MIP3P0"/>
    <form suffix="ueles" tag="MIP2S0"/>
    <form suffix="uelo" tag="MIP1S0"/>
  </table>
  <table name="V84" rads=".*(desah|proh|ah)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: desah/proh/ah + ijar -->
    <!-- 3 members -->
    <form suffix="ijar" tag="MN0000" synt="Infinitive"/>
    <form suffix="ijaba" tag="MII1S0"/>
    <form suffix="ijaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="ijabais" tag="MII2P0"/>
    <form suffix="ijaban" tag="MII3P0"/>
    <form suffix="ijabas" tag="MII2S0"/>
    <form suffix="ijad" tag="MM02P0" synt="Imperative"/>
    <form suffix="ijada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ijadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ijado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ijados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ijamos" tag="MIP1P0"/>
    <form suffix="ijamos" tag="MIS1P0"/>
    <form suffix="ijando" tag="MG0000"/>
    <form suffix="ijara" tag="MSI1S0"/>
    <form suffix="ijara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ijarais" tag="MSI2P0"/>
    <form suffix="ijaran" tag="MSI3P0"/>
    <form suffix="ijaras" tag="MSI2S0"/>
    <form suffix="ijare" tag="MSF1S0"/>
    <form suffix="ijare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ijareis" tag="MSF2P0"/>
    <form suffix="ijaremos" tag="MIF1P0"/>
    <form suffix="ijaren" tag="MSF3P0"/>
    <form suffix="ijares" tag="MSF2S0"/>
    <form suffix="ijaron" tag="MIS3P0"/>
    <form suffix="ijará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="ijarán" tag="MIF3P0"/>
    <form suffix="ijarás" tag="MIF2S0"/>
    <form suffix="ijaré" tag="MIF1S0"/>
    <form suffix="ijaréis" tag="MIF2P0"/>
    <form suffix="ijaría" tag="MIC1S0"/>
    <form suffix="ijaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ijaríais" tag="MIC2P0"/>
    <form suffix="ijaríamos" tag="MIC1P0"/>
    <form suffix="ijarían" tag="MIC3P0"/>
    <form suffix="ijarías" tag="MIC2S0"/>
    <form suffix="ijase" tag="MSI1S0"/>
    <form suffix="ijase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ijaseis" tag="MSI2P0"/>
    <form suffix="ijasen" tag="MSI3P0"/>
    <form suffix="ijases" tag="MSI2S0"/>
    <form suffix="ijaste" tag="MIS2S0"/>
    <form suffix="ijasteis" tag="MIS2P0"/>
    <form suffix="ijemos" tag="MSP1P0"/>
    <form suffix="ijemos" tag="MM01P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ijábamos" tag="MII1P0"/>
    <form suffix="ijáis" tag="MIP2P0"/>
    <form suffix="ijáramos" tag="MSI1P0"/>
    <form suffix="ijáremos" tag="MSF1P0"/>
    <form suffix="ijásemos" tag="MSI1P0"/>
    <form suffix="ijé" tag="MIS1S0"/>
    <form suffix="ijéis" tag="MSP2P0"/>
    <form suffix="ijó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="íja" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="íja" tag="MM02S0" synt="Imperative"/>
    <form suffix="íjan" tag="MIP3P0"/>
    <form suffix="íjas" tag="MIP2S0"/>
    <form suffix="íje" tag="MSP1S0"/>
    <form suffix="íje" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="íje" tag="MM03S0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="íjen" tag="MSP3P0"/>
    <form suffix="íjen" tag="MM03P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="íjes" tag="MSP2S0"/>
    <form suffix="íjo" tag="MIP1S0"/>
  </table>
  <table name="V85" rads=".*(disc|desc|trasc)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: disc/desc/trac + ordar -->
    <!-- 3 members -->
    <form suffix="ordar" tag="MN0000" synt="Infinitive"/>
    <form suffix="ordaba" tag="MII1S0"/>
    <form suffix="ordaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="ordabais" tag="MII2P0"/>
    <form suffix="ordaban" tag="MII3P0"/>
    <form suffix="ordabas" tag="MII2S0"/>
    <form suffix="ordad" tag="MM02P0" synt="Imperative"/>
    <form suffix="ordada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ordadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ordado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ordados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ordamos" tag="MIP1P0"/>
    <form suffix="ordamos" tag="MIS1P0"/>
    <form suffix="ordando" tag="MG0000"/>
    <form suffix="ordara" tag="MSI1S0"/>
    <form suffix="ordara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ordarais" tag="MSI2P0"/>
    <form suffix="ordaran" tag="MSI3P0"/>
    <form suffix="ordaras" tag="MSI2S0"/>
    <form suffix="ordare" tag="MSF1S0"/>
    <form suffix="ordare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ordareis" tag="MSF2P0"/>
    <form suffix="ordaremos" tag="MIF1P0"/>
    <form suffix="ordaren" tag="MSF3P0"/>
    <form suffix="ordares" tag="MSF2S0"/>
    <form suffix="ordaron" tag="MIS3P0"/>
    <form suffix="ordará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="ordarán" tag="MIF3P0"/>
    <form suffix="ordarás" tag="MIF2S0"/>
    <form suffix="ordaré" tag="MIF1S0"/>
    <form suffix="ordaréis" tag="MIF2P0"/>
    <form suffix="ordaría" tag="MIC1S0"/>
    <form suffix="ordaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ordaríais" tag="MIC2P0"/>
    <form suffix="ordaríamos" tag="MIC1P0"/>
    <form suffix="ordarían" tag="MIC3P0"/>
    <form suffix="ordarías" tag="MIC2S0"/>
    <form suffix="ordase" tag="MSI1S0"/>
    <form suffix="ordase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ordaseis" tag="MSI2P0"/>
    <form suffix="ordasen" tag="MSI3P0"/>
    <form suffix="ordases" tag="MSI2S0"/>
    <form suffix="ordaste" tag="MIS2S0"/>
    <form suffix="ordasteis" tag="MIS2P0"/>
    <form suffix="ordemos" tag="MSP1P0"/>
    <form suffix="ordemos" tag="MM01P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ordábamos" tag="MII1P0"/>
    <form suffix="ordáis" tag="MIP2P0"/>
    <form suffix="ordáramos" tag="MSI1P0"/>
    <form suffix="ordáremos" tag="MSF1P0"/>
    <form suffix="ordásemos" tag="MSI1P0"/>
    <form suffix="ordé" tag="MIS1S0"/>
    <form suffix="ordéis" tag="MSP2P0"/>
    <form suffix="ordó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="uerda" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uerda" tag="MM02S0" synt="Imperative"/>
    <form suffix="uerdan" tag="MIP3P0"/>
    <form suffix="uerdas" tag="MIP2S0"/>
    <form suffix="uerde" tag="MSP1S0"/>
    <form suffix="uerde" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uerde" tag="MM03S0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="uerden" tag="MSP3P0"/>
    <form suffix="uerden" tag="MM03P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="uerdes" tag="MSP2S0"/>
    <form suffix="uerdo" tag="MIP1S0"/>
  </table>
  <table name="V86" rads=".*(c|conc|disc)" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: c/conc/disc + ernir -->
    <form suffix="ernir" tag="MN0000" synt="Infinitive"/>
    <form suffix="ernamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="ernamos" tag="MSP1P0"/>
    <form suffix="ernid" tag="MM02P0" synt="Imperative"/>
    <form suffix="ernida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ernidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ernido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ernidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="erniendo" tag="MG0000"/>
    <form suffix="erniera" tag="MSI1S0"/>
    <form suffix="erniera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ernierais" tag="MSI2P0"/>
    <form suffix="ernieran" tag="MSI3P0"/>
    <form suffix="ernieras" tag="MSI2S0"/>
    <form suffix="erniere" tag="MSF1S0"/>
    <form suffix="erniere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="erniereis" tag="MSF2P0"/>
    <form suffix="ernieren" tag="MSF3P0"/>
    <form suffix="ernieres" tag="MSF2S0"/>
    <form suffix="ernieron" tag="MIS3P0"/>
    <form suffix="erniese" tag="MSI1S0"/>
    <form suffix="erniese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ernieseis" tag="MSI2P0"/>
    <form suffix="erniesen" tag="MSI3P0"/>
    <form suffix="ernieses" tag="MSI2S0"/>
    <form suffix="ernimos" tag="MIP1P0"/>
    <form suffix="ernimos" tag="MIS1P0"/>
    <form suffix="erniremos" tag="MIF1P0"/>
    <form suffix="ernirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="ernirán" tag="MIF3P0"/>
    <form suffix="ernirás" tag="MIF2S0"/>
    <form suffix="erniré" tag="MIF1S0"/>
    <form suffix="erniréis" tag="MIF2P0"/>
    <form suffix="erniría" tag="MIC1S0"/>
    <form suffix="erniría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="erniríais" tag="MIC2P0"/>
    <form suffix="erniríamos" tag="MIC1P0"/>
    <form suffix="ernirían" tag="MIC3P0"/>
    <form suffix="ernirías" tag="MIC2S0"/>
    <form suffix="erniste" tag="MIS2S0"/>
    <form suffix="ernisteis" tag="MIS2P0"/>
    <form suffix="erniéramos" tag="MSI1P0"/>
    <form suffix="erniéremos" tag="MSF1P0"/>
    <form suffix="erniésemos" tag="MSI1P0"/>
    <form suffix="ernió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ernáis" tag="MSP2P0"/>
    <form suffix="erní" tag="MIS1S0"/>
    <form suffix="ernía" tag="MII1S0"/>
    <form suffix="ernía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="erníais" tag="MII2P0"/>
    <form suffix="erníamos" tag="MII1P0"/>
    <form suffix="ernían" tag="MII3P0"/>
    <form suffix="ernías" tag="MII2S0"/>
    <form suffix="ernís" tag="MIP2P0"/>
    <form suffix="ierna" tag="MM03S0" synt="Imperative"/>
    <form suffix="ierna" tag="MSP1S0"/>
    <form suffix="ierna" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="iernan" tag="MM03P0" synt="Imperative"/>
    <form suffix="iernan" tag="MSP3P0"/>
    <form suffix="iernas" tag="MSP2S0"/>
    <form suffix="ierne" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ierne" tag="MM02S0" synt="Imperative"/>
    <form suffix="iernen" tag="MIP3P0"/>
    <form suffix="iernes" tag="MIP2S0"/>
    <form suffix="ierno" tag="MIP1S0"/>
  </table>
  <table name="V87" rads=".*(hebr|arc|jud|enr)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: hebra/arca/juda + izar -->
    <!-- 3 members -->
    <form suffix="aizar" tag="MN0000" synt="Infinitive"/>
    <form suffix="aicemos" tag="MSP1P0"/>
    <form suffix="aicemos" tag="MM01P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="aicé" tag="MIS1S0"/>
    <form suffix="aicéis" tag="MSP2P0"/>
    <form suffix="aizaba" tag="MII1S0"/>
    <form suffix="aizaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="aizabais" tag="MII2P0"/>
    <form suffix="aizaban" tag="MII3P0"/>
    <form suffix="aizabas" tag="MII2S0"/>
    <form suffix="aizad" tag="MM02P0" synt="Imperative"/>
    <form suffix="aizada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="aizadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="aizado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="aizados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="aizamos" tag="MIP1P0"/>
    <form suffix="aizamos" tag="MIS1P0"/>
    <form suffix="aizando" tag="MG0000"/>
    <form suffix="aizara" tag="MSI1S0"/>
    <form suffix="aizara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="aizarais" tag="MSI2P0"/>
    <form suffix="aizaran" tag="MSI3P0"/>
    <form suffix="aizaras" tag="MSI2S0"/>
    <form suffix="aizare" tag="MSF1S0"/>
    <form suffix="aizare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="aizareis" tag="MSF2P0"/>
    <form suffix="aizaremos" tag="MIF1P0"/>
    <form suffix="aizaren" tag="MSF3P0"/>
    <form suffix="aizares" tag="MSF2S0"/>
    <form suffix="aizaron" tag="MIS3P0"/>
    <form suffix="aizará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="aizarán" tag="MIF3P0"/>
    <form suffix="aizarás" tag="MIF2S0"/>
    <form suffix="aizaré" tag="MIF1S0"/>
    <form suffix="aizaréis" tag="MIF2P0"/>
    <form suffix="aizaría" tag="MIC1S0"/>
    <form suffix="aizaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="aizaríais" tag="MIC2P0"/>
    <form suffix="aizaríamos" tag="MIC1P0"/>
    <form suffix="aizarían" tag="MIC3P0"/>
    <form suffix="aizarías" tag="MIC2S0"/>
    <form suffix="aizase" tag="MSI1S0"/>
    <form suffix="aizase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="aizaseis" tag="MSI2P0"/>
    <form suffix="aizasen" tag="MSI3P0"/>
    <form suffix="aizases" tag="MSI2S0"/>
    <form suffix="aizaste" tag="MIS2S0"/>
    <form suffix="aizasteis" tag="MIS2P0"/>
    <form suffix="aizábamos" tag="MII1P0"/>
    <form suffix="aizáis" tag="MIP2P0"/>
    <form suffix="aizáramos" tag="MSI1P0"/>
    <form suffix="aizáremos" tag="MSF1P0"/>
    <form suffix="aizásemos" tag="MSI1P0"/>
    <form suffix="aizó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="aíce" tag="MSP1S0"/>
    <form suffix="aíce" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="aíce" tag="MM03S0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="aícen" tag="MSP3P0"/>
    <form suffix="aícen" tag="MM03P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="aíces" tag="MSP2S0"/>
    <form suffix="aíza" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="aíza" tag="MM02S0" synt="Imperative"/>
    <form suffix="aízan" tag="MIP3P0"/>
    <form suffix="aízas" tag="MIP2S0"/>
    <form suffix="aízo" tag="MIP1S0"/>
  </table>
  <table name="V88" rads=".*(desatra|atra|tra)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: desatra/atra/tra + illar -->
    <!-- 3 members -->
    <form suffix="illar" tag="MN0000" synt="Infinitive"/>
    <form suffix="illaba" tag="MII1S0"/>
    <form suffix="illaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="illabais" tag="MII2P0"/>
    <form suffix="illaban" tag="MII3P0"/>
    <form suffix="illabas" tag="MII2S0"/>
    <form suffix="illad" tag="MM02P0" synt="Imperative"/>
    <form suffix="illada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="illadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="illado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="illados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="illamos" tag="MIP1P0"/>
    <form suffix="illamos" tag="MIS1P0"/>
    <form suffix="illando" tag="MG0000"/>
    <form suffix="illara" tag="MSI1S0"/>
    <form suffix="illara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="illarais" tag="MSI2P0"/>
    <form suffix="illaran" tag="MSI3P0"/>
    <form suffix="illaras" tag="MSI2S0"/>
    <form suffix="illare" tag="MSF1S0"/>
    <form suffix="illare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="illareis" tag="MSF2P0"/>
    <form suffix="illaremos" tag="MIF1P0"/>
    <form suffix="illaren" tag="MSF3P0"/>
    <form suffix="illares" tag="MSF2S0"/>
    <form suffix="illaron" tag="MIS3P0"/>
    <form suffix="illará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="illarán" tag="MIF3P0"/>
    <form suffix="illarás" tag="MIF2S0"/>
    <form suffix="illaré" tag="MIF1S0"/>
    <form suffix="illaréis" tag="MIF2P0"/>
    <form suffix="illaría" tag="MIC1S0"/>
    <form suffix="illaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="illaríais" tag="MIC2P0"/>
    <form suffix="illaríamos" tag="MIC1P0"/>
    <form suffix="illarían" tag="MIC3P0"/>
    <form suffix="illarías" tag="MIC2S0"/>
    <form suffix="illase" tag="MSI1S0"/>
    <form suffix="illase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="illaseis" tag="MSI2P0"/>
    <form suffix="illasen" tag="MSI3P0"/>
    <form suffix="illases" tag="MSI2S0"/>
    <form suffix="illaste" tag="MIS2S0"/>
    <form suffix="illasteis" tag="MIS2P0"/>
    <form suffix="illemos" tag="MSP1P0"/>
    <form suffix="illemos" tag="MM01P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="illábamos" tag="MII1P0"/>
    <form suffix="illáis" tag="MIP2P0"/>
    <form suffix="illáramos" tag="MSI1P0"/>
    <form suffix="illáremos" tag="MSF1P0"/>
    <form suffix="illásemos" tag="MSI1P0"/>
    <form suffix="illé" tag="MIS1S0"/>
    <form suffix="illéis" tag="MSP2P0"/>
    <form suffix="illó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ílla" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ílla" tag="MM02S0" synt="Imperative"/>
    <form suffix="íllan" tag="MIP3P0"/>
    <form suffix="íllas" tag="MIP2S0"/>
    <form suffix="ílle" tag="MSP1S0"/>
    <form suffix="ílle" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ílle" tag="MM03S0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="íllen" tag="MSP3P0"/>
    <form suffix="íllen" tag="MM03P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ílles" tag="MSP2S0"/>
    <form suffix="íllo" tag="MIP1S0"/>

  </table>
  <table name="V89" rads=".*(bienqu|malqu|desqu)" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: bienqu/malqu/desqu + erer --> 
    <!-- 3 members -->
    <form suffix="erer" tag="MN0000" synt="Infinitive"/>
    <form suffix="eramos" tag="MSP1P0"/>
    <form suffix="eramos" tag="MM01P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ered" tag="MM02P0" synt="Imperative"/>
    <form suffix="ered" tag="MM0P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="eremos" tag="MIP1P0"/>
    <form suffix="erida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="eridas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="erido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="eridos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="eriendo" tag="MG0000"/>
    <form suffix="erremos" tag="MIF1P0"/>
    <form suffix="errá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="errán" tag="MIF3P0"/>
    <form suffix="errás" tag="MIF2S0"/>
    <form suffix="erré" tag="MIF1S0"/>
    <form suffix="erréis" tag="MIF2P0"/>
    <form suffix="erría" tag="MIC1S0"/>
    <form suffix="erría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="erríais" tag="MIC2P0"/>
    <form suffix="erríamos" tag="MIC1P0"/>
    <form suffix="errían" tag="MIC3P0"/>
    <form suffix="errías" tag="MIC2S0"/>
    <form suffix="eráis" tag="MSP2P0"/>
    <form suffix="eréis" tag="MIP2P0"/>
    <form suffix="ería" tag="MII1S0"/>
    <form suffix="ería" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="eríais" tag="MII2P0"/>
    <form suffix="eríamos" tag="MII1P0"/>
    <form suffix="erían" tag="MII3P0"/>
    <form suffix="erías" tag="MII2S0"/>
    <form suffix="iera" tag="MSP1S0"/>
    <form suffix="iera" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="iera" tag="MM03S0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ieran" tag="MSP3P0"/>
    <form suffix="ieran" tag="MM03P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ieras" tag="MSP2S0"/>
    <form suffix="iere" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="iere" tag="MM02S0" synt="Imperative"/>
    <form suffix="ieren" tag="MIP3P0"/>
    <form suffix="ieres" tag="MIP2S0"/>
    <form suffix="iero" tag="MIP1S0"/>
    <form suffix="ise" tag="MIS1S0"/>
    <form suffix="isiera" tag="MSI1S0"/>
    <form suffix="isiera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="isierais" tag="MSI2P0"/>
    <form suffix="isieran" tag="MSI3P0"/>
    <form suffix="isieras" tag="MSI2S0"/>
    <form suffix="isiere" tag="MSF1S0"/>
    <form suffix="isiere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="isiereis" tag="MSF2P0"/>
    <form suffix="isieren" tag="MSF3P0"/>
    <form suffix="isieres" tag="MSF2S0"/>
    <form suffix="isieron" tag="MIS3P0"/>
    <form suffix="isiese" tag="MSI1S0"/>
    <form suffix="isiese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="isieseis" tag="MSI2P0"/>
    <form suffix="isiesen" tag="MSI3P0"/>
    <form suffix="isieses" tag="MSI2S0"/>
    <form suffix="isimos" tag="MIS1P0"/>
    <form suffix="isiste" tag="MIS2S0"/>
    <form suffix="isisteis" tag="MIS2P0"/>
    <form suffix="isiéramos" tag="MSI1P0"/>
    <form suffix="isiéremos" tag="MSF1P0"/>
    <form suffix="isiésemos" tag="MSI1P0"/>
    <form suffix="iso" tag="MIS3S0" synt="ThirdSing"/>
  </table>
  <table name="V90" rads=".*(esc|rec|c)" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: esc/rec/c + ocer -->
    <!-- 3 members -->
    <form suffix="ocer" tag="MN0000" synt="Infinitive"/>
    <form suffix="oced" tag="MM02P0" synt="Imperative"/>
    <form suffix="ocemos" tag="MIP1P0"/>
    <form suffix="oceremos" tag="MIF1P0"/>
    <form suffix="ocerá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="ocerán" tag="MIF3P0"/>
    <form suffix="ocerás" tag="MIF2S0"/>
    <form suffix="oceré" tag="MIF1S0"/>
    <form suffix="oceréis" tag="MIF2P0"/>
    <form suffix="ocería" tag="MIC1S0"/>
    <form suffix="ocería" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="oceríais" tag="MIC2P0"/>
    <form suffix="oceríamos" tag="MIC1P0"/>
    <form suffix="ocerían" tag="MIC3P0"/>
    <form suffix="ocerías" tag="MIC2S0"/>
    <form suffix="ocida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ocidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ocido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ocidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ociendo" tag="MG0000"/>
    <form suffix="ociera" tag="MSI1S0"/>
    <form suffix="ociera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ocierais" tag="MSI2P0"/>
    <form suffix="ocieran" tag="MSI3P0"/>
    <form suffix="ocieras" tag="MSI2S0"/>
    <form suffix="ociere" tag="MSF1S0"/>
    <form suffix="ociere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ociereis" tag="MSF2P0"/>
    <form suffix="ocieren" tag="MSF3P0"/>
    <form suffix="ocieres" tag="MSF2S0"/>
    <form suffix="ocieron" tag="MIS3P0"/>
    <form suffix="ociese" tag="MSI1S0"/>
    <form suffix="ociese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ocieseis" tag="MSI2P0"/>
    <form suffix="ociesen" tag="MSI3P0"/>
    <form suffix="ocieses" tag="MSI2S0"/>
    <form suffix="ocimos" tag="MIS1P0"/>
    <form suffix="ociste" tag="MIS2S0"/>
    <form suffix="ocisteis" tag="MIS2P0"/>
    <form suffix="ociéramos" tag="MSI1P0"/>
    <form suffix="ociéremos" tag="MSF1P0"/>
    <form suffix="ociésemos" tag="MSI1P0"/>
    <form suffix="oció" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="océis" tag="MIP2P0"/>
    <form suffix="ocí" tag="MIS1S0"/>
    <form suffix="ocía" tag="MII1S0"/>
    <form suffix="ocía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="ocíais" tag="MII2P0"/>
    <form suffix="ocíamos" tag="MII1P0"/>
    <form suffix="ocían" tag="MII3P0"/>
    <form suffix="ocías" tag="MII2S0"/>
    <form suffix="ozamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="ozamos" tag="MSP1P0"/>
    <form suffix="ozáis" tag="MSP2P0"/>
    <form suffix="uece" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uece" tag="MM02S0" synt="Imperative"/>
    <form suffix="uecen" tag="MIP3P0"/>
    <form suffix="ueces" tag="MIP2S0"/>
    <form suffix="ueza" tag="MM03S0" synt="Imperative"/>
    <form suffix="ueza" tag="MSP1S0"/>
    <form suffix="ueza" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uezan" tag="MM03P0" synt="Imperative"/>
    <form suffix="uezan" tag="MSP3P0"/>
    <form suffix="uezas" tag="MSP2S0"/>
    <form suffix="uezo" tag="MIP1S0"/>
  </table>
  <!--table name="V91" rads=".*(cellisque|relampague|respect)" fast="-"-->
  <table name="V91" rads=".*(cellisque)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: cellisque + ar -->
    <!-- 3 members -->
    <form suffix="ar" tag="MN0000" synt="Infinitive"/>
    <form suffix="a" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="aba" tag="MII1S0"/>
    <form suffix="aba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="abais" tag="MII2P0"/>
    <form suffix="aban" tag="MII3P0"/>
    <form suffix="abas" tag="MII2S0"/>
    <form suffix="ada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="adas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="an" tag="MIP3P0"/>
    <form suffix="ando" tag="MG0000"/>
    <form suffix="ara" tag="MSI1S0"/>
    <form suffix="ara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="arais" tag="MSI2P0"/>
    <form suffix="aran" tag="MSI3P0"/>
    <form suffix="aras" tag="MSI2S0"/>
    <form suffix="are" tag="MSF1S0"/>
    <form suffix="are" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="areis" tag="MSF2P0"/>
    <form suffix="aremos" tag="MIF1P0"/>
    <form suffix="aren" tag="MSF3P0"/>
    <form suffix="ares" tag="MSF2S0"/>
    <form suffix="ará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="arán" tag="MIF3P0"/>
    <form suffix="arás" tag="MIF2S0"/>
    <form suffix="aré" tag="MIF1S0"/>
    <form suffix="aréis" tag="MIF2P0"/>
    <form suffix="aría" tag="MIC1S0"/>
    <form suffix="aría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="aríais" tag="MIC2P0"/>
    <form suffix="aríamos" tag="MIC1P0"/>
    <form suffix="arían" tag="MIC3P0"/>
    <form suffix="arías" tag="MIC2S0"/>
    <form suffix="as" tag="MIP2S0"/>
    <form suffix="ase" tag="MSI1S0"/>
    <form suffix="ase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="aseis" tag="MSI2P0"/>
    <form suffix="asen" tag="MSI3P0"/>
    <form suffix="ases" tag="MSI2S0"/>
    <form suffix="e" tag="MSP1S0"/>
    <form suffix="e" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="en" tag="MSP3P0"/>
    <form suffix="es" tag="MSP2S0"/>
    <form suffix="ábamos" tag="MII1P0"/>
    <form suffix="áramos" tag="MSI1P0"/>
    <form suffix="áremos" tag="MSF1P0"/>
    <form suffix="ásemos" tag="MSI1P0"/>
    <form suffix="ó" tag="MIS3S0" synt="ThirdSing"/>
  </table>
  <table name="V92" rads=".*(desc|c|rec)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: desc/c/rec + ontar -->
    <!-- 3 members -->
    <form suffix="ontar" tag="MN0000" synt="Infinitive"/>
    <form suffix="ontaba" tag="MII1S0"/>
    <form suffix="ontaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="ontabais" tag="MII2P0"/>
    <form suffix="ontaban" tag="MII3P0"/>
    <form suffix="ontabas" tag="MII2S0"/>
    <form suffix="ontad" tag="MM02P0" synt="Imperative"/>
    <form suffix="ontada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ontadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ontado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ontados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ontamos" tag="MIP1P0"/>
    <form suffix="ontamos" tag="MIS1P0"/>
    <form suffix="ontando" tag="MG0000"/>
    <form suffix="ontara" tag="MSI1S0"/>
    <form suffix="ontara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ontarais" tag="MSI2P0"/>
    <form suffix="ontaran" tag="MSI3P0"/>
    <form suffix="ontaras" tag="MSI2S0"/>
    <form suffix="ontare" tag="MSF1S0"/>
    <form suffix="ontare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ontareis" tag="MSF2P0"/>
    <form suffix="ontaremos" tag="MIF1P0"/>
    <form suffix="ontaren" tag="MSF3P0"/>
    <form suffix="ontares" tag="MSF2S0"/>
    <form suffix="ontaron" tag="MIS3P0"/>
    <form suffix="ontará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="ontarán" tag="MIF3P0"/>
    <form suffix="ontarás" tag="MIF2S0"/>
    <form suffix="ontaré" tag="MIF1S0"/>
    <form suffix="ontaréis" tag="MIF2P0"/>
    <form suffix="ontaría" tag="MIC1S0"/>
    <form suffix="ontaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ontaríais" tag="MIC2P0"/>
    <form suffix="ontaríamos" tag="MIC1P0"/>
    <form suffix="ontarían" tag="MIC3P0"/>
    <form suffix="ontarías" tag="MIC2S0"/>
    <form suffix="ontase" tag="MSI1S0"/>
    <form suffix="ontase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ontaseis" tag="MSI2P0"/>
    <form suffix="ontasen" tag="MSI3P0"/>
    <form suffix="ontases" tag="MSI2S0"/>
    <form suffix="ontaste" tag="MIS2S0"/>
    <form suffix="ontasteis" tag="MIS2P0"/>
    <form suffix="ontemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="ontemos" tag="MSP1P0"/>
    <form suffix="ontábamos" tag="MII1P0"/>
    <form suffix="ontáis" tag="MIP2P0"/>
    <form suffix="ontáramos" tag="MSI1P0"/>
    <form suffix="ontáremos" tag="MSF1P0"/>
    <form suffix="ontásemos" tag="MSI1P0"/>
    <form suffix="onté" tag="MIS1S0"/>
    <form suffix="ontéis" tag="MSP2P0"/>
    <form suffix="ontó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="uenta" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uenta" tag="MM02S0" synt="Imperative"/>
    <form suffix="uentan" tag="MIP3P0"/>
    <form suffix="uentas" tag="MIP2S0"/>
    <form suffix="uente" tag="MM03S0" synt="Imperative"/>
    <form suffix="uente" tag="MSP1S0"/>
    <form suffix="uente" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uenten" tag="MM03P0" synt="Imperative"/>
    <form suffix="uenten" tag="MSP3P0"/>
    <form suffix="uentes" tag="MSP2S0"/>
    <form suffix="uento" tag="MIP1S0"/>
  </table>
  <table name="V93" rads=".*(contrah|desh|h|desf|satisf)" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: contrah/desh/h + acer -->
    <!-- 3 members -->
    <form suffix="acer" tag="MN0000" synt="Infinitive"/>
    <form suffix="ace" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="aced" tag="MM02P0" synt="Imperative"/>
    <form suffix="acemos" tag="MIP1P0"/>
    <form suffix="acen" tag="MIP3P0"/>
    <form suffix="aces" tag="MIP2S0"/>
    <form suffix="aciendo" tag="MG0000"/>
    <form suffix="acéis" tag="MIP2P0"/>
    <form suffix="acía" tag="MII1S0"/>
    <form suffix="acía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="acíais" tag="MII2P0"/>
    <form suffix="acíamos" tag="MII1P0"/>
    <form suffix="acían" tag="MII3P0"/>
    <form suffix="acías" tag="MII2S0"/>
    <form suffix="aga" tag="MM03S0" synt="Imperative"/>
    <form suffix="aga" tag="MSP1S0"/>
    <form suffix="aga" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="agamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="agamos" tag="MSP1P0"/>
    <form suffix="agan" tag="MM03P0" synt="Imperative"/>
    <form suffix="agan" tag="MSP3P0"/>
    <form suffix="agas" tag="MSP2S0"/>
    <form suffix="ago" tag="MIP1S0"/>
    <form suffix="agáis" tag="MSP2P0"/>
    <form suffix="aremos" tag="MIF1P0"/>
    <form suffix="ará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="arán" tag="MIF3P0"/>
    <form suffix="arás" tag="MIF2S0"/>
    <form suffix="aré" tag="MIF1S0"/>
    <form suffix="aréis" tag="MIF2P0"/>
    <form suffix="aría" tag="MIC1S0"/>
    <form suffix="aría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="aríais" tag="MIC2P0"/>
    <form suffix="aríamos" tag="MIC1P0"/>
    <form suffix="arían" tag="MIC3P0"/>
    <form suffix="arías" tag="MIC2S0"/>
    <form suffix="az" tag="MM02S0" synt="Imperative"/>
    <form suffix="echa" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="echas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="echo" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="echos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ice" tag="MIS1S0"/>
    <form suffix="iciera" tag="MSI1S0"/>
    <form suffix="iciera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="icierais" tag="MSI2P0"/>
    <form suffix="icieran" tag="MSI3P0"/>
    <form suffix="icieras" tag="MSI2S0"/>
    <form suffix="iciere" tag="MSF1S0"/>
    <form suffix="iciere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="iciereis" tag="MSF2P0"/>
    <form suffix="icieren" tag="MSF3P0"/>
    <form suffix="icieres" tag="MSF2S0"/>
    <form suffix="icieron" tag="MIS3P0"/>
    <form suffix="iciese" tag="MSI1S0"/>
    <form suffix="iciese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="icieseis" tag="MSI2P0"/>
    <form suffix="iciesen" tag="MSI3P0"/>
    <form suffix="icieses" tag="MSI2S0"/>
    <form suffix="icimos" tag="MIS1P0"/>
    <form suffix="iciste" tag="MIS2S0"/>
    <form suffix="icisteis" tag="MIS2P0"/>
    <form suffix="iciéramos" tag="MSI1P0"/>
    <form suffix="iciéremos" tag="MSF1P0"/>
    <form suffix="iciésemos" tag="MSI1P0"/>
    <form suffix="izo" tag="MIS3S0" synt="ThirdSing"/>
  </table>
  <table name="V94" rads=".*(desc|c|h)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: desc/c/h + olgar -->
    <!-- 3 members -->
    <form suffix="olgar" tag="MN0000" synt="Infinitive"/>
    <form suffix="olgaba" tag="MII1S0"/>
    <form suffix="olgaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="olgabais" tag="MII2P0"/>
    <form suffix="olgaban" tag="MII3P0"/>
    <form suffix="olgabas" tag="MII2S0"/>
    <form suffix="olgad" tag="MM02P0" synt="Imperative"/>
    <form suffix="olgada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="olgadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="olgado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="olgados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="olgamos" tag="MIP1P0"/>
    <form suffix="olgamos" tag="MIS1P0"/>
    <form suffix="olgando" tag="MG0000"/>
    <form suffix="olgara" tag="MSI1S0"/>
    <form suffix="olgara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="olgarais" tag="MSI2P0"/>
    <form suffix="olgaran" tag="MSI3P0"/>
    <form suffix="olgaras" tag="MSI2S0"/>
    <form suffix="olgare" tag="MSF1S0"/>
    <form suffix="olgare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="olgareis" tag="MSF2P0"/>
    <form suffix="olgaremos" tag="MIF1P0"/>
    <form suffix="olgaren" tag="MSF3P0"/>
    <form suffix="olgares" tag="MSF2S0"/>
    <form suffix="olgaron" tag="MIS3P0"/>
    <form suffix="olgará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="olgarán" tag="MIF3P0"/>
    <form suffix="olgarás" tag="MIF2S0"/>
    <form suffix="olgaré" tag="MIF1S0"/>
    <form suffix="olgaréis" tag="MIF2P0"/>
    <form suffix="olgaría" tag="MIC1S0"/>
    <form suffix="olgaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="olgaríais" tag="MIC2P0"/>
    <form suffix="olgaríamos" tag="MIC1P0"/>
    <form suffix="olgarían" tag="MIC3P0"/>
    <form suffix="olgarías" tag="MIC2S0"/>
    <form suffix="olgase" tag="MSI1S0"/>
    <form suffix="olgase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="olgaseis" tag="MSI2P0"/>
    <form suffix="olgasen" tag="MSI3P0"/>
    <form suffix="olgases" tag="MSI2S0"/>
    <form suffix="olgaste" tag="MIS2S0"/>
    <form suffix="olgasteis" tag="MIS2P0"/>
    <form suffix="olguemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="olguemos" tag="MSP1P0"/>
    <form suffix="olgué" tag="MIS1S0"/>
    <form suffix="olguéis" tag="MSP2P0"/>
    <form suffix="olgábamos" tag="MII1P0"/>
    <form suffix="olgáis" tag="MIP2P0"/>
    <form suffix="olgáramos" tag="MSI1P0"/>
    <form suffix="olgáremos" tag="MSF1P0"/>
    <form suffix="olgásemos" tag="MSI1P0"/>
    <form suffix="olgó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="uelga" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uelga" tag="MM02S0" synt="Imperative"/>
    <form suffix="uelgan" tag="MIP3P0"/>
    <form suffix="uelgas" tag="MIP2S0"/>
    <form suffix="uelgo" tag="MIP1S0"/>
    <form suffix="uelgue" tag="MM03S0" synt="Imperative"/>
    <form suffix="uelgue" tag="MSP1S0"/>
    <form suffix="uelgue" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uelguen" tag="MM03P0" synt="Imperative"/>
    <form suffix="uelguen" tag="MSP3P0"/>
    <form suffix="uelgues" tag="MSP2S0"/>
  </table>
  <table name="V95" rads=".*(deca|reca|ca)" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: deca/reca/ca + er -->
    <!-- 3 members -->
    <form suffix="er" tag="MN0000" synt="Infinitive"/>
    <form suffix="e" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="e" tag="MM02S0" synt="Imperative"/>
    <form suffix="ed" tag="MM02P0" synt="Imperative"/>
    <form suffix="emos" tag="MIP1P0"/>
    <form suffix="en" tag="MIP3P0"/>
    <form suffix="eremos" tag="MIF1P0"/>
    <form suffix="erá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="erán" tag="MIF3P0"/>
    <form suffix="erás" tag="MIF2S0"/>
    <form suffix="eré" tag="MIF1S0"/>
    <form suffix="eréis" tag="MIF2P0"/>
    <form suffix="ería" tag="MIC1S0"/>
    <form suffix="ería" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="eríais" tag="MIC2P0"/>
    <form suffix="eríamos" tag="MIC1P0"/>
    <form suffix="erían" tag="MIC3P0"/>
    <form suffix="erías" tag="MIC2S0"/>
    <form suffix="es" tag="MIP2S0"/>
    <form suffix="iga" tag="MM03S0" synt="Imperative"/>
    <form suffix="iga" tag="MSP1S0"/>
    <form suffix="iga" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="igamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="igamos" tag="MSP1P0"/>
    <form suffix="igan" tag="MM03P0" synt="Imperative"/>
    <form suffix="igan" tag="MSP3P0"/>
    <form suffix="igas" tag="MSP2S0"/>
    <form suffix="igo" tag="MIP1S0"/>
    <form suffix="igáis" tag="MSP2P0"/>
    <form suffix="yendo" tag="MG0000"/>
    <form suffix="yera" tag="MSI1S0"/>
    <form suffix="yera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="yerais" tag="MSI2P0"/>
    <form suffix="yeran" tag="MSI3P0"/>
    <form suffix="yeras" tag="MSI2S0"/>
    <form suffix="yere" tag="MSF1S0"/>
    <form suffix="yere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="yereis" tag="MSF2P0"/>
    <form suffix="yeren" tag="MSF3P0"/>
    <form suffix="yeres" tag="MSF2S0"/>
    <form suffix="yeron" tag="MIS3P0"/>
    <form suffix="yese" tag="MSI1S0"/>
    <form suffix="yese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="yeseis" tag="MSI2P0"/>
    <form suffix="yesen" tag="MSI3P0"/>
    <form suffix="yeses" tag="MSI2S0"/>
    <form suffix="yéramos" tag="MSI1P0"/>
    <form suffix="yéremos" tag="MSF1P0"/>
    <form suffix="yésemos" tag="MSI1P0"/>
    <form suffix="yó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="éis" tag="MIP2P0"/>
    <form suffix="í" tag="MIS1S0"/>
    <form suffix="ía" tag="MII1S0"/>
    <form suffix="ía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="íais" tag="MII2P0"/>
    <form suffix="íamos" tag="MII1P0"/>
    <form suffix="ían" tag="MII3P0"/>
    <form suffix="ías" tag="MII2S0"/>
    <form suffix="ída" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ídas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ído" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ídos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ímos" tag="MIS1P0"/>
    <form suffix="íste" tag="MIS2S0"/>
    <form suffix="ísteis" tag="MIS2P0"/>
  </table>
  <table name="V97" rads=".*(rep|derr|comp)" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: rep/derr/comp + etir -->
    <!-- 3 members -->
    <form suffix="etir" tag="MN0000" synt="Infinitive"/>
    <form suffix="etid" tag="MM02P0" synt="Imperative"/>
    <form suffix="etida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="etidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="etido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="etidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="etimos" tag="MIP1P0"/>
    <form suffix="etimos" tag="MIS1P0"/>
    <form suffix="etiremos" tag="MIF1P0"/>
    <form suffix="etirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="etirán" tag="MIF3P0"/>
    <form suffix="etirás" tag="MIF2S0"/>
    <form suffix="etiré" tag="MIF1S0"/>
    <form suffix="etiréis" tag="MIF2P0"/>
    <form suffix="etiría" tag="MIC1S0"/>
    <form suffix="etiría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="etiríais" tag="MIC2P0"/>
    <form suffix="etiríamos" tag="MIC1P0"/>
    <form suffix="etirían" tag="MIC3P0"/>
    <form suffix="etirías" tag="MIC2S0"/>
    <form suffix="etiste" tag="MIS2S0"/>
    <form suffix="etisteis" tag="MIS2P0"/>
    <form suffix="etí" tag="MIS1S0"/>
    <form suffix="etía" tag="MII1S0"/>
    <form suffix="etía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="etíais" tag="MII2P0"/>
    <form suffix="etíamos" tag="MII1P0"/>
    <form suffix="etían" tag="MII3P0"/>
    <form suffix="etías" tag="MII2S0"/>
    <form suffix="etís" tag="MIP2P0"/>
    <form suffix="ita" tag="MM03S0" synt="Imperative"/>
    <form suffix="ita" tag="MSP1S0"/>
    <form suffix="ita" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="itamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="itamos" tag="MSP1P0"/>
    <form suffix="itan" tag="MM03P0" synt="Imperative"/>
    <form suffix="itan" tag="MSP3P0"/>
    <form suffix="itas" tag="MSP2S0"/>
    <form suffix="ite" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ite" tag="MM02S0" synt="Imperative"/>
    <form suffix="iten" tag="MIP3P0"/>
    <form suffix="ites" tag="MIP2S0"/>
    <form suffix="itiendo" tag="MG0000"/>
    <form suffix="itiera" tag="MSI1S0"/>
    <form suffix="itiera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="itierais" tag="MSI2P0"/>
    <form suffix="itieran" tag="MSI3P0"/>
    <form suffix="itieras" tag="MSI2S0"/>
    <form suffix="itiere" tag="MSF1S0"/>
    <form suffix="itiere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="itiereis" tag="MSF2P0"/>
    <form suffix="itieren" tag="MSF3P0"/>
    <form suffix="itieres" tag="MSF2S0"/>
    <form suffix="itieron" tag="MIS3P0"/>
    <form suffix="itiese" tag="MSI1S0"/>
    <form suffix="itiese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="itieseis" tag="MSI2P0"/>
    <form suffix="itiesen" tag="MSI3P0"/>
    <form suffix="itieses" tag="MSI2S0"/>
    <form suffix="itiéramos" tag="MSI1P0"/>
    <form suffix="itiéremos" tag="MSF1P0"/>
    <form suffix="itiésemos" tag="MSI1P0"/>
    <form suffix="itió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ito" tag="MIP1S0"/>
    <form suffix="itáis" tag="MSP2P0"/>
  </table>
  <table name="V98" rads=".*(s|res|tr)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: s/res/tr + onar -->
    <!-- 3 members -->
    <form suffix="onar" tag="MN0000" synt="Infinitive"/>
    <form suffix="onaba" tag="MII1S0"/>
    <form suffix="onaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="onabais" tag="MII2P0"/>
    <form suffix="onaban" tag="MII3P0"/>
    <form suffix="onabas" tag="MII2S0"/>
    <form suffix="onad" tag="MM02P0" synt="Imperative"/>
    <form suffix="onada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="onadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="onado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="onados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="onamos" tag="MIP1P0"/>
    <form suffix="onamos" tag="MIS1P0"/>
    <form suffix="onando" tag="MG0000"/>
    <form suffix="onara" tag="MSI1S0"/>
    <form suffix="onara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="onarais" tag="MSI2P0"/>
    <form suffix="onaran" tag="MSI3P0"/>
    <form suffix="onaras" tag="MSI2S0"/>
    <form suffix="onare" tag="MSF1S0"/>
    <form suffix="onare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="onareis" tag="MSF2P0"/>
    <form suffix="onaremos" tag="MIF1P0"/>
    <form suffix="onaren" tag="MSF3P0"/>
    <form suffix="onares" tag="MSF2S0"/>
    <form suffix="onaron" tag="MIS3P0"/>
    <form suffix="onará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="onarán" tag="MIF3P0"/>
    <form suffix="onarás" tag="MIF2S0"/>
    <form suffix="onaré" tag="MIF1S0"/>
    <form suffix="onaréis" tag="MIF2P0"/>
    <form suffix="onaría" tag="MIC1S0"/>
    <form suffix="onaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="onaríais" tag="MIC2P0"/>
    <form suffix="onaríamos" tag="MIC1P0"/>
    <form suffix="onarían" tag="MIC3P0"/>
    <form suffix="onarías" tag="MIC2S0"/>
    <form suffix="onase" tag="MSI1S0"/>
    <form suffix="onase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="onaseis" tag="MSI2P0"/>
    <form suffix="onasen" tag="MSI3P0"/>
    <form suffix="onases" tag="MSI2S0"/>
    <form suffix="onaste" tag="MIS2S0"/>
    <form suffix="onasteis" tag="MIS2P0"/>
    <form suffix="onemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="onemos" tag="MSP1P0"/>
    <form suffix="onábamos" tag="MII1P0"/>
    <form suffix="onáis" tag="MIP2P0"/>
    <form suffix="onáramos" tag="MSI1P0"/>
    <form suffix="onáremos" tag="MSF1P0"/>
    <form suffix="onásemos" tag="MSI1P0"/>
    <form suffix="oné" tag="MIS1S0"/>
    <form suffix="onéis" tag="MSP2P0"/>
    <form suffix="onó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="uena" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uena" tag="MM02S0" synt="Imperative"/>
    <form suffix="uenan" tag="MIP3P0"/>
    <form suffix="uenas" tag="MIP2S0"/>
    <form suffix="uene" tag="MM03S0" synt="Imperative"/>
    <form suffix="uene" tag="MSP1S0"/>
    <form suffix="uene" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uenen" tag="MM03P0" synt="Imperative"/>
    <form suffix="uenen" tag="MSP3P0"/>
    <form suffix="uenes" tag="MSP2S0"/>
    <form suffix="ueno" tag="MIP1S0"/>
  </table>
  <table name="V100" rads=".*(r|desl|sonr|engr)" fast="-"> <!-- Nieves: verbos tercera conjugación en infinitivo: r/desl/sonr + eír -->
    <!-- 3 members -->
    <form suffix="eír" tag="MN0000" synt="Infinitive"/>
    <form suffix="eiremos" tag="MIF1P0"/>
    <form suffix="eirá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="eirán" tag="MIF3P0"/>
    <form suffix="eirás" tag="MIF2S0"/>
    <form suffix="eiré" tag="MIF1S0"/>
    <form suffix="eiréis" tag="MIF2P0"/>
    <form suffix="eiría" tag="MIC1S0"/>
    <form suffix="eiría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="eiríais" tag="MIC2P0"/>
    <form suffix="eiríamos" tag="MIC1P0"/>
    <form suffix="eirían" tag="MIC3P0"/>
    <form suffix="eirías" tag="MIC2S0"/>
    <form suffix="eí" tag="MIS1S0"/>
    <form suffix="eía" tag="MII1S0"/>
    <form suffix="eía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="eíais" tag="MII2P0"/>
    <form suffix="eíamos" tag="MII1P0"/>
    <form suffix="eían" tag="MII3P0"/>
    <form suffix="eías" tag="MII2S0"/>
    <form suffix="eíd" tag="MM02P0" synt="Imperative"/>
    <form suffix="eída" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="eídas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="eído" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="eídos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="eímos" tag="MIP1P0"/>
    <form suffix="eímos" tag="MIS1P0"/>
    <form suffix="eís" tag="MIP2P0"/>
    <form suffix="eíste" tag="MIS2S0"/>
    <form suffix="eísteis" tag="MIS2P0"/>
    <form suffix="iamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="iamos" tag="MSP1P0"/>
    <form suffix="iendo" tag="MG0000"/>
    <form suffix="iera" tag="MSI1S0"/>
    <form suffix="iera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ierais" tag="MSI2P0"/>
    <form suffix="ieran" tag="MSI3P0"/>
    <form suffix="ieras" tag="MSI2S0"/>
    <form suffix="iere" tag="MSF1S0"/>
    <form suffix="iere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="iereis" tag="MSF2P0"/>
    <form suffix="ieren" tag="MSF3P0"/>
    <form suffix="ieres" tag="MSF2S0"/>
    <form suffix="ieron" tag="MIS3P0"/>
    <form suffix="iese" tag="MSI1S0"/>
    <form suffix="iese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ieseis" tag="MSI2P0"/>
    <form suffix="iesen" tag="MSI3P0"/>
    <form suffix="ieses" tag="MSI2S0"/>
    <form suffix="iáis" tag="MSP2P0"/>
    <form suffix="iéramos" tag="MSI1P0"/>
    <form suffix="iéremos" tag="MSF1P0"/>
    <form suffix="iésemos" tag="MSI1P0"/>
    <form suffix="ió" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="ía" tag="MM03S0" synt="Imperative"/>
    <form suffix="ía" tag="MSP1S0"/>
    <form suffix="ía" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="ían" tag="MM03P0" synt="Imperative"/>
    <form suffix="ían" tag="MSP3P0"/>
    <form suffix="ías" tag="MSP2S0"/>
    <form suffix="íe" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="íe" tag="MM02S0" synt="Imperative"/>
    <form suffix="íen" tag="MIP3P0"/>
    <form suffix="íes" tag="MIP2S0"/>
    <form suffix="ío" tag="MIP1S0"/>
  </table>
  <table name="V102" rads=".*(lic|acens|adec)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: lic/acens/adec + uar -->
    <!-- 3 members -->
    <form suffix="uar" tag="MN0000" synt="Infinitive"/>
    <form suffix="ua" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="ua" tag="MM02S0" synt="Imperative"/>
    <form suffix="uaba" tag="MII1S0"/>
    <form suffix="uaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="uabais" tag="MII2P0"/>
    <form suffix="uaban" tag="MII3P0"/>
    <form suffix="uabas" tag="MII2S0"/>
    <form suffix="uad" tag="MM02P0" synt="Imperative"/>
    <form suffix="uada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="uadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="uado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="uados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="uamos" tag="MIP1P0"/>
    <form suffix="uamos" tag="MIS1P0"/>
    <form suffix="uan" tag="MIP3P0"/>
    <form suffix="uando" tag="MG0000"/>
    <form suffix="uara" tag="MSI1S0"/>
    <form suffix="uara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="uarais" tag="MSI2P0"/>
    <form suffix="uaran" tag="MSI3P0"/>
    <form suffix="uaras" tag="MSI2S0"/>
    <form suffix="uare" tag="MSF1S0"/>
    <form suffix="uare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="uareis" tag="MSF2P0"/>
    <form suffix="uaremos" tag="MIF1P0"/>
    <form suffix="uaren" tag="MSF3P0"/>
    <form suffix="uares" tag="MSF2S0"/>
    <form suffix="uaron" tag="MIS3P0"/>
    <form suffix="uará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="uarán" tag="MIF3P0"/>
    <form suffix="uarás" tag="MIF2S0"/>
    <form suffix="uaré" tag="MIF1S0"/>
    <form suffix="uaréis" tag="MIF2P0"/>
    <form suffix="uaría" tag="MIC1S0"/>
    <form suffix="uaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="uaríais" tag="MIC2P0"/>
    <form suffix="uaríamos" tag="MIC1P0"/>
    <form suffix="uarían" tag="MIC3P0"/>
    <form suffix="uarías" tag="MIC2S0"/>
    <form suffix="uas" tag="MIP2S0"/>
    <form suffix="uase" tag="MSI1S0"/>
    <form suffix="uase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="uaseis" tag="MSI2P0"/>
    <form suffix="uasen" tag="MSI3P0"/>
    <form suffix="uases" tag="MSI2S0"/>
    <form suffix="uaste" tag="MIS2S0"/>
    <form suffix="uasteis" tag="MIS2P0"/>
    <form suffix="ue" tag="MSP1S0"/>
    <form suffix="ue" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uemos" tag="MM01P0" synt="Imperative"/>
    <form suffix="uemos" tag="MSP1P0"/>
    <form suffix="uen" tag="MSP3P0"/>
    <form suffix="ues" tag="MSP2S0"/>
    <form suffix="uo" tag="MIP1S0"/>
    <form suffix="uábamos" tag="MII1P0"/>
    <form suffix="uáis" tag="MIP2P0"/>
    <form suffix="uáramos" tag="MSI1P0"/>
    <form suffix="uáremos" tag="MSF1P0"/>
    <form suffix="uásemos" tag="MSI1P0"/>
    <form suffix="ué" tag="MIS1S0"/>
    <form suffix="uéis" tag="MSP2P0"/>
    <form suffix="uó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="úa" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="úa" tag="MM02S0" synt="Imperative"/>
    <form suffix="úan" tag="MIP3P0"/>
    <form suffix="úas" tag="MIP2S0"/>
    <form suffix="úe" tag="MM03S0" synt="Imperative"/>
    <form suffix="úe" tag="MSP1S0"/>
    <form suffix="úe" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="úen" tag="MM03P0" synt="Imperative"/>
    <form suffix="úen" tag="MSP3P0"/>
    <form suffix="úes" tag="MSP2S0"/>
    <form suffix="úo" tag="MIP1S0"/>
  </table>
  <table name="V104" rads=".*(t|ret|dest)" fast="-"> <!-- Nieves: verbos segunda conjugación en infinitivo: t/ret/dest + orcer -->
    <!-- 3 members -->
    <form suffix="orcer" tag="MN0000" synt="Infinitive"/>
    <form suffix="orced" tag="MM02P0" synt="Imperative"/>
    <form suffix="orcemos" tag="MIP1P0"/>
    <form suffix="orceremos" tag="MIF1P0"/>
    <form suffix="orcerá" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="orcerán" tag="MIF3P0"/>
    <form suffix="orcerás" tag="MIF2S0"/>
    <form suffix="orceré" tag="MIF1S0"/>
    <form suffix="orceréis" tag="MIF2P0"/>
    <form suffix="orcería" tag="MIC1S0"/>
    <form suffix="orcería" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="orceríais" tag="MIC2P0"/>
    <form suffix="orceríamos" tag="MIC1P0"/>
    <form suffix="orcerían" tag="MIC3P0"/>
    <form suffix="orcerías" tag="MIC2S0"/>
    <form suffix="orcida" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="orcidas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="orcido" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="orcidos" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="orciendo" tag="MG0000"/>
    <form suffix="orciera" tag="MSI1S0"/>
    <form suffix="orciera" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="orcierais" tag="MSI2P0"/>
    <form suffix="orcieran" tag="MSI3P0"/>
    <form suffix="orcieras" tag="MSI2S0"/>
    <form suffix="orciere" tag="MSF1S0"/>
    <form suffix="orciere" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="orciereis" tag="MSF2P0"/>
    <form suffix="orcieren" tag="MSF3P0"/>
    <form suffix="orcieres" tag="MSF2S0"/>
    <form suffix="orcieron" tag="MIS3P0"/>
    <form suffix="orciese" tag="MSI1S0"/>
    <form suffix="orciese" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="orcieseis" tag="MSI2P0"/>
    <form suffix="orciesen" tag="MSI3P0"/>
    <form suffix="orcieses" tag="MSI2S0"/>
    <form suffix="orcimos" tag="MIS1P0"/>
    <form suffix="orciste" tag="MIS2S0"/>
    <form suffix="orcisteis" tag="MIS2P0"/>
    <form suffix="orciéramos" tag="MSI1P0"/>
    <form suffix="orciéremos" tag="MSF1P0"/>
    <form suffix="orciésemos" tag="MSI1P0"/>
    <form suffix="orció" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="orcéis" tag="MIP2P0"/>
    <form suffix="orcí" tag="MIS1S0"/>
    <form suffix="orcía" tag="MII1S0"/>
    <form suffix="orcía" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="orcíais" tag="MII2P0"/>
    <form suffix="orcíamos" tag="MII1P0"/>
    <form suffix="orcían" tag="MII3P0"/>
    <form suffix="orcías" tag="MII2S0"/>
    <form suffix="orzamos" tag="MM01P0" synt="Imperative"/>
    <form suffix="orzamos" tag="MSP1P0"/>
    <form suffix="orzáis" tag="MSP2P0"/>
    <form suffix="uerce" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uerce" tag="MM02S0" synt="Imperative"/>
    <form suffix="uercen" tag="MIP3P0"/>
    <form suffix="uerces" tag="MIP2S0"/>
    <form suffix="uerza" tag="MM03S0" synt="Imperative"/>
    <form suffix="uerza" tag="MSP1S0"/>
    <form suffix="uerza" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uerzan" tag="MM03P0" synt="Imperative"/>
    <form suffix="uerzan" tag="MSP3P0"/>
    <form suffix="uerzas" tag="MSP2S0"/>
    <form suffix="uerzo" tag="MIP1S0"/>
  </table>
  <table name="V105" rads=".*(ac|manc|desc)" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: ac/manc/desc + ornar -->
    <!-- 3 members -->
    <form suffix="ornar" tag="MN0000" synt="Infinitive"/>
    <form suffix="ornaba" tag="MII1S0"/>
    <form suffix="ornaba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="ornabais" tag="MII2P0"/>
    <form suffix="ornaban" tag="MII3P0"/>
    <form suffix="ornabas" tag="MII2S0"/>
    <form suffix="ornad" tag="MM02P0" synt="Imperative"/>
    <form suffix="ornada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="ornadas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="ornado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="ornados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="ornamos" tag="MIP1P0"/>
    <form suffix="ornamos" tag="MIS1P0"/>
    <form suffix="ornando" tag="MG0000"/>
    <form suffix="ornara" tag="MSI1S0"/>
    <form suffix="ornara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ornarais" tag="MSI2P0"/>
    <form suffix="ornaran" tag="MSI3P0"/>
    <form suffix="ornaras" tag="MSI2S0"/>
    <form suffix="ornare" tag="MSF1S0"/>
    <form suffix="ornare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="ornareis" tag="MSF2P0"/>
    <form suffix="ornaremos" tag="MIF1P0"/>
    <form suffix="ornaren" tag="MSF3P0"/>
    <form suffix="ornares" tag="MSF2S0"/>
    <form suffix="ornaron" tag="MIS3P0"/>
    <form suffix="ornará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="ornarán" tag="MIF3P0"/>
    <form suffix="ornarás" tag="MIF2S0"/>
    <form suffix="ornaré" tag="MIF1S0"/>
    <form suffix="ornaréis" tag="MIF2P0"/>
    <form suffix="ornaría" tag="MIC1S0"/>
    <form suffix="ornaría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="ornaríais" tag="MIC2P0"/>
    <form suffix="ornaríamos" tag="MIC1P0"/>
    <form suffix="ornarían" tag="MIC3P0"/>
    <form suffix="ornarías" tag="MIC2S0"/>
    <form suffix="ornase" tag="MSI1S0"/>
    <form suffix="ornase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="ornaseis" tag="MSI2P0"/>
    <form suffix="ornasen" tag="MSI3P0"/>
    <form suffix="ornases" tag="MSI2S0"/>
    <form suffix="ornaste" tag="MIS2S0"/>
    <form suffix="ornasteis" tag="MIS2P0"/>
    <form suffix="ornemos" tag="MSP1P0"/>
    <form suffix="ornemos" tag="MM01P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="ornábamos" tag="MII1P0"/>
    <form suffix="ornáis" tag="MIP2P0"/>
    <form suffix="ornáramos" tag="MSI1P0"/>
    <form suffix="ornáremos" tag="MSF1P0"/>
    <form suffix="ornásemos" tag="MSI1P0"/>
    <form suffix="orné" tag="MIS1S0"/>
    <form suffix="ornéis" tag="MSP2P0"/>
    <form suffix="ornó" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="uerna" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="uerna" tag="MM02S0" synt="Imperative"/>
    <form suffix="uernan" tag="MIP3P0"/>
    <form suffix="uernas" tag="MIP2S0"/>
    <form suffix="uerne" tag="MSP1S0"/>
    <form suffix="uerne" tag="MSP3S0" synt="ThirdSing"/>
    <form suffix="uerne" tag="MM03S0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="uernen" tag="MSP3P0"/>
    <form suffix="uernen" tag="MM03P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="uernes" tag="MSP2S0"/>
    <form suffix="uerno" tag="MIP1S0"/>
  </table>
  <!-- Tabla para la construcción del verbo haber-->
  <table name="V106" rads="" fast="-">
    <form suffix="haber" tag="AN0000" synt="Infinitive"/>
    <form suffix="habido" tag="AP0000" synt="PastParticiple"/>
    <form suffix="habida" tag="AP0000" synt="PastParticiple"/>
    <form suffix="habiendo" tag="AG0000"/>
    <form suffix="he" tag="AIP1S0"/>
    <form suffix="has" tag="AIP2S0"/>
    <form suffix="ha" tag="AIP3S0" synt="ThirdSing"/>
    <form suffix="hay" tag="MIP3S0" synt="ThirdSing"/>
    <form suffix="hemos" tag="AIP1P0"/>
    <form suffix="habéis" tag="AIP2P0"/>
    <form suffix="han" tag="AIP3P0"/>
    <form suffix="había" tag="AII1S0"/>
    <form suffix="habías" tag="AII2S0"/>
    <form suffix="había" tag="AII3S0" synt="ThirdSing"/>
    <form suffix="habíamos" tag="AII1P0"/>
    <form suffix="habíais" tag="AII2P0"/>
    <form suffix="habían" tag="AII3P0"/>
    <form suffix="habré" tag="AIF1S0"/>
    <form suffix="habrás" tag="AIF2S0"/>
    <form suffix="habrá" tag="AIF3S0" synt="ThirdSing"/>
    <form suffix="habremos" tag="AIF1P0"/>
    <form suffix="habreis" tag="AIF2P0"/>
    <form suffix="habrán" tag="AIF3P0"/>
    <form suffix="habría" tag="AIC1S0"/>
    <form suffix="habrías" tag="AIC2S0"/>
    <form suffix="habría" tag="AIC3S0" synt="ThirdSing"/>
    <form suffix="habríamos" tag="AIC1P0"/>
    <form suffix="habríais" tag="AIC2P0"/>
    <form suffix="habrían" tag="AIC3P0"/>
    <form suffix="hube" tag="AIS1S0"/>
    <form suffix="hubiste" tag="AIS2S0"/>
    <form suffix="hubo" tag="AIS3S0" synt="ThirdSing"/>
    <form suffix="hubimos" tag="AIS1P0"/>
    <form suffix="hubisteis" tag="AIS2P0"/>
    <form suffix="hubieron" tag="AIS3P0"/>
    <form suffix="haya" tag="ASP1S0"/>
    <form suffix="hayas" tag="ASP2S0"/>
    <form suffix="haya" tag="ASP3S0" synt="ThirdSing"/>
    <form suffix="hayamos" tag="ASP1P0"/>
    <form suffix="hayamos" tag="AM01P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="hayáis" tag="ASP2P0"/>
    <form suffix="hayan" tag="ASP3P0"/>
    <form suffix="hubiera" tag="ASI1S0"/>
    <form suffix="hubieras" tag="ASI2S0"/>
    <form suffix="hubiera" tag="ASI3S0" synt="ThirdSing"/>
    <form suffix="hubiéramos" tag="ASI1P0"/>
    <form suffix="hubierais" tag="ASI2P0"/>
    <form suffix="hubieran" tag="ASI3P0"/>
    <form suffix="hubiese" tag="ASI1S0"/>
    <form suffix="hubieses" tag="ASI2S0"/>
    <form suffix="hubiese" tag="ASI3S0" synt="ThirdSing"/>
    <form suffix="hubiésemos" tag="ASI1P0"/>
    <form suffix="hubieseis" tag="ASI2P0"/>
    <form suffix="hubiesen" tag="ASI3P0"/>
    <form suffix="hubiere" tag="ASF1S0"/>
    <form suffix="hubieres" tag="ASF2S0"/>
    <form suffix="hubiere" tag="ASF3S0" synt="ThirdSing"/>
    <form suffix="hubiéremos" tag="ASF1P0"/>
    <form suffix="hubiereis" tag="ASF2P0"/>
    <form suffix="hubieren" tag="ASF3P0"/>
    <form suffix="he" tag="AM02S0" synt="Imperative"/>
    <form suffix="habed" tag="AM02P0" synt="Imperative"/>
    <form suffix="haya" tag="AM02S0" synt="Imperative"/>
    <form suffix="hayan" tag="AM02P0" synt="Imperative"/>
  </table>
  <!-- Tabla para la construcción del verbo ser-->
  <table name="V107" rads="" fast="-">
    <form suffix="ser" tag="AN0000" synt="Infinitive"/>
    <form suffix="sido" tag="AP0000" synt="PastParticiple"/>
    <form suffix="siendo" tag="AG0000"/>
    <form suffix="soy" tag="AIP1S0"/>
    <form suffix="eres" tag="AIP2S0"/>
    <form suffix="es" tag="AIP3S0" synt="ThirdSing"/>
    <form suffix="somos" tag="AIP1P0"/>
    <form suffix="sois" tag="AIP2P0"/>
    <form suffix="son" tag="AIP3P0"/>
    <form suffix="era" tag="AII1S0"/>
    <form suffix="eras" tag="AII2S0"/>
    <form suffix="era" tag="AII3S0" synt="ThirdSing"/>
    <form suffix="éramos" tag="AII1P0"/>
    <form suffix="erais" tag="AII2P0"/>
    <form suffix="eran" tag="AII3P0"/>
    <form suffix="seré" tag="AIF1S0"/>
    <form suffix="serás" tag="AIF2S0"/>
    <form suffix="será" tag="AIF3S0" synt="ThirdSing"/>
    <form suffix="seremos" tag="AIF1P0"/>
    <form suffix="sereis" tag="AIF2P0"/>
    <form suffix="serán" tag="AIF3P0"/>
    <form suffix="sería" tag="AIC1S0"/>
    <form suffix="serías" tag="AIC2S0"/>
    <form suffix="sería" tag="AIC3S0" synt="ThirdSing"/>
    <form suffix="seríamos" tag="AIC1P0"/>
    <form suffix="seríais" tag="AIC2P0"/>
    <form suffix="serían" tag="AIC3P0"/>
    <form suffix="fuí" tag="AIS1S0"/>
    <form suffix="fuiste" tag="AIS2S0"/>
    <form suffix="fue" tag="AIS3S0" synt="ThirdSing"/>
    <form suffix="fuimos" tag="AIS1P0"/>
    <form suffix="fuisteis" tag="AIS2P0"/>
    <form suffix="fueron" tag="AIS3P0"/>
    <form suffix="sea" tag="ASP1S0"/>
    <form suffix="seas" tag="ASP2S0"/>
    <form suffix="sea" tag="ASP3S0" synt="ThirdSing"/>
    <form suffix="seamos" tag="ASP1P0"/>
    <form suffix="seamos" tag="AM01P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="seáis" tag="ASP2P0"/>
    <form suffix="sean" tag="ASP3P0"/>
    <form suffix="fuera" tag="ASI1S0"/>
    <form suffix="fueras" tag="ASI2S0"/>
    <form suffix="fuera" tag="ASI3S0" synt="ThirdSing"/>
    <form suffix="fuéramos" tag="ASI1P0"/>
    <form suffix="fuerais" tag="ASI2P0"/>
    <form suffix="fueran" tag="ASI3P0"/>
    <form suffix="fuese" tag="ASI1S0"/>
    <form suffix="fueses" tag="ASI2S0"/>
    <form suffix="fuese" tag="ASI3S0" synt="ThirdSing"/>
    <form suffix="fuésemos" tag="ASI1P0"/>
    <form suffix="fueseis" tag="ASI2P0"/>
    <form suffix="fuesen" tag="ASI3P0"/>
    <form suffix="fuere" tag="ASF1S0"/>
    <form suffix="fueres" tag="ASF2S0"/>
    <form suffix="fuere" tag="ASF3S0" synt="ThirdSing"/>
    <form suffix="fuéremos" tag="ASF1P0"/>
    <form suffix="fuereis" tag="ASF2P0"/>
    <form suffix="fueren" tag="ASF3P0"/>
    <form suffix="sé" tag="AM02S0" synt="Imperative"/>
    <form suffix="sed" tag="AM02P0" synt="Imperative"/>
    <form suffix="sea" tag="AM02S0" synt="Imperative"/>
    <form suffix="sean" tag="AM02P0" synt="Imperative"/>
  </table>
  <!-- Tabla para la construcción del verbo estar-->
  <table name="V108" rads="" fast="-">
    <form suffix="estar" tag="AN0000" synt="Infinitive"/>
    <form suffix="estado" tag="AP0000" synt="PastParticiple"/>
    <form suffix="estando" tag="AG0000"/>
    <form suffix="estoy" tag="AIP1S0"/>
    <form suffix="estás" tag="AIP2S0"/>
    <form suffix="está" tag="AIP3S0" synt="ThirdSing"/>
    <form suffix="estamos" tag="AIP1P0"/>
    <form suffix="estais" tag="AIP2P0"/>
    <form suffix="están" tag="AIP3P0"/>
    <form suffix="estaba" tag="AII1S0"/>
    <form suffix="estabas" tag="AII2S0"/>
    <form suffix="estaba" tag="AII3S0" synt="ThirdSing"/>
    <form suffix="estábamos" tag="AII1P0"/>
    <form suffix="estabais" tag="AII2P0"/>
    <form suffix="estaban" tag="AII3P0"/>
    <form suffix="estará" tag="AIF1S0"/>
    <form suffix="estarás" tag="AIF2S0"/>
    <form suffix="estará" tag="AIF3S0" synt="ThirdSing"/>
    <form suffix="estaremos" tag="AIF1P0"/>
    <form suffix="estareis" tag="AIF2P0"/>
    <form suffix="estarán" tag="AIF3P0"/>
    <form suffix="estaría" tag="AIC1S0"/>
    <form suffix="estarías" tag="AIC2S0"/>
    <form suffix="estaría" tag="AIC3S0" synt="ThirdSing"/>
    <form suffix="estaríamos" tag="AIC1P0"/>
    <form suffix="estaríais" tag="AIC2P0"/>
    <form suffix="estarían" tag="AIC3P0"/>
    <form suffix="estuve" tag="AIS1S0"/>
    <form suffix="estuviste" tag="AIS2S0"/>
    <form suffix="estuvo" tag="AIS3S0" synt="ThirdSing"/>
    <form suffix="estuvimos" tag="AIS1P0"/>
    <form suffix="estuvisteis" tag="AIS2P0"/>
    <form suffix="estuvieron" tag="AIS3P0"/>
    <form suffix="esté" tag="ASP1S0"/>
    <form suffix="estés" tag="ASP2S0"/>
    <form suffix="esté" tag="ASP3S0" synt="ThirdSing"/>
    <form suffix="estemos" tag="ASP1P0"/>
    <form suffix="estemos" tag="AM01P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="estéis" tag="ASP2P0"/>
    <form suffix="estén" tag="ASP3P0"/>
    <form suffix="estuviera" tag="ASI1S0"/>
    <form suffix="estuvieras" tag="ASI2S0"/>
    <form suffix="estuviera" tag="ASI3S0" synt="ThirdSing"/>
    <form suffix="estuviéramos" tag="ASI1P0"/>
    <form suffix="estuvierais" tag="ASI2P0"/>
    <form suffix="estuvieran" tag="ASI3P0"/>
    <form suffix="estuviese" tag="ASI1S0"/>
    <form suffix="estuvieses" tag="ASI2S0"/>
    <form suffix="estuviese" tag="ASI3S0" synt="ThirdSing"/>
    <form suffix="estuviésemos" tag="ASI1P0"/>
    <form suffix="estuvieseis" tag="ASI2P0"/>
    <form suffix="estuviesen" tag="ASI3P0"/>
    <form suffix="estuviere" tag="ASF1S0"/>
    <form suffix="estuvieres" tag="ASF2S0"/>
    <form suffix="estuviere" tag="ASF3S0" synt="ThirdSing"/>
    <form suffix="estuviéremos" tag="ASF1P0"/>
    <form suffix="estuviereis" tag="ASF2P0"/>
    <form suffix="estuvieren" tag="ASF3P0"/>
    <form suffix="está" tag="AM02S0" synt="Imperative"/>
    <form suffix="estad" tag="AM02P0" synt="Imperative"/>
    <form suffix="esté" tag="AM02S0" synt="Imperative"/>
    <form suffix="estén" tag="AM02P0" synt="Imperative"/>
  </table>
  <!-- Tabla para la construcción del verbo querer, no funciona--> 
  <table name="V109" rads=".*q" fast="-">
	<form suffix="ueramos" tag="MM01P0" synt="Imperative"/>
	<form suffix="ueramos" tag="MSP1P0"/>
	<form suffix="uered" tag="MM02P0" synt="Imperative"/>
	<form suffix="ueremos" tag="MIP1P0"/>
	<form suffix="uerer" tag="MN0000" synt="Infinitive"/>
	<form suffix="uerida" tag="MP00SF" synt="PastParticiple"/>
	<form suffix="ueridas" tag="MP00PF" synt="PastParticiple"/>
	<form suffix="uerido" tag="MP00SM" synt="PastParticiple"/>
	<form suffix="ueridos" tag="MP00PM" synt="PastParticiple"/>
	<form suffix="ueriendo" tag="MG0000"/>
	<form suffix="uerremos" tag="MIF1P0"/>
	<form suffix="uerrá" tag="MIF3S0" synt="ThirdSing"/>
	<form suffix="uerrán" tag="MIF3P0"/>
	<form suffix="uerrás" tag="MIF2S0"/>
	<form suffix="uerré" tag="MIF1S0"/>
	<form suffix="uerréis" tag="MIF2P0"/>
	<form suffix="uerría" tag="MIC1S0"/>
	<form suffix="uerría" tag="MIC3S0" synt="ThirdSing"/>
	<form suffix="uerríais" tag="MIC2P0"/>
	<form suffix="uerríamos" tag="MIC1P0"/>
	<form suffix="uerrían" tag="MIC3P0"/>
	<form suffix="uerrías" tag="MIC2S0"/>
	<form suffix="ueráis" tag="MSP2P0"/>
	<form suffix="ueréis" tag="MIP2P0"/>
	<form suffix="uería" tag="MII1S0"/>
	<form suffix="uería" tag="MII3S0" synt="ThirdSing"/>
	<form suffix="ueríais" tag="MII2P0"/>
	<form suffix="ueríamos" tag="MII1P0"/>
	<form suffix="uerían" tag="MII3P0"/>
	<form suffix="uerías" tag="MII2S0"/>
	<form suffix="uiera" tag="MM03S0" synt="Imperative"/>
	<form suffix="uiera" tag="MSP1S0"/>
	<form suffix="uiera" tag="MSP3S0" synt="ThirdSing"/>
	<form suffix="uieran" tag="MM03P0" synt="Imperative"/>
	<form suffix="uieran" tag="MSP3P0"/>
	<form suffix="uieras" tag="MSP2S0"/>
	<form suffix="uiere" tag="MIP3S0" synt="ThirdSing"/>
	<form suffix="uiere" tag="MM02S0" synt="Imperative"/>
	<form suffix="uieren" tag="MIP3P0"/>
	<form suffix="uieres" tag="MIP2S0"/>
	<form suffix="uiero" tag="MIP1S0"/>
	<form suffix="uise" tag="MIS1S0"/>
	<form suffix="uisiera" tag="MSI1S0"/>
	<form suffix="uisiera" tag="MSI3S0" synt="ThirdSing"/>
	<form suffix="uisierais" tag="MSI2P0"/>
	<form suffix="uisieran" tag="MSI3P0"/>
	<form suffix="uisieras" tag="MSI2S0"/>
	<form suffix="uisiere" tag="MSF1S0"/>
	<form suffix="uisiere" tag="MSF3S0" synt="ThirdSing"/>
	<form suffix="uisiereis" tag="MSF2P0"/>
	<form suffix="uisieren" tag="MSF3P0"/>
	<form suffix="uisieres" tag="MSF2S0"/>
	<form suffix="uisieron" tag="MIS3P0"/>
	<form suffix="uisiese" tag="MSI1S0"/>
	<form suffix="uisiese" tag="MSI3S0" synt="ThirdSing"/>
	<form suffix="uisieseis" tag="MSI2P0"/>
	<form suffix="uisiesen" tag="MSI3P0"/>
	<form suffix="uisieses" tag="MSI2S0"/>
	<form suffix="uisimos" tag="MIS1P0"/>
	<form suffix="uisiste" tag="MIS2S0"/>
	<form suffix="uisisteis" tag="MIS2P0"/>
	<form suffix="uisiéramos" tag="MSI1P0"/>
	<form suffix="uisiéremos" tag="MSF1P0"/>
	<form suffix="uisiésemos" tag="MSI1P0"/>
	<form suffix="uiso" tag="MIS3S0" synt="ThirdSing"/>
  </table>
<table name="V110" rads=".*ad" fast="-"> <!-- Nieves: verbos primera conjugación en infinitivo: ad + estrar -->
    <!-- 1 member -->
    <form suffix="estrar" tag="MN0000" synt="Infinitive"/>
    <form suffix="estraba" tag="MII1S0"/>
    <form suffix="estraba" tag="MII3S0" synt="ThirdSing"/>
    <form suffix="estrabais" tag="MII2P0"/>
    <form suffix="estraban" tag="MII3P0"/>
    <form suffix="estrabas" tag="MII2S0"/>
    <form suffix="estrad" tag="MM02P0" synt="Imperative"/>
    <form suffix="estrada" tag="MP00SF" synt="PastParticiple"/>
    <form suffix="estradas" tag="MP00PF" synt="PastParticiple"/>
    <form suffix="estrado" tag="MP00SM" synt="PastParticiple"/>
    <form suffix="estrados" tag="MP00PM" synt="PastParticiple"/>
    <form suffix="estramos" tag="MIP1P0"/>
    <form suffix="estramos" tag="MIS1P0"/>
    <form suffix="estrando" tag="MG0000"/>
    <form suffix="estrara" tag="MSI1S0"/>
    <form suffix="estrara" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="estrarais" tag="MSI2P0"/>
    <form suffix="estraran" tag="MSI3P0"/>
    <form suffix="estraras" tag="MSI2S0"/>
    <form suffix="estrare" tag="MSF1S0"/>
    <form suffix="estrare" tag="MSF3S0" synt="ThirdSing"/>
    <form suffix="estrareis" tag="MSF2P0"/>
    <form suffix="estraremos" tag="MIF1P0"/>
    <form suffix="estraren" tag="MSF3P0"/>
    <form suffix="estrares" tag="MSF2S0"/>
    <form suffix="estraron" tag="MIS3P0"/>
    <form suffix="estrará" tag="MIF3S0" synt="ThirdSing"/>
    <form suffix="estrarán" tag="MIF3P0"/>
    <form suffix="estrarás" tag="MIF2S0"/>
    <form suffix="estraré" tag="MIF1S0"/>
    <form suffix="estraréis" tag="MIF2P0"/>
    <form suffix="estraría" tag="MIC1S0"/>
    <form suffix="estraría" tag="MIC3S0" synt="ThirdSing"/>
    <form suffix="estraríais" tag="MIC2P0"/>
    <form suffix="estraríamos" tag="MIC1P0"/>
    <form suffix="estrarían" tag="MIC3P0"/>
    <form suffix="estrarías" tag="MIC2S0"/>
    <form suffix="estrase" tag="MSI1S0"/>
    <form suffix="estrase" tag="MSI3S0" synt="ThirdSing"/>
    <form suffix="estraseis" tag="MSI2P0"/>
    <form suffix="estrasen" tag="MSI3P0"/>
    <form suffix="estrases" tag="MSI2S0"/>
    <form suffix="estraste" tag="MIS2S0"/>
    <form suffix="estrasteis" tag="MIS2P0"/>
    <form suffix="estremos" tag="MSP1P0"/>
    <form suffix="estremos" tag="MM01P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="estrábamos" tag="MII1P0"/>
    <form suffix="estráis" tag="MIP2P0"/>
    <form suffix="estráramos" tag="MSI1P0"/>
    <form suffix="estráremos" tag="MSF1P0"/>
    <form suffix="estrásemos" tag="MSI1P0"/>
    <form suffix="estré" tag="MIS1S0"/>
    <form suffix="estréis" tag="MSP2P0"/>
    <form suffix="estró" tag="MIS3S0" synt="ThirdSing"/>
    <form suffix="iestra" tag="MM02S0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="iestre" tag="MSP1S0"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="iestre" tag="MM03S0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="iestre" tag="MSP3S0" synt="ThirSing"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="iestren" tag="MSP3P0"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="iestren" tag="MM03P0"/> <!-- Nieves: added all the forms for Imperative -->
    <form suffix="iestres" tag="MSP2S0"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="iestra" tag="MIP3S0" synt="ThirSing"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="iestran" tag="MIP3P0"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="iestras" tag="MIP2S0"/> <!-- Nieves: added all the forms for Present -->
    <form suffix="iestro" tag="MIP1S0"/> <!-- Nieves: added all the forms for Present -->
  </table>
</description>
